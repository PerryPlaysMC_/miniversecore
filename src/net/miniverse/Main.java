package net.miniverse;

import net.miniverse.anticheat.Settings;
import net.miniverse.chat.Message;
import net.miniverse.core.IMiniverse;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.events.user.UserActionEvent;
import net.miniverse.core.events.user.UserJoinEvent;
import net.miniverse.core.events.user.UserQuitEvent;
import net.miniverse.enchantment.CustomEnchant;
import net.miniverse.enchantment.EnchantHandler;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.OfflineUserBase;
import net.miniverse.user.User;
import net.miniverse.user.UserBase;
import net.miniverse.user.permissions.Group;
import net.miniverse.user.permissions.GroupManager;
import net.miniverse.util.*;
import net.miniverse.util.abstraction.AbstractionHandler;
import net.miniverse.util.config.ConfigManager;
import net.miniverse.util.itemutils.ItemEnchantUtil;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@SuppressWarnings("all")
public class Main extends MiniverseCore implements Listener {


    private static HashMap<Player, Integer> afk = new HashMap<>();

    public static HashMap<Player, Integer>  getAFK() {
        return afk;
    }

    @Override
    public void onEnable() {
        init();
        initialize("MiniverseCore");
        ConfigManager.getAllConfigs(true);
        Settings.registerEvents();
        AbstractionHandler.load();
        if(settings.getBoolean(""))
        if(getWorlds().getSection("Worlds") != null) {
            for(String world : getWorlds().getSection("Worlds").getKeys(false)) {
                if(Bukkit.getWorld(world) == null) {
                    try {
                        WorldCreator ww = new WorldCreator(world);
                        debug("Adding world: " + ww.name());
                        Bukkit.createWorld(ww);
                        Bukkit.getWorlds().add(Bukkit.getWorld(ww.name()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if(!ConfigManager.copyFolderFiles("plugins/Miniverse/userInfo","plugins/Miniverse/userInfoBackup")){
            Miniverse.log(IMiniverse.LogType.ERROR, "Error has occurred");
        }
    }

    HashMap<String, Integer> stuff = new HashMap<>();

    @EventHandler
    void onAnvilPrepare(PrepareAnvilEvent e) {
        AnvilInventory inv = e.getInventory();
        if(inv.getItem(0) == null || inv.getItem(1) == null) return;
        ItemStack i1 = inv.getItem(0), i2 = inv.getItem(1);
        if(!i1.hasItemMeta() || !i2.hasItemMeta()) return;
        ItemStack r = e.getResult();
        for(HumanEntity h : e.getInventory().getViewers()) {
            ((Player)h).updateInventory();
        }
        e.getInventory().setRepairCost(10);
        for(HumanEntity h : e.getInventory().getViewers()) {
            ((Player) h).updateInventory();
        }
        if(r == null ||r.getType().name().contains("AIR")) {
            if(i1.getType() == i2.getType()) r.setType(i1.getType());
            r.setItemMeta(i1.getItemMeta());
            e.setResult(r);
            for(HumanEntity h : e.getInventory().getViewers()) {
                if(stuff.containsKey(((Player) h).getName())) {
                    e.getInventory().setRepairCost(stuff.get(((Player) h).getName()));
                    break;
                }else {
                    stuff.put(((Player) h).getName(), new Random().nextInt(15)+1);
                    e.getInventory().setRepairCost(stuff.get(((Player) h).getName()));
                    break;
                }
            }
            for(HumanEntity h : e.getInventory().getViewers()) {
                ((Player) h).updateInventory();
            }
        }
        r.setItemMeta(r.getItemMeta());
        e.setResult(r);
        List<CustomEnchant> enchantments = new ArrayList<>();
        for(CustomEnchant ench : EnchantHandler.getEnchantments()) {
            ItemMeta im1 = i1.getItemMeta(), im2 = i2.getItemMeta(), imr = r.getItemMeta();
            if(!enchantments.contains(ench) && im1.hasEnchant(ench) && im2.hasEnchants() && !im2.hasEnchant(ench)) {
                A:for(Enchantment en : im2.getEnchants().keySet()) {
                    if(ench.conflictsWith(en)) {
                        if(!(en instanceof CustomEnchant))
                            imr = ItemEnchantUtil.removeBukkitEnchant(r, en);
                        else if((en instanceof CustomEnchant) && r != null && r.hasItemMeta())
                            imr = ItemEnchantUtil.removeEnchant(r, (CustomEnchant)en, ItemEnchantUtil.getEnchant(i1, (CustomEnchant)en));
                        r.setItemMeta(imr);
                        e.setResult(r);
                    }else {
                        boolean willbreak = false;
                        if(!(en instanceof CustomEnchant)) {
                            imr.addEnchant(en, im2.getEnchantLevel(en), true);
                        }else if((en instanceof CustomEnchant) && r != null && r.hasItemMeta()) {
                            imr = ItemEnchantUtil.addEnchantment(r, (CustomEnchant)en, ItemEnchantUtil.getEnchant(i1, (CustomEnchant)en));
                            willbreak = true;
                        }
                        r.setItemMeta(imr);
                        e.setResult(r);
                        enchantments.add(ench);
                        if(willbreak) break A;
                    }
                }
            }
            if(!enchantments.contains(ench) && im2.hasEnchant(ench) && im1.hasEnchants() && !im1.hasEnchant(ench)) {
                boolean willAdd = true;
                A:for(Enchantment en : im1.getEnchants().keySet()) {
                    if(ench.conflictsWith(en)) {
                        willAdd = false;
                        continue A;
                    }
                    willAdd = true;
                }
                if(willAdd) {
                    imr = ItemEnchantUtil.addEnchantment(r, ench, ItemEnchantUtil.getEnchant(i2, ench));
                    r.setItemMeta(imr);
                    e.setResult(r);
                    ((Player)e.getView().getPlayer()).updateInventory();
                    r.setItemMeta(imr);
                    e.setResult(r);
                    ((Player)e.getView().getPlayer()).updateInventory();
                    e.getInventory().setItem(2, r);
                    ((Player)e.getView().getPlayer()).updateInventory();
                    e.getInventory().setItem(2, r);
                    ((Player)e.getView().getPlayer()).updateInventory();
                    enchantments.add(ench);
                    willAdd = false;
                }
            }
            ItemEnchantUtil.getEnchant(r, ench);
            if(!enchantments.contains(ench) &&((im1.hasEnchant(ench) && im2.hasEnchant(ench)) || (ItemEnchantUtil.hasEnchant(i1, ench)
                    && ItemEnchantUtil.hasEnchant(i2, ench)))) {
                int lvl1 = ItemEnchantUtil.getEnchant(i1, ench), lvl2 = ItemEnchantUtil.getEnchant(i2, ench);
                if(lvl1 == lvl2) {
                    if((lvl1+1) > ench.getMaxLevel()) continue;
                    imr = ItemEnchantUtil.removeEnchant(r, ench, lvl1);
                    r.setItemMeta(imr);
                    e.setResult(r);
                    imr = ItemEnchantUtil.forceAddEnchantment(r, ench, lvl1+1);
                    r.setItemMeta(imr);
                    e.setResult(r);
                }
                else if(lvl2 > lvl1) {
                    imr = ItemEnchantUtil.removeEnchant(r, ench, lvl1);
                    r.setItemMeta(imr);
                    e.setResult(r);
                    imr = ItemEnchantUtil.forceAddEnchantment(r, ench, lvl2);
                    r.setItemMeta(imr);
                    e.setResult(r);
                }
                else if(lvl1 > lvl2) {
                    imr = ItemEnchantUtil.removeEnchant(r, ench, lvl2);
                    r.setItemMeta(imr);
                    e.setResult(r);
                    imr = ItemEnchantUtil.forceAddEnchantment(r, ench, lvl1);
                    r.setItemMeta(imr);
                    e.setResult(r);
                }
            }
        }
    }

    int getLevel(CustomEnchant en) {
        int exp = 1;
        switch(en.getLevels()) {
            case LEVELS_25_30:
                exp = new Random().nextInt(6) +25;
                break;
            case LEVELS_20_25:
                exp = new Random().nextInt(6) +20;
                break;
            case LEVELS_15_20:
                exp = new Random().nextInt(6) +15;
                break;
            case LEVELS_10_15:
                exp = new Random().nextInt(6) +10;
                break;
            case LEVELS_6_10:
                exp = new Random().nextInt(5) +6;
                break;
            case LEVELS_0_6:
                exp = new Random().nextInt(7);
                break;
        }
        return exp;
    }

    @EventHandler
    public void onEnchantEvent(EnchantItemEvent e) {
        Player player = e.getEnchanter();
        ItemStack item = e.getItem();
        ItemMeta im = item.getItemMeta();
        A:for(Enchantment enc : e.getEnchantsToAdd().keySet()) {
            for(Enchantment et : im.getEnchants().keySet()) {
                if(enc.conflictsWith(et))
                    continue A;
            }
            if(!im.hasEnchant(enc)) {
                im.addEnchant(enc, e.getEnchantsToAdd().get(enc), true);
                item.setItemMeta(im);
            }
        }
        List<CustomEnchant> enchants2 = new ArrayList<>();
        List<CustomEnchant> hold = new ArrayList<>();
        hold.addAll(EnchantHandler.getEnchantments());
        List<CustomEnchant> random = new ArrayList<>();
        while(hold.size() > 0) {
            int r = new Random().nextInt(hold.size());
            random.add(hold.get(r));
            hold.remove(r);
        }

        A:for(CustomEnchant ench : random) {
            boolean t = false;
            C:switch(ench.getLevels()) {
                case LEVELS_25_30:
                    if(e.getExpLevelCost() >= 25) t = true;
                    break C;
                case LEVELS_20_25:
                    if(e.getExpLevelCost() >= 20 && e.getExpLevelCost() < 25) t = true;
                    break C;
                case LEVELS_15_20:
                    if(e.getExpLevelCost() >= 15 && e.getExpLevelCost() < 20) t = true;
                    break C;
                case LEVELS_10_15:
                    if(e.getExpLevelCost() >= 10 && e.getExpLevelCost() < 15) t = true;
                    break C;
                case LEVELS_6_10:
                    if(e.getExpLevelCost() >= 6 && e.getExpLevelCost() < 10) t = true;
                    break C;
                case LEVELS_0_6:
                    if(e.getExpLevelCost() >= 0 && e.getExpLevelCost() < 6) t = true;
                    break C;
            }
            if(!t) continue A;
            for(Enchantment enc : im.getEnchants().keySet()) {
                if(ench.conflictsWith(enc)) continue A;
            }
            for(Enchantment enc : enchants2) {
                if(ench.conflictsWith(enc)) continue A;
            }
            for(Enchantment enc : e.getEnchantsToAdd().keySet()) {
                if(ench.conflictsWith(enc)) continue A;
            }
            if(ench.canEnchantItem(item) && !item.containsEnchantment(ench) && !item.getItemMeta().hasEnchant(ench)) {
                for(Enchantment enc : im.getEnchants().keySet()) {
                    if(ench.conflictsWith(enc)) continue A;
                }
                for(Enchantment enc : enchants2) {
                    if(ench.conflictsWith(enc)) continue A;
                }
                for(Enchantment enc : e.getEnchantsToAdd().keySet()) {
                    if(ench.conflictsWith(enc)) continue A;
                }
                item.setItemMeta(ItemEnchantUtil.addEnchantment(item, ench, e.getExpLevelCost()));
                if(ItemEnchantUtil.getEnchant(item, ench) > -1) {
                    enchants2.add(ench);
                }
            }
        }
        item.setItemMeta(ItemEnchantUtil.fixEnchantments(item));
    }

    @Override
    public void onDisable() {
        File folder1 = new File("plugins/Miniverse/userInfo");
        File folder2 = new File("plugins/Miniverse/userInfoBackup");
        if((folder1.listFiles() == null || folder2.listFiles() == null) || (folder1.listFiles().length != folder2.listFiles().length)) return;
        for(int i = 0; i < folder1.listFiles().length; i++) {
            File f1 = folder1.listFiles()[i];
            File f2 = folder2.listFiles()[i];
            try {
                if(f1.length() == 0) {
                    FileUtils.copyFile(f2, f1);
                    log(LogType.INFO, "Copied file " + f2.getName() + " To " + f1.getName());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void userAction(PlayerInteractEvent e) {
        Player player = e.getPlayer(), p = e.getPlayer();
        User u = Miniverse.getUser(player);
        if(u == null) {
            MiniverseCore.getAPI().addUser(new UserBase(player));
        }
        u = Miniverse.getUser(player);
        UserActionEvent.Action action = UserActionEvent.Action.NULL;
        UserActionEvent.Action actiontype = UserActionEvent.Action.NULL;
        if(e.getAction() == Action.LEFT_CLICK_AIR) {
            action = UserActionEvent.Action.LEFT_CLICK_AIR;
            actiontype = UserActionEvent.Action.LEFT_CLICK;
        } else if(e.getAction() == Action.RIGHT_CLICK_AIR) {
            action = UserActionEvent.Action.RIGHT_CLICK_AIR;
            actiontype = UserActionEvent.Action.RIGHT_CLICK;
        } else if(e.getAction() == Action.LEFT_CLICK_BLOCK) {
            action = UserActionEvent.Action.LEFT_CLICK_BLOCK;
            actiontype = UserActionEvent.Action.LEFT_CLICK;
        } else if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            action = UserActionEvent.Action.RIGHT_CLICK_BLOCK;
            actiontype = UserActionEvent.Action.RIGHT_CLICK;
        }
        UserActionEvent event = new UserActionEvent(u, action, actiontype, null, null, e.getClickedBlock());
        Bukkit.getPluginManager().callEvent(event);
        if(event.isCancelled())
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void userMove(PlayerMoveEvent e) {
        Player player = e.getPlayer(), p = e.getPlayer();
        User u = Miniverse.getUser(player);
        if(u == null) {
            MiniverseCore.getAPI().addUser(new UserBase(player));
            MiniverseCore.getAPI().addUser(new UserBase(player));
        }
        u = Miniverse.getUser(player);
        double toX = e.getTo().getX(), fromX = e.getFrom().getX();
        double toZ = e.getTo().getZ(), fromZ = e.getFrom().getZ();
        int toYaw = (int)e.getTo().getYaw(), fromYaw = (int)e.getFrom().getYaw();
        int toPitch = (int)e.getTo().getPitch(), fromPitch = (int)e.getFrom().getPitch();
        Block to = e.getTo().getBlock(), from = e.getFrom().getBlock(), toUnder = to.getRelative(0,-1,0);

        UserActionEvent.Action action = UserActionEvent.Action.NULL;
        UserActionEvent.Action actiontype = UserActionEvent.Action.NULL;

        if((toZ > fromZ || toZ < fromZ) && (toX > fromX || toX < fromX)) {
            action = UserActionEvent.Action.MOVE_X_Z;
            actiontype = UserActionEvent.Action.MOVE;
        } else if(toX > fromX || toX < fromX) {
            action = UserActionEvent.Action.MOVE_X;
            actiontype = UserActionEvent.Action.MOVE;
        } else if(toZ > fromZ || toZ < fromZ) {
            action = UserActionEvent.Action.MOVE_Z;
            actiontype = UserActionEvent.Action.MOVE;
        } else if(e.getTo().getY() < e.getFrom().getY()) {
            action = UserActionEvent.Action.MOVE_Y;
            actiontype = UserActionEvent.Action.FALL;
        } else if(to.getY() > from.getY() && (!toUnder.getType().isSolid() && toUnder.getType().name().contains("AIR"))) {
            action = UserActionEvent.Action.MOVE_Y;
            actiontype = UserActionEvent.Action.JUMP;
        } else if((toX > fromX || toX < fromX) && (to.getX() > from.getY() && (!toUnder.getType().isSolid() && toUnder.getType().name().contains("AIR")))) {
            action = UserActionEvent.Action.MOVE_X_Y;
            actiontype = UserActionEvent.Action.MOVE;
        } else if((toZ > fromZ || toZ < fromZ) && (to.getX() > from.getY() && (!toUnder.getType().isSolid() && toUnder.getType().name().contains("AIR")))) {
            action = UserActionEvent.Action.MOVE_Z_Y;
            actiontype = UserActionEvent.Action.MOVE;
        } else if((toYaw > fromYaw) && (toPitch > fromPitch)) {
            action = UserActionEvent.Action.PITCH_DOWN_YAW_RIGHT;
            actiontype = UserActionEvent.Action.PITCH_YAW;
        } else if((toYaw < fromYaw) && (toPitch < fromPitch)) {
            action = UserActionEvent.Action.PITCH_UP_YAW_LEFT;
            actiontype = UserActionEvent.Action.PITCH_YAW;
        } else if((toYaw > fromYaw) && (toPitch < fromPitch)) {
            action = UserActionEvent.Action.PITCH_UP_YAW_RIGHT;
            actiontype = UserActionEvent.Action.PITCH_YAW;
        } else if((toYaw < fromYaw) && (toPitch > fromPitch)) {
            action = UserActionEvent.Action.PITCH_DOWN_YAW_LEFT;
            actiontype = UserActionEvent.Action.PITCH_YAW;
        } else if((toPitch > fromPitch)) {
            action = UserActionEvent.Action.PITCH_DOWN;
            actiontype = UserActionEvent.Action.PITCH;
        } else if((toPitch < fromPitch)) {
            action = UserActionEvent.Action.PITCH_UP;
            actiontype = UserActionEvent.Action.PITCH;
        } else if((toYaw < fromYaw)) {
            action = UserActionEvent.Action.YAW_LEFT;
            actiontype = UserActionEvent.Action.YAW;
        } else if((toYaw > fromYaw)) {
            action = UserActionEvent.Action.YAW_RIGHT;
            actiontype = UserActionEvent.Action.YAW;
        }
        UserActionEvent event = new UserActionEvent(u, action, actiontype, to.getLocation(), from.getLocation(), u.getLocation().clone().subtract(0,1,0).getBlock());
        Bukkit.getPluginManager().callEvent(event);
        if(event.isCancelled())
            e.setCancelled(true);
    }

    public static void update(Player p) {
        afk.put(p, Miniverse.getSettings().getInt("Afk.Timer"));
    }

    @EventHandler
    void playerLogin(PlayerLoginEvent e) {
        Player p = e.getPlayer();
        User u = Miniverse.getUser(p);
        if(u == null) {
            u = new UserBase(p, true);
            if(Miniverse.getOfflineUser(p.getName()) != null)
                removeOfflineUser(u);
            addUser(u);
        }
        for(User us : getOnlineUsers()) {
            us.setVanish(us.isVanished());
        }
        if(Miniverse.getBan(u.getUUID()) != null) {
            BanData b = Miniverse.getBan(u.getUUID());
            if(b.getExpire() == -1) {

                String reason =
                        Miniverse.format(Miniverse.getSettings(), "BanReasonPrefix", "Never", b.getReason());
                e.disallow(PlayerLoginEvent.Result.KICK_OTHER, reason);
                e.setResult(PlayerLoginEvent.Result.KICK_OTHER);
                return;
            }
            if(TimeUtil.formatDateDiff((b.getExpire())) == "Now") {
                Miniverse.pardonPlayer(u.getUUID());
                return;
            }
            String reason = TimeUtil.removeTimePattern(
                    Miniverse.format(Miniverse.getSettings(), "BanReasonPrefix", TimeUtil.formatDateDiff(b.getExpire()), b.getReason()));
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, reason);
            e.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            return;
        }
        List<String> white = Miniverse.getWhiteList().getStringList("whitelisted");
        if(!white.contains(u.getUUID().toString()) && Miniverse.getWhiteList().getBoolean("Enabled")) {
            String kickMsg = Miniverse.getSettings().getString("Commands.Kick.kickPrefix");
            Miniverse.getSettings().set("Commands.Kick.kickPrefix", "");
            p.kickPlayer(Miniverse.getSettings().getString("Events.Connection.WhiteList.Join"));
            Miniverse.getSettings().set("Commands.Kick.kickPrefix", kickMsg);
            return;
        }
    }

    @EventHandler
    void onRename(PrepareAnvilEvent e) {
        User u = Miniverse.getUser(e.getView().getPlayer().getName());
        ItemStack r = e.getResult();
        if(r != null && u != null && e.getInventory().getRenameText() != null && e.getInventory().getRenameText() != StringUtils.getNameFromEnum(r.getType())) {
            ItemMeta ir = r.getItemMeta();
            if(ir != null) {
                ir.setDisplayName(StringUtils.translateUserPerms(u, "§r", e.getInventory().getRenameText()));
                r.setItemMeta(ir);
                r.setItemMeta(ItemEnchantUtil.fixEnchantments(r));
            }
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        User u = Miniverse.getUser(p);
        if(u == null) {
            u = new UserBase(p, true);
            if(Miniverse.getOfflineUser(p.getName()) != null)
                removeOfflineUser(u);
            addUser(u);
        }
        if(!u.getConfig().isSet("hasJoined")) {
            p.teleport(p.getWorld().getSpawnLocation());
            u.getConfig().set("hasJoined", true);
        }
        if(GroupManager.getGroups() != null && GroupManager.getGroups().size()>0)
            try{
                for(Group g : GroupManager.getGroups()) {
                    if(g.isDefault()) {
                        if(!e.getPlayer().hasPlayedBefore() || !u.getGroups().contains(g)) {
                            g.addUser(u);
                            GroupManager.reload();
                        }
                    }
                }
            }catch (Exception e2) {
                log(LogType.ERROR, "Error onJoin");
            }
        UserJoinEvent event = new UserJoinEvent(u, e.getJoinMessage());
        Bukkit.getPluginManager().callEvent(event);
        e.setJoinMessage(event.getMessage());
        if(PollManager.getActivePolls().size() > 0)
            for(Poll poll : PollManager.getActivePolls()) {
                Message msg = new Message("[c]Poll '[pc]" + poll.getName() + "[c]' Active! (" + TimeUtil.getFormatedTime(poll.getTime(), false) + ")");
                for(String a : poll.getTopics()) {
                    msg.then("\n &b-").then("&3"+a).tooltip("&3Click to vote for &b" + a).command("/poll vote " + poll.getName() + " " + a);
                }
            }
        p.setScoreboard(Miniverse.registerNewScoreBoard());
    }


    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        User u = Miniverse.getUser(p.getName());
        OfflineUser ou = new OfflineUserBase(p, true);
        u.getMiniUser().cancelAll = true;
        removeUser(u);
        addOfflineUser(ou);
        UserQuitEvent event = new UserQuitEvent(ou, e.getQuitMessage());
        Bukkit.getPluginManager().callEvent(event);
        e.setQuitMessage(event.getMessage());
        p.setScoreboard(Miniverse.registerNewScoreBoard());
    }
}
