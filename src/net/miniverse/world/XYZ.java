package net.miniverse.world;

import java.util.Objects;

public class XYZ
{
  public int x;
  public int y;
  public int z;
  
  public XYZ(int x, int y, int z)
  {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  

  public XYZ() {}
  

  public boolean equals(Object o)
  {
    if (this == o) return true;
    if ((o == null) || (getClass() != o.getClass())) return false;
    XYZ xyz = (XYZ)o;
    return (x == x) && (y == y) && (z == z);
  }
  
  public int hashCode()
  {
    return Objects.hash(new Object[] { Integer.valueOf(x), Integer.valueOf(y), Integer.valueOf(z) });
  }
}
