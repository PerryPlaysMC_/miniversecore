package net.miniverse.world;

import java.util.Random;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.util.noise.SimplexOctaveGenerator;

public class NoiseGenerator
{
  private static float[][][][][] grid;
  private static Random r;
  private static int density;
  private static int size;
  private static int zsize;
  private static int dimensions;
  private static boolean is2D;
  private static DistanceMetric metric;
  private static int level;
  static final int[][] order3D = { { 0, 0, 0 }, { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 }, { -1, 0, 0 }, { 0, -1, 0 }, { 0, 0, -1 }, { 1, 1, 0 }, { 1, 0, 1 }, { 0, 1, 1 }, { -1, 1, 0 }, { -1, 0, 1 }, { 0, -1, 1 }, { 1, -1, 0 }, { 1, 0, -1 }, { 0, 1, -1 }, { -1, -1, 0 }, { -1, 0, -1 }, { 0, -1, -1 }, { 1, 1, 1 }, { -1, 1, 1 }, { 1, -1, 1 }, { 1, 1, -1 }, { -1, -1, 1 }, { -1, 1, -1 }, { 1, -1, -1 }, { -1, -1, -1 } };
  
  static final int[][] order2D = { { 0, 0 }, { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 1 }, { -1, 1 }, { 1, -1 }, { -1, -1 } };
  
  public NoiseGenerator(int size, boolean is2D, long seed, int density, DistanceMetric metric, int level) {
    zsize = is2D ? 1 : size;
    dimensions = is2D ? 2 : 3;
    grid = new float[size][size][zsize][density][dimensions];
    r = new Random(seed);
    size = size;
    density = density;
    metric = metric;
    level = level;
    is2D = is2D;
    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++)
        for (int k = 0; k < zsize; k++)
          for (int d = 0; d < density; d++)
            for (int e = 0; e < dimensions; e++)
              grid[i][j][k][d][e] = r.nextFloat();
  }
  
  public static Chunk genFloor(int x, int z, Chunk c) {
    for (int y = 0; y < 5; y++) {
      if (y < 3) {
        c.getBlock(x, y, z).setType(Material.BEDROCK);
      } else {
        int rnd = new Random().nextInt(100);
        if (rnd < 40) {
          c.getBlock(x, y, z).setType(Material.BEDROCK);
        } else
          c.getBlock(x, y, z).setType(Material.STONE);
      }
    }
    return c;
  }
  
  public static int genHighlands(int x, int z, int xChunk, int zChunk, int current_height, SimplexOctaveGenerator gen) {
    if (current_height < 50) {
      return 0;
    }
    double noise = gen.noise((x + xChunk * 16) / 250.0F, (z + zChunk * 16) / 250.0F, 0.6D, 0.6D) * 25.0D;
    return (int)(34.0D + noise);
  }
  
  public static int genHills(int sz, int x, int z, int xChunk, int zChunk, int current_height, SimplexOctaveGenerator gen) {
    size = sz;
    double noise = gen.noise((x + xChunk * 16) / 250.0F, (z + zChunk * 16) / 250.0F, 0.6D, 0.6D) * 10.0D;
    return (int)(current_height - 2 + noise);
  }
  
  public static int genGround(int x, int z, int xChunk, int zChunk, int minHeight, SimplexOctaveGenerator gen) {
    gen.setScale(0.0078125D);
    double noise = gen.noise(x + xChunk * 16, z + zChunk * 16, 0.01D, 0.5D) * 20.0D;
    return (int)(minHeight + noise);
  }
  
  public static int genMountains(int sz, int x, int z, int xChunk, int zChunk, int current_height) {
    size = sz;
    is2D = true;
    double noise = get((x + xChunk * 16) / 250.0F, (z + zChunk * 16) / 250.0F) * 100.0F;
    int limit = (int)(current_height + noise);
    if (limit < 30) {
      return 0;
    }
    return limit;
  }
  
  private static float distance(float[] a, int[] offset, float[] b) {
    float[] m = new float[dimensions];
    for (int i = 0; i < dimensions; i++) {
      b[i] -= a[i] + offset[i];
    }
    float d = 0.0F;
    switch (metric.ordinal()) {
    case 1: 
      for (int i = 0; i < dimensions; i++)
        d += m[i] * m[i];
      return (float)Math.sqrt(d);
    case 2: 
      for (int i = 0; i < dimensions; i++)
        d += m[i] * m[i];
      return d;
    case 3: 
      for (int i = 0; i < dimensions; i++)
        d += Math.abs(m[i]);
      return d;
    case 4: 
      for (int i = 0; i < dimensions; i++)
        d = Math.max(Math.abs(m[i]), d);
      return d;
    case 5: 
      for (int i = 0; i < dimensions; i++)
        for (int j = i; j < dimensions; j++)
          d += m[i] * m[j];
      return d;
    case 6: 
      for (int i = 0; i < dimensions; i++)
        d = (float)(d + Math.pow(Math.abs(m[i]), 15.0D));
      return (float)Math.pow(d, 0.06666667014360428D);
    }
    return 1.0F;
  }
  
  public float get(float xin, float yin, float zin) {
    if (is2D) {
      throw new UnsupportedOperationException("Cannot create 3D Voronoi basis when instantiated with is2D = true.");
    }
    
    int[] cell = { fastfloor(xin), fastfloor(yin), fastfloor(zin) };
    float[] pos = { xin - cell[0], yin - cell[1], zin - cell[2] };
    for (int i = 0; i < 3; i++) { cell[i] %= size;
    }
    float[] distances = new float[level];
    for (int i = 0; i < level; i++) distances[i] = Float.MAX_VALUE;
    for (int i = 0; i < order3D.length; i++) {
      boolean possible = true;
      float farDist = distances[(level - 1)];
      if (farDist < Float.MAX_VALUE)
        for (int j = 0; j < 3; j++)
          if (((order3D[i][j] < 0) && (farDist < pos[j])) || ((order3D[i][j] > 0) && (farDist < 1.0F - pos[j])))
          {
            possible = false;
            break;
          }
      if (possible) {
        int cx = (order3D[i][0] + cell[0]) % size;
        int cy = (order3D[i][1] + cell[1]) % size;
        int cz = (order3D[i][2] + cell[2]) % size;
        for (int j = 0; j < density; j++) {
          float d = distance(grid[cx][cy][cz][j], order3D[i], pos);
          for (int k = 0; k < level; k++)
            if (d < distances[k]) {
              for (int l = level - 1; l > k; l--)
                distances[l] = distances[(l - 1)];
              distances[k] = d;
              break;
            }
        }
      }
    }
    return distances[(level - 1)];
  }
  
  public static float get(float xin, float yin) { if (!is2D) {
      throw new UnsupportedOperationException("Cannot create 2D Voronoi basis when instantiated with is2D = false.");
    }
    
    int[] cell = { fastfloor(xin), fastfloor(yin) };
    float[] pos = { xin - cell[0], yin - cell[1] };
    for (int i = 0; i < 2; i++) { cell[i] %= size;
    }
    float[] distances = new float[level];
    for (int i = 0; i < level; i++) distances[i] = Float.MAX_VALUE;
    for (int i = 0; i < order2D.length; i++) {
      boolean possible = true;
      float farDist = distances[(level - 1)];
      if (farDist < Float.MAX_VALUE)
        for (int j = 0; j < dimensions; j++)
          if (((order2D[i][j] < 0) && (farDist < pos[j])) || ((order2D[i][j] > 0) && (farDist < 1.0F - pos[j])))
          {
            possible = false;
            break;
          }
      if (possible) {
        int cx = (order2D[i][0] + cell[0] + size) % size;
        int cy = (order2D[i][1] + cell[1] + size) % size;
        for (int j = 0; j < density; j++) {
          float d = distance(grid[cx][cy][0][j], order2D[i], pos);
          for (int k = 0; k < level; k++)
            if (d < distances[k]) {
              for (int l = level - 1; l > k; l--)
                distances[l] = distances[(l - 1)];
              distances[k] = d;
              break;
            }
        }
      }
    }
    return distances[(level - 1)];
  }
  
  private static int fastfloor(float x) {
    return x > 0.0F ? (int)x : (int)x - 1;
  }
  
  public static enum DistanceMetric
  {
    Linear,  Squared,  Manhattan,  Quadratic,  Chebyshev,  Wiggly;
    
    private DistanceMetric() {}
  }
}
