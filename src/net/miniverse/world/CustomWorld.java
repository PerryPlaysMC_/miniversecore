package net.miniverse.world;

import org.bukkit.WorldCreator;

public class CustomWorld extends WorldCreator
{
  public CustomWorld(String name, org.bukkit.generator.ChunkGenerator gen)
  {
    super(name);
    generator(gen);
  }
}
