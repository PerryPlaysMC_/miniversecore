package net.miniverse.world.populators;

import java.util.Random;
import java.util.Set;
import net.miniverse.world.XYZ;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.generator.BlockPopulator;

public class CavePopulator extends BlockPopulator
{
  public CavePopulator() {}
  
  public void populate(World world, Random random, Chunk source)
  {
    if (random.nextInt(100) < 3) {
      int x = 4 + random.nextInt(8) + source.getX() * 16;
      int z = 4 + random.nextInt(8) + source.getZ() * 16;
      int maxY = world.getHighestBlockYAt(x, z);
      if (maxY < 16) {
        maxY = 32;
      }
      
      int y = random.nextInt(maxY);
      Set<XYZ> snake = selectBlocksForCave(world, random, x, y, z);
      buildCave(world, (XYZ[])snake.toArray(new XYZ[snake.size()]));
      for (XYZ block : snake) {
        world.unloadChunkRequest(x / 16, z / 16);
      }
    }
  }
  
  static Set<XYZ> selectBlocksForCave(World world, Random random, int blockX, int blockY, int blockZ) {
    Set<XYZ> snakeBlocks = new java.util.HashSet();
    
    int airHits = 0;
    XYZ block = new XYZ();
    
    while (airHits <= 1200)
    {
      if (random.nextInt(20) == 0) {
        blockY++;
      } else if (world.getBlockAt(blockX, blockY + 2, blockZ).getType() == Material.AIR) {
        blockY += 2;
      } else if (world.getBlockAt(blockX + 2, blockY, blockZ).getType() == Material.AIR) {
        blockX++;
      } else if (world.getBlockAt(blockX - 2, blockY, blockZ).getType() == Material.AIR) {
        blockX--;
      } else if (world.getBlockAt(blockX, blockY, blockZ + 2).getType() == Material.AIR) {
        blockZ++;
      } else if (world.getBlockAt(blockX, blockY, blockZ - 2).getType() == Material.AIR) {
        blockZ--;
      } else if (world.getBlockAt(blockX + 1, blockY, blockZ).getType() == Material.AIR) {
        blockX++;
      } else if (world.getBlockAt(blockX - 1, blockY, blockZ).getType() == Material.AIR) {
        blockX--;
      } else if (world.getBlockAt(blockX, blockY, blockZ + 1).getType() == Material.AIR) {
        blockZ++;
      } else if (world.getBlockAt(blockX, blockY, blockZ - 1).getType() == Material.AIR) {
        blockZ--;
      } else if (random.nextBoolean()) {
        if (random.nextBoolean()) {
          blockX++;
        } else {
          blockZ++;
        }
      }
      else if (random.nextBoolean()) {
        blockX--;
      } else {
        blockZ--;
      }
      
      if (world.getBlockAt(blockX, blockY, blockZ).getType() != Material.AIR) {
        int radius = 1 + random.nextInt(2);
        int radius2 = radius * radius + 1;
        for (int x = -radius; x <= radius; x++) {
          for (int y = -radius; y <= radius; y++) {
            for (int z = -radius; z <= radius; z++) {
              if ((x * x + y * y + z * z <= radius2) && (y >= 0) && (y < 128)) {
                if (world.getBlockAt(blockX + x, blockY + y, blockZ + z).getType() == Material.AIR) {
                  airHits++;
                } else {
                  x = (blockX + x);
                  y = (blockY + y);
                  z = (blockZ + z);
                  if (snakeBlocks.add(block)) {
                    block = new XYZ();
                  }
                }
              }
            }
          }
        }
      } else {
        airHits++;
      }
    }
    
    return snakeBlocks;
  }
  
  static void buildCave(World world, XYZ[] snakeBlocks) {
    for (XYZ l : snakeBlocks) {
      Block block = world.getBlockAt(l.x, l.y, l.z);
      if ((!block.isEmpty()) && (!block.isLiquid()) && (block.getType() != Material.BEDROCK)) {
        block.setType(Material.AIR);
      }
    }
  }
}
