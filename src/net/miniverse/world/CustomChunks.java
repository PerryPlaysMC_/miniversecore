package net.miniverse.world;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.*;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.noise.PerlinNoiseGenerator;
import org.bukkit.util.noise.PerlinOctaveGenerator;
import org.bukkit.util.noise.SimplexNoiseGenerator;
import org.bukkit.util.noise.SimplexOctaveGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SuppressWarnings("all")
public class CustomChunks extends ChunkGenerator {

    @Override
    public boolean canSpawn(World world, int x, int z) {
        return true;
    }

    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        List<BlockPopulator> populators = (new ArrayList<>());
        //populators.add(new GrassPopulator());
        //populators.add(new OrePopulator());
        //populators.add(new CavePopulator());
        //populators.add(new DungeonPopulator());
        //populators.add(new QuarryPopulator());
        //populators.add(new SpookyRoomPopulator());
        return populators;
    }

    ChunkData setBlock(int x, int y, int z, ChunkData c, Material m) {
        c.setBlock(x, y, z, m.createBlockData());
        return c;
    }

    Material getBlock(int x, int y, int z, ChunkData c) {
        return c.getType(x, y, z);
    }
    int ch = 50;

    @Override
    public ChunkData generateChunkData(World world, Random random, int cx, int cz, BiomeGrid biome) {
        ChunkData chunk = createChunkData(world);
        int minHeight = 69;

        Random r = new Random(world.getSeed());

        SimplexOctaveGenerator octave1 = new SimplexOctaveGenerator(r, 8);
        PerlinOctaveGenerator octave2 = new PerlinOctaveGenerator(r, 1);
        //Frequency higher = Bigger hills, Amplitude higher = idk yet
        octave1.setScale(1 / 64);
        octave2.setScale(1 / 48);
        int height, height2;
        for (int x=0;x<16;x++) {
            for (int z=0;z<16;z++) {
                height = (int)(octave1.noise((x+cx*16), (z+cz*16), 0.5, 0.5)*10D)+minHeight;
                height2 = (int)(octave2.noise((x+cx*16), (z+cz*16), 0.8, 0.5)*10D)+minHeight;
                switch(biome.getBiome(x,z)) {
                    case DESERT:
                        height = height-height2+height2/3;
                        chunk.setBlock(x,height,z, Material.SAND);
                        for(int y = height+-1; y>0; y++) {
                            chunk.setBlock(x,y,z,Material.STONE);
                        }
                        if(height+-4 > 0) {
                            chunk.setBlock(x, height + -1, z, Material.SAND);
                            chunk.setBlock(x, height + -2, z, Material.SAND);
                            chunk.setBlock(x, height + -3, z, Material.SAND);
                            chunk.setBlock(x, height + -4, z, Material.SAND);
                        }
                        break;
                    default:
                        chunk.setBlock(x,height,z, Material.GRASS_BLOCK);
                        for(int y = height+-1; y>0; y++) {
                            chunk.setBlock(x,y,z,Material.STONE);
                        }
                        if(height+-3 > 0) {
                            chunk.setBlock(x, height + -1, z, Material.DIRT);
                            chunk.setBlock(x, height + -2, z, Material.DIRT);
                            chunk.setBlock(x, height + -3, z, Material.DIRT);
                        }
                }
                chunk = genFloor(x,z, chunk);
            }
        }
        return chunk;
    }

    public ChunkData genFloor(int x, int z, ChunkData c) {
        for (int y = 0; y < 5; y++) {
            if(y < 3) {
                c.setBlock(x, y, z, Material.BEDROCK);
            } else {
                int rnd = new Random().nextInt(100);
                if(rnd < 40)
                    c.setBlock(x, y, z, Material.BEDROCK);
                else
                    c.setBlock(x, y, z, Material.STONE);
            }
        }
        return c;
    }

	/*

	gen = (int) (gener.noise(cx * 16 + x, cz * 16 + z, 0.5D, 0.5D) * 15D + minHeight);
				if(biome.getBiome(x,z).name().contains(Biome.MOUNTAINS.name()) ) {
					gener.setScale(0.06);
					gen = gen +5;//(int) (gener.noise(cx * 16 + x, cz * 16 + z, 50, 0.5D) * 8D + minHeight);
					if(e <=0.05) check = false;
					chunk.setBlock(x,gen,z, Material.GRASS_BLOCK);
					chunk.setBlock(x, 0, z, Material.BEDROCK);
					gener.setScale(0.005);
				}
				else if(biome.getBiome(x,z).name().contains(Biome.OCEAN.name()) ) {
					chunk.setBlock(x,gen,z, Material.AIR);
					//gener.setScale(0.008);
					gen = (int) (ocean.noise(cx * 16 + x, cz * 16 + z, 0.5D, 0.5D) * 12 + 63)*-1;
					for(int a = gen+1; a < 63;a++){
						chunk.setBlock(x,a,z, Material.WATER);
					}
					chunk.setBlock(x,gen,z, Material.GRAVEL);
					//gener.setScale(0.005);

				}else {
                    chunk.setBlock(x, gen, z, Material.GRASS_BLOCK);
                }
				for(int y = gen+-1; y > 0; y--) {
					chunk.setBlock(x,y,z, Material.STONE);
				}
				Random r = new Random();
				for(int i2 = 4; i2 > 10; i2++) {
					chunk.setBlock(x, gen + -i2, z, Material.COBBLESTONE);
					if(r.nextInt(100) < 30) {
						chunk.setBlock(x, gen + -i2, z, Material.GRAVEL);
					}
				}

				chunk.setBlock(x,gen +-1, z, Material.DIRT);
				chunk.setBlock(x,gen +-2, z, Material.DIRT);
				chunk.setBlock(x,gen +-3, z, Material.DIRT);
				chunk=genFloor(x,z,chunk);
	@Override
        public ChunkData generateChunkData(World world, Random random, int chunkx, int chunkz, BiomeGrid biome) {
            SimplexOctaveGenerator generator = new SimplexOctaveGenerator(new Random(world.getSeed()), 8);
            ChunkData chunk = createChunkData(world);
            int minHeight = 60;
            for (int x = 0; x < 16; x++) {
                for (int z = 0; z < 16; z++) {
                    ch = NoiseGenerator.genGround(x, z, chunkx, chunkz, 65, generator);
                    switch (biome.getBiome(x, z)) {
                        case MOUNTAINS:
                            //int x, int z, int xChunk, int zChunk, int current_height
                            //ch = NoiseGenerator.genMountains(25, x,z,chunkx,chunkz,ch)+-ch;

                            ch = (int) (generator.noise(chunkx * 16 + x, chunkz * 16 + z, 0.5D, 0.5D) * 15D + minHeight);
                            chunk.setBlock(x, ch, z, Material.GRASS_BLOCK);
                            chunk.setBlock(x, ch - 1, z, Material.DIRT);
                            for (int i = ch - 2; i > 0; i--)
                                chunk.setBlock(x, i, z, Material.STONE);
                            chunk.setBlock(x, 0, z, Material.BEDROCK);
                            break;
                        case GRAVELLY_MOUNTAINS:
                            ch = NoiseGenerator.genMountains(25, x,z,chunkx,chunkz,ch)+-ch;
                            for (int i = 0; i < 5; i++) {
                                chunk.setBlock(x, ch + -i, z, Material.GRAVEL);
                            }
                            for (int i = 5; i < 8; i++) {
                                chunk.setBlock(x, ch + -i, z, Material.SANDSTONE);
                            }
                            for (int i = ch - 2; i > 0; i--)
                                chunk.setBlock(x, i, z, Material.STONE);
                            chunk.setBlock(x, 0, z, Material.BEDROCK);
                            break;
                        case DESERT:
                            //ch = (NoiseGenerator.genHills(15,x,z,chunkx,chunkz, ch,generator))+-ch;
                            ch = (int) (generator.noise(chunkx * 16 + x, chunkz * 16 + z, 0.5D, 0.5D) * 15D + minHeight);
                            for (int i = 0; i < 5; i++) {
                                chunk.setBlock(x, ch + -i, z, Material.SAND);
                            }
                            for (int i = 5; i < 8; i++) {
                                chunk.setBlock(x, ch + -i, z, Material.SANDSTONE);
                            }
                            for (int i = ch - 2; i > 0; i--)
                                chunk.setBlock(x, i, z, Material.STONE);
                            chunk.setBlock(x, 0, z, Material.BEDROCK);
                            break;
                        case PLAINS:
                            ch = (NoiseGenerator.genHills(15, x,z,chunkx,chunkz, ch,generator))+-ch;
                            //ch = (int) (generator.noise(chunkx * 16 + x, chunkz * 16 + z, 0.2D, 0.2D) * 15D + minHeight);
                            chunk.setBlock(x, ch, z, Material.GRASS_BLOCK);
                            chunk.setBlock(x, ch - 1, z, Material.DIRT);
                            for (int i = ch - 2; i > 0; i--)
                                chunk.setBlock(x, i, z, Material.STONE);
                            chunk.setBlock(x, 0, z, Material.BEDROCK);
                            if(new Random().nextInt(50) <= 5) {
                                chunk.setBlock(x, ch + 1, z, Material.GRASS);
                            }
                            break;
                        case SUNFLOWER_PLAINS:
                            ch = (NoiseGenerator.genHills(15, x,z,chunkx,chunkz, ch,generator))+-ch;
                            //ch = (int) (generator.noise(chunkx * 16 + x, chunkz * 16 + z, 0.2D, 0.2D) * 15D + minHeight);
                            chunk.setBlock(x, ch, z, Material.GRASS_BLOCK);
                            chunk.setBlock(x, ch - 1, z, Material.DIRT);
                            for (int i = ch - 2; i > 0; i--)
                                chunk.setBlock(x, i, z, Material.STONE);
                            chunk.setBlock(x, 0, z, Material.BEDROCK);
                            if(new Random().nextInt(50) <= 5) {
                                chunk.setBlock(x, ch + 1, z, Material.GRASS);
                            }
                            if(new Random().nextInt(50) <= 15) {
                                chunk.setBlock(x, ch + 1, z, Material.SUNFLOWER);
                            }
                            break;
                        case RIVER:
                            //ch = (int) (generator.noise(chunkx * 16 + x, chunkz * 16 + z, 0.8D, 0.15D) * 15D + minHeight);
                            chunk.setBlock(x, ch, z, Material.WATER);
                            chunk.setBlock(x, ch+-1, z, Material.WATER);
                            for (int i = ch - 2; i > 0; i--)
                                chunk.setBlock(x, i, z, Material.STONE);
                            chunk.setBlock(x, 0, z, Material.BEDROCK);
                            if(new Random().nextInt(50) <= 5) {
                                chunk.setBlock(x, ch + 1, z, Material.GRASS);
                            }
                            break;
                        default:
                            ch = (int) (generator.noise(chunkx * 16 + x, chunkz * 16 + z, 0.5D, 0.5D) * 15D + minHeight) +- ch;
                            chunk.setBlock(x, ch, z, Material.GRASS_BLOCK);
                            chunk.setBlock(x, ch - 1, z, Material.DIRT);
                            for (int i = ch - 2; i > 0; i--)
                                chunk.setBlock(x, i, z, Material.STONE);
                            chunk.setBlock(x, 0, z, Material.BEDROCK);
                            break;
                    }
                }
            }

            return chunk;
        }
    */
    //	@Override
//	public ChunkData generateChunkData(World world, Random r, int chunkx, int chunkz, BiomeGrid biome) {
//		ChunkData chunk = createChunkData(world);
//		//	SimplexOctaveGenerator overhangs = new SimplexOctaveGenerator(world, 8);
//		//SimplexOctaveGenerator oceans = new SimplexOctaveGenerator(world, 8);
//		SimplexOctaveGenerator hills = new SimplexOctaveGenerator(world, 8);
//		SimplexOctaveGenerator land = new SimplexOctaveGenerator(world, 8);
//		land.setScale(0.005D);
//		hills.setScale(0.007D);
//		int ch = 0, bottom = 0;
//
//		for(int x = 0; x < 16; x++) {
//			for(int z = 0; z < 16; z++) {
//				ch = (int) (land.noise(chunkx+x*16, chunkz+z*16, 0.5D, 0.5D)*15D+50D);
////				if(biome.getBiome(x, z).name().contains("OCEAN")) {
////					ch = (int) (land.noise(cx*16+x, cz*16+z, 0.5D, 0.5D)*15D+25D);
////				}
//				if(biome.getBiome(x,z).name().contains("mountain")) {
//					ch = (int) (hills.noise(chunkx+x*16, chunkz+z*16, 0.5D, 0.5D)*15D+75D);
//				}
//				setBlock(x, ch, z, chunk, Material.GRASS_BLOCK);
//				setBlock(x, ch +-1, z, chunk, Material.DIRT);
//				setBlock(x, ch +-2, z, chunk, Material.DIRT);
//				setBlock(x, ch +-3, z, chunk, Material.DIRT);
//				for(int y = 0; y < ch +-3; y++) {
//					if(getBlock(x,y,z, chunk) == Material.AIR) {
//						setBlock(x,y,z, chunk, Material.STONE);
//					}
//				}
//				if(r.nextInt(15) < 4) {
//					bottom =2;
//				}
//
//				if(r.nextInt(15) < 3) {
//					bottom =1;
//				}
//
//				setBlock(x, 0, z, chunk, Material.BEDROCK);
//				setBlock(x, bottom, z, chunk, Material.BEDROCK);
//				setBlock(x, bottom, z, chunk, Material.BEDROCK);
//			}
//		}
//
//		return chunk;
//	}

    private static class GrassPopulator extends BlockPopulator {
        @Override
        public void populate(World w, Random r, Chunk c) {
            int x,y,z;
            for(x=0;x<16;x++){
                for(z=0;z<16;z++){
                    if(r.nextInt(100)<40){
                        y = w.getHighestBlockYAt(c.getX()+x,c.getZ()+z);
                        if(c.getBlock(x,y,z).getType() == Material.GRASS_BLOCK) {
                            if(new Random().nextInt(50) < 15) {

                                c.getBlock(x,y+1,z).setType(Material.TALL_GRASS);
                            }else {
                                c.getBlock(x, y + 1, z).setType(Material.GRASS);
                            }
                        }
                    }
                }
            }
        }
    }

    private static class OrePopulator extends BlockPopulator {

        @Override
        public void populate(World world, Random random, Chunk source)
        {
            int[] iterations = { 10, 20, 20, 2, 8, 1, 1, 1 };
            int[] amount = { 32, 16, 8, 8, 7, 7, 6 };
            Material[] type = { Material.GRAVEL, Material.COAL_ORE,
                    Material.IRON_ORE, Material.GOLD_ORE, Material.REDSTONE_ORE,
                    Material.DIAMOND_ORE, Material.LAPIS_ORE };
            int[] maxHeight = { 128, 128, 128, 128, 128, 64, 32, 16, 16,
                    32 };
            for (int i = 0; i < type.length; i++) {
                for (int j = 0; j < iterations[i]; j++) {
                    internal(world, random, source.getZ() * 16 + random.nextInt(16), random.nextInt(maxHeight[i]), source.getZ() *
                            16 + random.nextInt(16), amount[i], type[i]);
                }
            }
        }

        private static void internal(World world, Random random, int originx, int originY, int originz, int amount, Material type)
        {
            double angle = random.nextDouble() * 3.141592653589793D;
            double x1 = originx + 8 + Math.sin(angle) * amount / 8.0D;
            double x2 = originx + 8 - Math.sin(angle) * amount / 8.0D;
            double z1 = originz + 8 + Math.cos(angle) * amount / 8.0D;
            double z2 = originz + 8 - Math.cos(angle) * amount / 8.0D;
            double y1 = originY + random.nextInt(3) + 2;
            double y2 = originY + random.nextInt(3) + 2;
            for (int i = 0; i <= amount; i++)
            {
                double seedx = x1 + (x2 - x1) * i / amount;
                double seedY = y1 + (y2 - y1) * i / amount;
                double seedz = z1 + (z2 - z1) * i / amount;
                double size = ((Math.sin(i * 3.141592653589793D / amount) + 1.0D) *
                        random.nextDouble() * amount / 16.0D + 1.0D) / 2.0D;

                int startx = (int)(seedx - size);
                int startY = (int)(seedY - size);
                int startz = (int)(seedz - size);
                int endx = (int)(seedx + size);
                int endY = (int)(seedY + size);
                int endz = (int)(seedz + size);
                for (int x = startx; x <= endx; x++)
                {
                    double sizex = (x + 0.5D - seedx) / size;
                    sizex *= sizex;
                    if (sizex < 1.0D) {
                        for (int y = startY; y <= endY; y++)
                        {
                            double sizeY = (y + 0.5D - seedY) / size;
                            sizeY *= sizeY;
                            if (sizex + sizeY < 1.0D) {
                                for (int z = startz; z <= endz; z++)
                                {
                                    double sizez = (z + 0.5D - seedz) / size;
                                    sizez *= sizez;

                                    Block block = world.getBlockAt(x, y, z);
                                    if (block != null && (sizex + sizeY + sizez < 1.0D) &&
                                            (block.getType() == Material.STONE)) {
                                        block.setType(type);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private class LakePopulator extends BlockPopulator{
        @Override
        public void populate(World world, Random random, Chunk chunk)
        {
            if (random.nextInt(100) < 10) {  // The chance of spawning a lake
                Block block;
                int chunkx = chunk.getX();
                int chunkz = chunk.getZ();
                int x = chunkx * 16 + random.nextInt(15)-8;
                int z = chunkz * 16 + random.nextInt(15)-8;
                int Y;
                for (Y = world.getMaxHeight()-1; chunk.getBlock(x, Y, z).getType() == Material.AIR; Y--);
                Y -= 7;
                block = world.getBlockAt(z+8, Y, z+8);
                if (random.nextInt(100) < 90) block.setType(Material.WATER); else block.setType(Material.LAVA);  // The chance of spawing a water or lava lake
                boolean[] aboolean = new boolean[2048];
                boolean flag;
                int i = random.nextInt(4)+4;

                int j, j1, k1;

                for (j = 0; j < i; ++j) {
                    double d0 = random.nextDouble() * 6.0D + 3.0D;
                    double d1 = random.nextDouble() * 4.0D + 2.0D;
                    double d2 = random.nextDouble() * 6.0D + 3.0D;
                    double d3 = random.nextDouble() * (16.0D - d0 - 2.0D) + 1.0D + d0 / 2.0D;
                    double d4 = random.nextDouble() * (8.0D - d1 - 4.0D) + 2.0D + d1 / 2.0D;
                    double d5 = random.nextDouble() * (16.0D - d2 - 2.0D) + 1.0D + d2 / 2.0D;

                    for (int k = 1; k < 15; ++k) {
                        for (int l = 1; l < 15; ++l) {
                            for (int i1 = 1; i1 < 7; ++i1) {
                                double d6 = ((double) k - d3) / (d0 / 2.0D);
                                double d7 = ((double) i1 - d4) / (d1 / 2.0D);
                                double d8 = ((double) l - d5) / (d2 / 2.0D);
                                double d9 = d6 * d6 + d7 * d7 + d8 * d8;

                                if (d9 < 1.0D) {
                                    aboolean[(k * 16 + l) * 8 + i1] = true;
                                }
                            }
                        }
                    }
                }

                for (j = 0; j < 16; ++j) {
                    for (k1 = 0; k1 < 16; ++k1) {
                        for (j1 = 0; j1 < 8; ++j1) {
                            if (aboolean[(j * 16 + k1) * 8 + j1]) {
                                world.getBlockAt(x + j, Y + j1, z + k1).setType(j1>4 ? Material.AIR : block.getType());
                            }
                        }
                    }
                }

                for (j = 0; j < 16; ++j) {
                    for (k1 = 0; k1 < 16; ++k1) {
                        for (j1 = 4; j1 < 8; ++j1) {
                            if (aboolean[(j * 16 + k1) * 8 + j1]) {
                                int x1 = x+j;
                                int Y1 = Y+j1-1;
                                int z1 = z+k1;
                                if (world.getBlockAt(x1, Y1, z1).getType() == Material.DIRT) {
                                    world.getBlockAt(x1, Y1, z1).setType(Material.GRASS);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private class Populator extends BlockPopulator {

        @Override
        public void populate(World world, Random random, Chunk chunk) {
            if (random.nextBoolean()) {
                int amount = random.nextInt(4)+1;  // Amount of trees
                for (int i = 1; i < amount; i++) {
                    int x = random.nextInt(15);
                    int z = random.nextInt(15);
                    int Y = world.getHighestBlockYAt(x, z);
                    for(Y = 0; Y < world.getHighestBlockYAt(x, z); Y++) {
                        if(chunk.getBlock(x, Y, z).getType() == Material.AIR) {
                            Block b = chunk.getBlock(x, Y, z);
                            Generator.generateCustomTree(Generator.TreeTypes.BIG, b);
                            break;
                        }
                    }
                }
            }
        }
    }

    private class DungeonPopulator extends BlockPopulator {
        private SimplexNoiseGenerator simplex;
        private Random random;
        private World world;

        public void populate(World w, Random rnd, Chunk chunk)
        {
            this.simplex = new SimplexNoiseGenerator(rnd);
            this.random = rnd;
            this.world = w;
            for (int x = 0; x < 16; x++) {
                for (int z = 0; z < 16; z++)
                {
                    int cx = (chunk.getX() << 4) + x;
                    int cz = (chunk.getZ() << 4) + z;
                    int y = this.world.getHighestBlockYAt(cx, cz);
                    Block block = chunk.getBlock(x, y - 1, z);
                    if ((block.getType() == Material.STONE) && (this.random.nextInt(1024) == 0)) {
                        placeChest(block);
                    }
                }
            }
            double density = this.simplex.noise(chunk.getX() * 16, chunk.getZ() * 16);
            if (density > 0.8D)
            {
                int roomCount = (int)(density * 10.0D) - 3;
                for (int i = 0; i < roomCount; i++) {
                    if (this.random.nextBoolean())
                    {
                        int x = (chunk.getX() << 4) + this.random.nextInt(16);
                        int z = (chunk.getZ() << 4) + this.random.nextInt(16);
                        int y = 12 + this.random.nextInt(22);

                        int sizex = this.random.nextInt(12) + 5;
                        int sizeY = this.random.nextInt(6) + 4;
                        int sizez = this.random.nextInt(12) + 5;

                        generateRoom(x, y, z, sizex, sizeY, sizez);
                    }
                }
            }
        }

        private void generateRoom(int posx, int posY, int posz, int sizex, int sizeY, int sizez)
        {
            for (int x = posx; x < posx + sizex; x++) {
                for (int y = posY; y < posY + sizeY; y++) {
                    for (int z = posz; z < posz + sizez; z++) {
                        placeBlock(x, y, z, Material.AIR);
                    }
                }
            }
            int numSpawners = 1 + this.random.nextInt(2);
            for (int i = 0; i < numSpawners; i++)
            {
                int x = posx + this.random.nextInt(sizex);
                int z = posz + this.random.nextInt(sizez);
                placeSpawner(this.world.getBlockAt(x, posY, z));
            }
            int numChests = numSpawners + this.random.nextInt(2);
            for (int i = 0; i < numChests; i++)
            {
                int x = posx + this.random.nextInt(sizex);
                int z = posz + this.random.nextInt(sizez);
                placeChest(this.world.getBlockAt(x, posY, z));
            }
            for (int x = posx - 1; x <= posx + sizex; x++) {
                for (int z = posz - 1; z <= posz + sizez; z++)
                {
                    placeBlock(x, posY - 1, z, pickStone());
                    placeBlock(x, posY + sizeY, z, pickStone());
                }
            }
            for (int y = posY - 1; y <= posY + sizex; y++) {
                for (int z = posz - 1; z <= posz + sizez; z++)
                {
                    placeBlock(posx - 1, y, z, pickStone());
                    placeBlock(posx + sizex, y, z, pickStone());
                }
            }
            for (int x = posx - 1; x <= posx + sizex; x++) {
                for (int y = posY - 1; y <= posY + sizeY; y++)
                {
                    placeBlock(x, y, posz - 1, pickStone());
                    placeBlock(x, y, posz + sizez, pickStone());
                }
            }
        }

        private Material pickStone()
        {
            if (this.random.nextInt(6) == 0) {
                return Material.MOSSY_COBBLESTONE;
            }
            return Material.COBBLESTONE;
        }

        private void placeSpawner(Block block)
        {
            String[] types = {
                    "SKELETON", "zOMBIE", "CREEPER", "SPIDER" };

            block.setType(Material.LEGACY_MOB_SPAWNER);
            ((CreatureSpawner)block.getState()).setCreatureTypeByName(types[this.random.nextInt(types.length)]);
        }

        private void placeChest(Block block)
        {
            block.setType(Material.CHEST);
            Inventory chest = ((Chest)block.getState()).getInventory();
            for (int i = 0; i < 5; i++)
            {
                chest.setItem(this.random.nextInt(chest.getSize()), getRandomTool(i));
                if (i < 5) {
                    chest.setItem(this.random.nextInt(chest.getSize()), getRandomArmor(i));
                }
            }
            chest.setItem(this.random.nextInt(chest.getSize()), getRandomOre());
        }

        private ItemStack getRandomOre()
        {
            int i = this.random.nextInt(255);
            int count = this.random.nextInt(63) + 1;
            if (i > 253) {
                return new ItemStack(Material.LAPIS_BLOCK, count);
            }
            if (i > 230) {
                return new ItemStack(Material.DIAMOND_ORE, count);
            }
            if (i > 190) {
                return new ItemStack(Material.GOLD_ORE, count);
            }
            if (i > 150) {
                return new ItemStack(Material.IRON_ORE, count);
            }
            return new ItemStack(Material.COAL, count);
        }

        private ItemStack getRandomTool(int index)
        {
            int i = this.random.nextInt(255);
            if (i > 245) {
                return new ItemStack(getMaterial(276 + index), 1);
            }
            if (i > 230) {
                return new ItemStack(getMaterial(283 + index), 1);
            }
            if (i > 190)
            {
                if (index == 0) {
                    return new ItemStack(getMaterial(267), 1);
                }
                return new ItemStack(getMaterial(255 + index), 1);
            }
            if (i > 150) {
                return new ItemStack(getMaterial(272 + index), 1);
            }
            return new ItemStack(getMaterial(268 + index), 1);
        }

        public Material getMaterial(int id) {
            for(Material m : Material.values()) {
                if(m.getId() == id) {
                    return m;
                }
            }
            return Material.AIR;
        }

        private ItemStack getRandomArmor(int index)
        {
            int i = this.random.nextInt(255);
            if (i > 245) {
                return new ItemStack(getMaterial(310 + index), 1);
            }
            if (i > 230) {
                return new ItemStack(getMaterial(302 + index), 1);
            }
            if (i > 190) {
                return new ItemStack(getMaterial(314 + index), 1);
            }
            if (i > 150) {
                return new ItemStack(getMaterial(306 + index), 1);
            }
            return new ItemStack(getMaterial(298 + index), 1);
        }

        private void placeBlock(int x, int y, int z, Material mat)
        {
            if (canPlaceBlock(x, y, z)) {
                this.world.getBlockAt(x, y, z).setType(mat);
            }
        }

        private boolean canPlaceBlock(int x, int y, int z)
        {
            switch (this.world.getBlockAt(x, y, z).getType())
            {
                case AIR:
                case BLAZE_ROD:
                case LEGACY_BOAT:
                case BONE:
                case BOOK:
                case DIAMOND_BLOCK:
                case DIAMOND_CHESTPLATE:
                    return false;
            }
            return true;
        }
    }

    private class QuarryPopulator extends BlockPopulator {
        public void populate(World world, Random random, Chunk chunk)
        {
            if (random.nextInt(100) <= 95) {
                return;
            }
            Block block = chunk.getBlock(8, world.getHighestBlockYAt(chunk.getX() * 16 + 8, chunk.getZ() * 16 + 8), 8);
            int sizex = 5 + random.nextInt(6);
            int sizeY = 5 + random.nextInt(6);
            int sizez = 3 + random.nextInt(7);
            for (int y = 0; y <= sizez + 1; y++)
            {
                sizex -= y;
                sizeY -= y;
                for (int x = -sizex; x <= sizex; x++) {
                    for (int z = -sizeY; z <= sizeY; z++)
                    {
                        Block block2 = block.getRelative(x, -y - 1, z);
                        if ((block2.getType().getId() != 0) && ((block2.getType().getId() < 8) || (block2.getType().getId() > 11))) {
                            if (random.nextBoolean()) {
                                block2.setType(Material.COBBLESTONE);
                            } else {
                                block2.setType(Material.GRAVEL);
                            }
                        }
                        if ((random.nextBoolean()) &&
                                (!block.getRelative(x, -y, z).isLiquid())) {
                            block.getRelative(x, -y, z).setType(Material.AIR);
                        }
                    }
                }
            }
        }
    }

    private class SpookyRoomPopulator extends BlockPopulator {
        public void populate(World world, Random random, Chunk chunk)
        {
            if (random.nextInt(200) >= 1) {
                return;
            }
            int sizex = 16;
            int sizeY = 12;
            int sizez = 16;
            int spawnerChance = 400;
            Material matWalls = Material.NETHERRACK;
            Material matFloor = Material.SOUL_SAND;
            Material matDecor = Material.LEGACY_WEB;

            int lengthH = sizex / 2;
            int heightH = sizeY / 2;
            int widthH = sizez / 2;
            int centerx = chunk.getX() * 16 + 8;
            int centerY = world.getHighestBlockYAt(chunk.getX() * 16 + 8, chunk.getZ() * 16 + 8) / 2;
            int centerz = chunk.getZ() * 16 + 8;
            int minx = centerx - lengthH;
            int maxx = centerx + lengthH;
            int minY = centerY - heightH;
            int maxY = centerY + heightH;
            int minz = centerz - widthH;
            int maxz = centerz + widthH;
            for (int x = minx; x <= maxx; x++) {
                for (int y = minY; y <= maxY; y++)
                {
                    world.getBlockAt(x, y, minz).setType(matWalls);
                    world.getBlockAt(x, y, maxz).setType(matWalls);
                }
            }
            for (int y = minY; y <= maxY; y++) {
                for (int z = minz; z <= maxz; z++)
                {
                    world.getBlockAt(minx, y, z).setType(matWalls);
                    world.getBlockAt(maxx, y, z).setType(matWalls);
                }
            }
            for (int z = minz; z <= maxz; z++) {
                for (int x = minx; x <= maxx; x++)
                {
                    world.getBlockAt(x, minY, z).setType(matWalls);
                    world.getBlockAt(x, maxY, z).setType(matWalls);
                }
            }
            minx++;maxx--;
            minY++;maxY--;
            minz++;maxz--;
            for (int x = minx; x <= maxx; x++) {
                for (int y = minY; y <= maxY; y++)
                {
                    world.getBlockAt(x, y, minz).setType(pickDecor(random, matDecor, matWalls));
                    world.getBlockAt(x, y, maxz).setType(pickDecor(random, matDecor, matWalls));
                }
            }
            for (int y = minY; y <= maxY; y++) {
                for (int z = minz; z <= maxz; z++)
                {
                    world.getBlockAt(minx, y, z).setType(pickDecor(random, matDecor, matWalls));
                    world.getBlockAt(maxx, y, z).setType(pickDecor(random, matDecor, matWalls));
                }
            }
            for (int z = minz; z <= maxz; z++) {
                for (int x = minx; x <= maxx; x++)
                {
                    int floor = random.nextInt(spawnerChance);
                    Block block = world.getBlockAt(x, minY, z);
                    if (floor < 12)
                    {
                        block.setType(Material.LEGACY_MOB_SPAWNER);
                        CreatureSpawner spawner = (CreatureSpawner)block.getState();
                        if (floor <= 2) {
                            spawner.setCreatureTypeByName("zOMBIE");
                        } else if ((floor >= 3) && (floor <= 7)) {
                            spawner.setCreatureTypeByName("SPIDER");
                        } else if ((floor >= 8) && (floor <= 10)) {
                            spawner.setCreatureTypeByName("SKELETON");
                        } else {
                            spawner.setCreatureTypeByName("GHAST");
                        }
                    }
                    else
                    {
                        block.setType(matFloor);
                    }
                    world.getBlockAt(x, maxY, z).setType(matWalls);
                }
            }
            minx++;maxx--;
            minY++;maxY--;
            minz++;maxz--;
            for (int z = minz; z <= maxz; z++) {
                for (int x = minx; x <= maxx; x++) {
                    for (int y = minY; y <= maxY; y++)
                    {
                        Block block = world.getBlockAt(x, y, z);
                        if ((block.getRelative(BlockFace.DOWN).getType() != Material.AIR) && (block.getRelative(BlockFace.DOWN).getType() != matDecor))
                        {
                            int rand = random.nextInt(10);
                            if (rand <= 6) {
                                world.getBlockAt(x, y, z).setType(matFloor);
                            } else {
                                world.getBlockAt(x, y, z).setType(pickDecor(random, matDecor, Material.AIR));
                            }
                        }
                        else
                        {
                            world.getBlockAt(x, y, z).setType(Material.AIR);
                        }
                    }
                }
            }
        }

        private Material pickDecor(Random random, Material decor, Material wall)
        {
            return random.nextInt(5) == 0 ? decor : wall;
        }
    }



}