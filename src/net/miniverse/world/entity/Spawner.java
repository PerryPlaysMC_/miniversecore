package net.miniverse.world.entity;

import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class Spawner {

    public static Entity summon(Location loc, EntityType type)
    {
        return loc.getWorld().spawnEntity(loc, type);
    }

    public static Entity summonExact(Location loc, EntityType type) {
        return summon(new Location(loc.getWorld(), loc
                .getBlockX() + 0.5D, loc
                .getBlockZ() + 0.5D, loc
                .getBlockY() + 0.5D), type);
    }

    public static Entity summon(Location loc, EntityType type, Object path) {
        Entity e = loc.getWorld().spawnEntity(loc, type);
        if ((e instanceof Creature)) {

        }
        e.teleport(loc);
        return e;
    }

    public static Entity summonExact(Location loc, EntityType type, Object path) {
        return summon(new Location(loc.getWorld(), loc
                .getBlockX() + 0.5D, loc
                .getBlockZ() + 0.5D, loc
                .getBlockY() + 0.5D), type);
    }
}
