package net.miniverse.world;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

public class Generator
{
  public Generator() {}
  
  public static void generateCustomTree(TreeTypes type, Location loc)
  {
    generateCustomTree(type, loc.getBlock());
  }
  

  public static void generateCustomTree(TreeTypes type, Block block)
  {
    if (type == TreeTypes.BIG)
    {
      Location origin = block.getLocation();
      World world = origin.getWorld();
      Random rnd = new Random();
      int minHeight = 10;
      int maxHeight = 15;
      int treeHeight = rnd.nextInt(maxHeight - minHeight) + minHeight;
      Block top = world.getBlockAt(origin.getBlockX(), origin.getBlockY() + treeHeight, origin.getBlockZ());
      world.getBlockAt(origin.getBlockX() + 1, origin.getBlockY(), origin.getBlockZ()).setType(Material.LEGACY_LOG);
      world.getBlockAt(origin.getBlockX() + -1, origin.getBlockY(), origin.getBlockZ()).setType(Material.LEGACY_LOG);
      world.getBlockAt(origin.getBlockX(), origin.getBlockY(), origin.getBlockZ() + 1).setType(Material.LEGACY_LOG);
      world.getBlockAt(origin.getBlockX(), origin.getBlockY(), origin.getBlockZ() + -1).setType(Material.LEGACY_LOG);
      
      for (int i = 0; i < treeHeight; i++) {
        world.getBlockAt(origin.getBlockX(), origin.getBlockY() + i, origin.getBlockZ()).setType(Material.LEGACY_LOG);
        if (i <= treeHeight) {
          world.getBlockAt(origin.getBlockX() + 1, origin.getBlockY() + i, origin.getBlockZ()).setType(Material.LEGACY_LOG);
          world.getBlockAt(origin.getBlockX() + -1, origin.getBlockY() + i, origin.getBlockZ()).setType(Material.LEGACY_LOG);
          world.getBlockAt(origin.getBlockX(), origin.getBlockY() + i, origin.getBlockZ() + 1).setType(Material.LEGACY_LOG);
          world.getBlockAt(origin.getBlockX(), origin.getBlockY() + i, origin.getBlockZ() + -1).setType(Material.LEGACY_LOG);
        }
        if (i >= treeHeight - 1) {
          world.getBlockAt(origin.getBlockX() - 1, origin.getBlockY() + i, origin.getBlockZ()).setType(Material.LEGACY_LEAVES);
          world.getBlockAt(origin.getBlockX() + 1, origin.getBlockY() + i, origin.getBlockZ()).setType(Material.LEGACY_LEAVES);
          world.getBlockAt(origin.getBlockX(), origin.getBlockY() + i, origin.getBlockZ() - 1).setType(Material.LEGACY_LEAVES);
          world.getBlockAt(origin.getBlockX(), origin.getBlockY() + i, origin.getBlockZ() + 1).setType(Material.LEGACY_LEAVES);
        }
        
        if ((i >= treeHeight - 4) && (i < treeHeight - 2)) {
          for (Block b : getBlocks(top, 3)) {
            if (b.getType() == Material.AIR) {
              b.setType(Material.LEGACY_LEAVES);
            }
          }
        }
      }
      







      world.getBlockAt(origin.getBlockX(), origin.getBlockY() + treeHeight, origin.getBlockZ()).setType(Material.LEGACY_LEAVES);

































    }
    else if (type != TreeTypes.HUGE)
    {
      if (type != TreeTypes.SMALL) {}
    }
  }
  
  private static List<Block> getRanBlocks(Block b, int radius)
  {
    return getRanBlocks(b.getLocation(), radius);
  }
  
  private static List<Block> getBlocks(Block b, int radius) {
    return getBlocks(b.getLocation(), radius);
  }
  
  private static List<Block> getRanBlocks(Location location, int radius) {
    List<Block> blocks = new ArrayList();
    int random = new Random().nextInt(radius * 2);
    for (int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
      for (int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; y++) {
        for (int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
          if (random >= radius + 1) {
            blocks.add(location.getWorld().getBlockAt(x, y, z));
          }
        }
      }
    }
    
    return blocks;
  }
  
  private static List<Block> getBlocks(Location location, int radius) {
    List<Block> blocks = new ArrayList();
    for (int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
      for (int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; y++) {
        for (int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
          blocks.add(location.getWorld().getBlockAt(x, y, z));
        }
      }
    }
    
    return blocks;
  }
  
  public static enum TreeTypes {
    BIG,  SMALL,  HUGE;
    
    private TreeTypes() {}
  }
}
