package net.miniverse.anticheat;

import org.bukkit.Location;
import org.bukkit.event.player.PlayerMoveEvent;

public class Distance {
  private Location from;
  private Location to;
  private Double xDiff;
  private Double yDiff;
  private Double zDiff;
  
  public Distance(PlayerMoveEvent e) {
    from = e.getFrom();
    to = e.getTo();
    xDiff = (from.getX() > to.getX() ? from.getX() : to.getX()) - (from.getX() < to.getX() ? from.getX() : to.getX());
    yDiff = (from.getY() > to.getY() ? from.getY() : to.getY()) - (from.getY() < to.getY() ? from.getY() : to.getY());
    zDiff = (from.getZ() > to.getZ() ? from.getZ() : to.getZ()) - (from.getZ() < to.getZ() ? from.getZ() : to.getZ());
  }
  
  public Location getFrom() {
    return from;
  }
  
  public Location getTo()
  {
    return to;
  }
  
  public Double getxDiff()
  {
    return xDiff;
  }
  
  public Double getyDiff()
  {
    return yDiff;
  }
  
  public Double getzDiff()
  {
    return zDiff;
  }
}
