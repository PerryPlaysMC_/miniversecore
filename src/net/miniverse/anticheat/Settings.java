package net.miniverse.anticheat;

import java.util.HashMap;
import net.miniverse.anticheat.checks.CheckResult;
import net.miniverse.anticheat.listeners.MoveListener;
import net.miniverse.anticheat.listeners.PlayerListener;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.events.user.UserJoinEvent;
import net.miniverse.core.events.user.UserQuitEvent;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import net.miniverse.util.Listener;
import org.bukkit.event.EventHandler;

public class Settings implements Listener {
    protected static Settings inst;
    public static HashMap<java.util.UUID, CheckUser> USERS;
    public static final Double MAX_XZ_EATING_SPEED = 0.10177D;
    public static final Double MAX_FLY_XZ_SPEED = 1.09D;
    public static final Double MAX_XZ_SPEED = 0.66D;

    public Settings() {}

    public static void registerEvents() {
        inst = new Settings();
        MiniverseCore api = MiniverseCore.getAPI();
        api.registerListeners(new MoveListener(), new Settings(), new PlayerListener());
        USERS = new HashMap<>();
        for (User u : Miniverse.getOnlineUsers()) {
            if (!USERS.containsKey(u.getUUID())) {
                USERS.put(u.getUUID(), new CheckUser(u));
            }
        }
    }

    @EventHandler
    void onJoin(UserJoinEvent e) {
        if (!USERS.containsKey(e.getUser().getUUID())) {
            USERS.put(e.getUser().getUUID(), new CheckUser(e.getUser()));
        }
    }

    @EventHandler
    void onLeave(UserQuitEvent e) {
        OfflineUser u = e.getUser();
        USERS.remove(u.getUUID());
    }

    public static void log(CheckResult cr, CheckUser u) {
        Miniverse.broadCastPerm("anticheat.log", "[c]Bob: \n[c] -Check: [pc]" + u
                .getUser().getName() + "\n[c] -Hacking: [pc]" + cr
                .getLevel().toString().toLowerCase() + "\n[c] -Type: [pc]" + cr
                .getType().getName() + "\n[c] -Info: [pc]" + cr
                .getMessgae());
    }
}
