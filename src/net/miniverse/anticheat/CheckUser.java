package net.miniverse.anticheat;

import net.miniverse.user.User;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class CheckUser {

    private Player base;
    private User user;
    private Long foodStart = 0L;
    private int invalidFoodEatableCount = 0;
    private Location fstart;

    public CheckUser(User u) {
        user = u;
        base = u.getBase();
    }

    public Player getBase()
    {
        return base;
    }

    public User getUser() {
        return user;
    }

    public boolean isFoodStart() {
        return foodStart != 0L;
    }

    public void setFoodStart() {
        foodStart = System.currentTimeMillis();
        fstart = base.getLocation();
    }

    public Location getFoodStartLocation() {
        return fstart;
    }

    public Long getFoodStart() {
        return foodStart;
    }

    public void addInvalidFoodEatableCount() {
        invalidFoodEatableCount += 1;
    }

    public int getInvalidFoodEatableCount() {
        return invalidFoodEatableCount;
    }

    public void resetInvalidFoodEatableCount()
    {
        invalidFoodEatableCount = 0;
    }
}
