package net.miniverse.anticheat.checks.movement;

import net.miniverse.anticheat.CheckUser;
import net.miniverse.anticheat.Distance;
import net.miniverse.anticheat.Settings;
import net.miniverse.anticheat.checks.CheckResult;
import net.miniverse.anticheat.checks.CheckType;
import net.miniverse.anticheat.checks.Level;

public class NoSlowDown {

    private static final CheckResult PASS = new CheckResult(Level.PASSED, "", CheckType.NO_SLOW);

    public static void registerMove(Distance d, CheckUser u) {
        xzDist = d.getxDiff() > d.getzDiff() ? d.getxDiff() : d.getzDiff();
        if ((xzDist > Settings.MAX_XZ_EATING_SPEED) && (System.currentTimeMillis() + -u.getFoodStart() > 1200L)) {
            u.addInvalidFoodEatableCount();
        }
    }

    public static double xzDist;

    public static CheckResult runCheck(Distance d, CheckUser u) {
        return PASS;
    }
}
