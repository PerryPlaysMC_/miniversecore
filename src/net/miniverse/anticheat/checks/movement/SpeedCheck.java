package net.miniverse.anticheat.checks.movement;

import net.miniverse.anticheat.CheckUser;
import net.miniverse.anticheat.Distance;
import net.miniverse.anticheat.Settings;
import net.miniverse.anticheat.checks.CheckResult;
import net.miniverse.anticheat.checks.CheckType;
import net.miniverse.anticheat.checks.Level;

public class SpeedCheck {

    public static CheckResult runCheck(Distance d, CheckUser u) {
        Double xz_speed = d.getxDiff() > d.getzDiff() ? d.getxDiff() : d.getzDiff();
        if (!u.getUser().hasPermission(CheckType.SPEED.getPerm())) {
            if ((xz_speed > Settings.MAX_FLY_XZ_SPEED) && (u.getBase().isFlying())) {
                return new CheckResult(Level.DEFINITELY,
                        "Tried to fly faster than normal\n [c]-Normal speed=(" +
                                Settings.MAX_FLY_XZ_SPEED + "), their speed=(" + xz_speed + ")", CheckType.SPEED);
            }
            if ((xz_speed > Settings.MAX_XZ_SPEED) && (!u.getBase().isFlying())) {
                return new CheckResult(Level.DEFINITELY,
                        "Tried to move faster than normal\n [c]-Normal speed=([pc]" +
                                Settings.MAX_XZ_SPEED + "[c]), their speed=([pc]" + xz_speed + "[c])", CheckType.SPEED);
            }
        }

        return new CheckResult(Level.PASSED, "", CheckType.NULL);
    }
}
