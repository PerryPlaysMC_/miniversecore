package net.miniverse.anticheat.checks;

public enum Level {
    PROBABLY,  DEFINITELY,  PASSED
}
