package net.miniverse.anticheat.checks;

import org.bukkit.ChatColor;


public class CheckResult {
    private Level level;
    private String messgae;
    private CheckType type;

    public CheckResult(Level level, String messgae, CheckType type) {
        this.level = level;
        this.messgae = ChatColor.translateAlternateColorCodes('&', messgae);
        this.type = type;
    }

    public Level getLevel() {
        return level;
    }

    public String getMessgae() {
        return ChatColor.translateAlternateColorCodes('&', messgae);
    }

    public CheckType getType() {
        return type;
    }

    public boolean failed() {
        return level != Level.PASSED;
    }
}
