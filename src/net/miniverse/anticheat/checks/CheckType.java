package net.miniverse.anticheat.checks;

public enum CheckType {
    NULL("", ""),
    NO_SLOW("No Slowdown", "bypass.noslow"),
    SPEED("Speed", "bypass.speed");

    private String name;
    private String perm;

    private CheckType(String name, String perm) { this.name = name;
        this.perm = perm;
    }

    public String getName() {
        return name;
    }

    public String getPerm() {
        return perm;
    }
}
