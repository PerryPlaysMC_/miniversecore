package net.miniverse.anticheat.listeners;

import java.util.HashMap;
import net.miniverse.anticheat.CheckUser;
import net.miniverse.anticheat.Distance;
import net.miniverse.anticheat.Settings;
import net.miniverse.anticheat.checks.CheckResult;
import net.miniverse.anticheat.checks.movement.NoSlowDown;
import net.miniverse.anticheat.checks.movement.SpeedCheck;
import net.miniverse.core.Miniverse;
import net.miniverse.user.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveListener implements net.miniverse.util.Listener {
    public MoveListener() {}

    @EventHandler
    void onPlayerMove(PlayerMoveEvent e) {
        User us = Miniverse.getUser(e.getPlayer());
        if (Settings.USERS.containsKey(us.getUUID())) {
            CheckUser u = Settings.USERS.get(us.getUUID());

            Distance d = new Distance(e);
            NoSlowDown.registerMove(d, u);
            CheckResult result = SpeedCheck.runCheck(d, u);
            if (result.failed()) {
                e.setTo(e.getFrom());
                Settings.log(result, u);
            }
        }
    }
}
