package net.miniverse.anticheat.listeners;

import net.miniverse.anticheat.CheckUser;
import net.miniverse.anticheat.Settings;
import net.miniverse.anticheat.checks.CheckResult;
import net.miniverse.anticheat.checks.CheckType;
import net.miniverse.anticheat.checks.Level;
import net.miniverse.anticheat.checks.movement.NoSlowDown;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.PlayerInventory;

public class PlayerListener implements net.miniverse.util.Listener {
  
  @EventHandler
  void onInteract(PlayerInteractEvent e) {
    CheckUser u = Settings.USERS.get(e.getPlayer().getUniqueId());
    if ((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
      if ((e.getPlayer().getInventory().getItemInMainHand() != null) &&
        (e.getPlayer().getInventory().getItemInMainHand().getType().isEdible())) {
        u.setFoodStart();
      } else if ((e.getPlayer().getInventory().getItemInOffHand() != null) &&
        (e.getPlayer().getInventory().getItemInOffHand().getType().isEdible())) {
        u.setFoodStart();
      }
    }
  }
  
  @org.bukkit.event.EventHandler
  void onChange(FoodLevelChangeEvent e)
  {
    CheckUser u = Settings.USERS.get(e.getEntity().getUniqueId());
    if (((e.getEntity().getInventory().getItemInMainHand() != null) && 
      (e.getEntity().getInventory().getItemInMainHand().getType().isEdible())) || (
      (e.getEntity().getInventory().getItemInOffHand() != null) && 
      (e.getEntity().getInventory().getItemInOffHand().getType().isEdible()))) {
      if ((u.getInvalidFoodEatableCount() > 0) && (u.isFoodStart())) { if (!u.getUser().hasPermission("noSlowDown")) {
          e.setCancelled(true);
          u.getBase().teleport(u.getFoodStartLocation());
          Settings.log(new CheckResult(Level.DEFINITELY,
                  "Tried to move too fast whilst eating\n [c]-[pc]" + u.getInvalidFoodEatableCount() +
                          " [c]times max=([pc]0[c])\n [c]-NormalSpeed=([pc]" + Settings.MAX_XZ_EATING_SPEED +
                          "[c]), Their Speed=([pc]" + NoSlowDown.xzDist + "[c])", CheckType.NO_SLOW), u);
        }
      }
      u.resetInvalidFoodEatableCount();
    }
  }
}
