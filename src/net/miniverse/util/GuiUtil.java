package net.miniverse.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class GuiUtil {
	
	
	public static ItemStack createItem(Material mat, String name, int amount, int metaID, String... lore) {
		ItemStack i = new ItemStack(mat, amount, (short) metaID);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(StringUtils.translate(name));
		List<String> newLore = new ArrayList<>();
		for(String nL : lore) {
			newLore.add(StringUtils.translate(nL));
		}
		im.setLore(newLore);
		im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		i.setItemMeta(im);
		return i;
	}
	
	public static ItemStack createItem(Material mat, String name, int amount, String... lore) {
		ItemStack i = new ItemStack(mat, amount);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(StringUtils.translate(name));
		List<String> newLore = new ArrayList<>();
		for(String nL : lore) {
			newLore.add(StringUtils.translate(nL));
		}
		im.setLore(newLore);
		im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		i.setItemMeta(im);
		return i;
	}
	public static ItemStack createItem(Material mat, String name, int amount) {
		ItemStack i = new ItemStack(mat, amount);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(StringUtils.translate(name));
		im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack createItem(Material mat, int amount, String... lore) {
		ItemStack i = new ItemStack(mat, amount);
		ItemMeta im = i.getItemMeta();
		List<String> newLore = new ArrayList<>();
		for(String nL : lore) {
			newLore.add(StringUtils.translate(nL));
		}
		im.setLore(newLore);
		im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		i.setItemMeta(im);
		return i;
	}
	
	public static ItemStack createItem(Material mat, String name, int amount, List<String> lore) {
		return createItem(mat, name, amount, lore.toArray(new String[lore.size()]));
	}
	

}
