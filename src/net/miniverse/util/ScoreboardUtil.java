package net.miniverse.util;

import net.miniverse.core.Miniverse;
import org.apache.commons.lang.Validate;
import org.bukkit.scoreboard.*;

public class ScoreboardUtil {

    private Scoreboard b;
    private Objective ob;
    private ScoreboardUtil inst;
    private String name, SBcriteria, display;

    public ScoreboardUtil(String name, String SBcriteria) {
        this.inst = this;
        b = Miniverse.registerNewScoreBoard();
        ob = b.getObjective(name) == null ? b.registerNewObjective(name, SBcriteria) : b.getObjective(name);
        ob.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.name= name;
        this.SBcriteria = SBcriteria;
    }
    public ScoreboardUtil(Scoreboard sb, String name, String SBcriteria) {
        this.inst = this;
        b = sb;
        ob = b.getObjective(name) == null ? b.registerNewObjective(name, SBcriteria) : b.getObjective(name);
        ob.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.name= name;
        this.SBcriteria = SBcriteria;
    }

    public ScoreboardUtil(String name, String SBcriteria, String display) {
        this.inst = this;
        b = Miniverse.registerNewScoreBoard();
        ob = b.getObjective(name) == null ? b.registerNewObjective(name,SBcriteria, StringUtils.translate(display)) : b.getObjective(name);
        ob.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.name= name;
        this.display = display;
        this.SBcriteria = SBcriteria;
    }

    public ScoreboardUtil reset() {
        return new ScoreboardUtil(name, SBcriteria, display);
    }


    public ScoreboardUtil setDisplay(String name, DisplaySlot slot) {
        ob.setDisplayName(StringUtils.translate(name));
        ob.setDisplaySlot(slot);
        return this;
    }
    public ScoreboardUtil setDisplay(String name) {
        ob.setDisplayName(StringUtils.translate(name));
        return this;
    }

    public ScoreboardUtil addObjective(String name, String criteria) {
        if(b.getObjective(name) == null)
            ob = b.registerNewObjective(name, criteria);
        else
            ob = b.getObjective(name);
        return this;
    }

    public ScoreboardUtil setScore(int i, String id, String score) {
        Validate.notNull(ob);
        setTeam(id, score);
        Score s = ob.getScore(t(id));
        s.setScore(i);
        return this;
    }

    public ScoreboardUtil setScoreboard(Scoreboard b) {
        this.b = b;
        return this;
    }

    public Team getTeam(String team) {
        return b.getTeam(StringUtils.translate(team));
    }

    public ScoreboardUtil setTeam(int i, String id, String prefix, String suffix) {
        Validate.notNull(ob);
        Team t = b.getTeam(id);
        if(t == null) {
            t = b.registerNewTeam(id);
        }
        t.addEntry(t(id));
        t.setPrefix(t(prefix));
        t.setSuffix(t(suffix));
        if(!ob.getScore(t(id)).isScoreSet())
            ob.getScore(t(id)).setScore(i);
        return this;
    }
    public ScoreboardUtil setTeam(String id, String prefix, String suffix) {
        Validate.notNull(ob);
        Team t = b.getTeam(t(id));
        if(t == null) {
            if(b.getEntryTeam(t(id))!=null) {
                t = b.getEntryTeam(t(id));
            }else
                t = b.registerNewTeam(t(id));
        }
        t.setPrefix(t(prefix));
        t.setSuffix(t(suffix));
        return this;
    }

    public ScoreboardUtil setTeam(int i, String teamId, String id, String prefix, String suffix) {
        Validate.notNull(ob);
        Team t = b.getTeam(teamId);
        if(t == null) {
            t = b.registerNewTeam(teamId);
        }else{}
        t.addEntry(t(id));
        t.setPrefix(t(prefix));
        t.setSuffix(t(suffix));
        ob.getScore(t(id)).setScore(i);
        return this;
    }

    public ScoreboardUtil setTeam(String team, String score) {
        Validate.notNull(ob);
        Team t;
        if(b.getTeam(team)!=null){
            b.getTeam(team).unregister();
        }
        t = b.registerNewTeam(team);
        t.addEntry(StringUtils.translate(score));
        t.setPrefix(t(score));
        return this;
    }

    private String t(String a) {
        return StringUtils.translate(a);
    }

    public Scoreboard finish() {
        return b;
    }

    void setInstance(ScoreboardUtil u) {
        inst = u;
    }


}
