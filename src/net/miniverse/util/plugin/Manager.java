package net.miniverse.util.plugin;

import java.io.File;
import java.util.List;

import org.bukkit.event.Event;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;

public abstract interface Manager
{
  public abstract PluginManager getPluginManager();
  
  public abstract Plugin getPlugin(String paramString);
  
  public abstract List<Plugin> getPlugins();
  
  public abstract PluginDescriptionFile getPluginDescription(File paramFile)
    throws InvalidDescriptionException;
  
  public abstract String load(String paramString);
  
  public abstract String load(String paramString1, String paramString2);
  
  public abstract String unload(Plugin paramPlugin);
  
  public abstract List<Plugin> getMiniversePlugins();
  
  public abstract boolean isMiniverse(Plugin paramPlugin);
  
  public abstract boolean isEnabled(Plugin paramPlugin);
  
  public abstract int getPluginSize();

  void callEvent(Event e);
}
