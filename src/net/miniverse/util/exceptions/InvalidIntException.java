package net.miniverse.util.exceptions;

public class InvalidIntException extends Exception
{
  private String number;
  
  public InvalidIntException(String number) {
    this.number = number;
  }
  
  public String getNumber() {
    return number;
  }
}
