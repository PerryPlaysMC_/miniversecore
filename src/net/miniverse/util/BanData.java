package net.miniverse.util;

import net.miniverse.core.Miniverse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import  net.miniverse.core.Miniverse.BanType;

public class BanData {

    private String reason, by;
    private UUID user;
    private long expire;
    private BanType type;
    private SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public BanData(BanType type, String by, UUID user, String reason, long expire) {
        this.type = type;
        this.reason = reason;
        this.by = by;
        this.user = user;
        this.expire = expire;
        if(type == BanType.IP) {
                Miniverse.getBannedPlayers()
                        .set("Banned."+user+".BannedBy", by)
                        .set("Banned."+user+".BannedOn", new Date().getTime())
                        .set("Banned."+user+".Expires", format.format(new Date(expire)))
                        .set("Banned."+user+".Reason", reason);
            return;
        }
        Miniverse.getBannedPlayers()
                .set("Banned."+user+".BanType", type.name())
                .set("Banned."+user+".BannedBy", by)
                .set("Banned."+user+".BannedOn", new Date().getTime())
                .set("Banned."+user+".Expires", format.format(new Date(expire)))
                .set("Banned."+user+".Reason", reason);
    }
    public BanData(BanType type, String by, UUID user, String reason) {
        this.type = type;
        this.reason = reason;
        this.by = by;
        this.user = user;
        this.expire = -1;
        if(type == BanType.IP) {
            Miniverse.getBannedPlayers()
                    .set("Banned."+user+".BannedBy", by)
                    .set("Banned."+user+".BannedOn", new Date().getTime())
                    .set("Banned."+user+".Expires", "Forever")
                    .set("Banned."+user+".Reason", reason);
            return;
        }
        Miniverse.getBannedPlayers()
                .set("Banned."+user+".BanType", type.name())
                .set("Banned."+user+".BannedBy", by)
                .set("Banned."+user+".BannedOn", new Date().getTime())
                .set("Banned."+user+".Expires", "Forever")
                .set("Banned."+user+".Reason", reason);
    }


    public long getExpire() {
        return expire;
    }

    public void setExpire(long expire) {
        this.expire = expire;
       Miniverse.getBannedPlayers()
                .set("Banned."+user+".Expires", format.format(new Date(expire)));
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
        Miniverse.getBannedPlayers()
                .set("Banned."+user+".Reason", reason);
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
        Miniverse.getBannedPlayers()
                .set("Banned." + user + ".BannedBy", by);
    }

    public UUID getUser() {
        return user;
    }

    public String toString() {
        return "BanData=(user=" + Miniverse.getOfflinePlayer(user).getName() + ",reason='" + reason + "',expire=" + expire + ",by="+by+")";
    }
}
