package net.miniverse.util;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.*;

@SuppressWarnings("all")
public class NumberUtil {
    final static char symbol[] = {'M', 'D', 'C', 'L', 'X', 'V', 'I'};
    final static int value[] = {1000, 500, 100, 50, 10, 5, 1};

    private static double calc2(String s) {
        double d =  0;
        int start = 0;
        char[] chars = s.toCharArray();
        for(int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(c == '+' || c == '-'
                    || c == '*' || c == '/') {
                start = i;
                break;
            }
        }
        String a = "";
        for(int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(c == '+' || c == '-'
                    || c == '*' || c == '/')break;
            a+=c;
        }
        d = getDouble(a);
        for(int i = start; i < chars.length; i++) {
            char pr = ' ';
            char nxt = ' ';
            if(i+1 <= chars.length+-1) nxt = chars[i+1];
            if(i+-1 > start) pr = chars[i+-1];
            if((pr == '+' || pr == '-' || pr == '/' || pr == '*') && nxt == '-')continue;
            char c = chars[i];
            if(c == '+') {
                String x = "";
                boolean isNegative = false;
                A:for(int j = i+1; j < chars.length; j++) {
                    if(j == i+1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '/') {
                        break A;
                    }
                    x+=chars[j];
                }
                d+=isNegative ? -getDouble(x) : getDouble(x);
            }
            else if(c == '-') {
                String x = "";
                boolean isNegative = false;
                A:for(int j = i+1; j < chars.length; j++) {
                    if(j == i+1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '/') {
                        break A;
                    }
                    x+=chars[j];
                }
                d-=isNegative ? -getDouble(x) : getDouble(x);
            }
            else if(c == '/') {
                String x = "";
                boolean isNegative = false;
                A:for(int j = i+1; j < chars.length; j++) {
                    if(j == i+1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '/') {
                        break A;
                    }
                    x+=chars[j];
                }
                d/=isNegative ? -getDouble(x) : getDouble(x);
            }
            else if(c == '*') {
                String x = "";
                boolean isNegative = false;
                A:for(int j = i+1; j < chars.length; j++) {
                    if(j == i+1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '/') {
                        break A;
                    }
                    x+=chars[j];
                }
                d*=isNegative ? -getDouble(x) : getDouble(x);
            }

        }

        return d;
    }

    public static double calc(String s) {
        char[] chars = s.toCharArray();
        double d = 0;
        int start = 0;
        for(int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(c == '(' && i == 0)continue; else if(i > 0 && c == '(')break;
            if(c == '+' || c == '-'
                    || c == '*' || c == '/' || c == ')') {
                if(c == ')') {
                    start = i+1;
                }else start = i;

                break;
            }
        }
        String a = "";
        boolean calcStr = false;
        for(int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(c == '(' && i == 0)continue; else if(i > 0 && c == '(')break;
            if(c == '('){
                calcStr=true;
                continue;
            }
            if(c == ')') break;
            if(!calcStr) {
                if(c == '+' || c == '-'
                        || c == '*' || c == '/')break;
            }
            a+=c;
        }
        if(calcStr) {
            d = calc2(a);
        }else d = getDouble(a);
        if(start == (calcStr ? chars.length+-2 : chars.length) || start == (calcStr ? chars.length+-2 : chars.length)+-1) return d;
        boolean m = false, ad = false, su = false, div = false;
        String f = "";
        for(int i = start; i < chars.length; i++) {
            char c = chars[i];
            char pr = ' ';
            char nxt = ' ';
            if(i+1 <= chars.length+-1) nxt = chars[i+1];
            if(i+-1 > start) pr = chars[i+-1];
            if((pr == '+' || pr == '-' || pr == '/' || pr == '*') && nxt == '-')continue;
            if(c == '(') {
                char v = chars[i+-1];
                if(v == '+')
                    ad=true;
                else if(v == '-')
                    su=true;
                else if(v == '/')
                    div=true;
                else if(v == '*')
                    m=true;
                else m = true;
                continue;
            }
            if(m || ad || su || div) {
                if(c == ')') {
                    if(!f.isEmpty()) {
                        if(m)
                            d*=calc2(f);
                        if(ad)
                            d+=calc2(f);
                        if(su)
                            d-=calc2(f);
                        if(div)
                            d/=calc2(f);
                    }
                    m = false;
                    ad = false;
                    su = false;
                    div = false;
                    f = "";
                    continue;
                }
                if(c != '(' && c != ')')
                    f+=c;
                continue;
            }
            if(i < chars.length+-2&&(""+chars[i+1])!=null && chars[i+1] == '(') continue;
            if(c == '+') {
                String x = "";
                boolean isNegative = false;
                A:for(int j = i+1; j < chars.length; j++) {
                    if(j == i+1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '/' || chars[j] == ')') {
                        break A;
                    }
                    x+=chars[j];
                }
                System.out.println(x);
                d+=isNegative ? -getDouble(x) : getDouble(x);
            }
            else if(c == '-') {
                String x = "";
                boolean isNegative = false;
                A:for(int j = i+1; j < chars.length; j++) {
                    if(j == i+1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '/' || chars[j] == ')') {
                        break A;
                    }
                    x+=chars[j];
                }
                d-=isNegative ? -getDouble(x) : getDouble(x);
            }
            else if(c == '/') {
                String x = "";
                boolean isNegative = false;
                A:for(int j = i+1; j < chars.length; j++) {
                    if(j == i+1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '/' || chars[j] == ')') {
                        break A;
                    }
                    x+=chars[j];
                }
                d/=isNegative ? -getDouble(x) : getDouble(x);
            }
            else if(c == '*') {
                String x = "";
                boolean isNegative = false;
                A:for(int j = i+1; j < chars.length; j++) {
                    if(j == i+1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '/' || chars[j] == ')') {
                        break A;
                    }
                    x+=chars[j];
                }
                d*=isNegative ? -getDouble(x) : getDouble(x);
            }

        }

        return d;
    }

   public static int intToRoman(String roman) {
        roman = roman.toUpperCase();
        if(roman.length() == 0) {
            return 0;
        }
        for (int i = 0; i < symbol.length; i++) {
            int pos = roman.indexOf(symbol[i]);
            if(pos >= 0) {
                return value[i] - intToRoman(roman.substring(0, pos)) + intToRoman(roman.substring(pos + 1));
            }
        }
        throw new IllegalArgumentException("Invalid Roman Symbol.");
    }

    private static int[] numbers = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

    private static String[] letters = {"M", "CM", "D", "CD", "C", "XC",
            "L", "XL", "X", "IX", "V", "IV", "I"};

    public static double getPercent(Double current, Double max) {
        if((max == null || max == 0) || (current == null || current == 0)) {
            return 0;
        }
        Double bottom = max;
        Double top = current;
        Double percent = (top * 100 / bottom);
        String p = (percent + "").replace("-", "");
        return percent;
    }

    public static int newRandomInt(int range) {
        return new Random().nextInt(range);
    }
    public static double newRandomDouble(double max) {
        return newRandomDouble(0, max);
    }

    public static double newRandomDouble(double min, double max) {
        Random r = new Random();
        double rv = min + (max - min) * r.nextDouble();
        return rv;
    }

    public static int getNumDrops(ItemStack item) {
        if(!item.containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) return 1;
        int i = item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
        if(i <= 0) {
            return 1;
        }
        int j = 2;
        int k = 35 - i * 5;
        if(i > 100) {
            j = (int) (j + Math.round(i - 40.0D));
        }
        if(i > 50) {
            k = 1;
        } else if(i > 25) {
            k = 2;
        } else if(i > 15) {
            k = 3;
        } else if(i > 8) {
            k = 4;
        }
        if(k < 5) {
            i = 100;
        }
        int l = k;
        int i1 = new Random().nextInt(100) + 1;
        while (l <= k * (i + 1)) {
            if(i1 <= l) {
                return j;
            }
            l += k;
            j++;
        }
        return 1;
    }

    public static boolean isEven(int i) {
        if((i % 2) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isInt(String is) {
        try {
            Integer.parseInt(is);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static long getLong(String is) {
        try {
            return Long.parseLong(is);
        } catch (Exception e) {
            return -1;
        }
    }

    public static String format(double d) {
        DecimalFormat df = new DecimalFormat("###,###.00");
        String x = df.format(d);
        try {
            return x.charAt(x.length()+-1) == '0' && x.charAt(x.length()+-2) == '0' ? x.substring(0, x.length()+-3) : x;
        }catch(Exception e) {
            return x;
        }
    }

    public static boolean isDouble(String is) {
        try {
            Double.parseDouble(is);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String convertToRoman(int N) {
        String roman = "";
        for (int i = 0; i < numbers.length; i++) {
            while (N >= numbers[i]) {
                roman += letters[i];
                N -= numbers[i];
            }
        }
        return roman;
    }

    public static Double getMoney(String intToGet) {
        double ret = 0;
        try {
            String era = intToGet.contains(".") ? intToGet.replace(".", ",").split(",")[0] : intToGet;

            ret = ret + Double.parseDouble(era);
            if((intToGet).contains(".")) {
                String a = intToGet.replace(".", ",");
                String re = (a).split(",")[1];
                if(Long.parseLong(re) >= Integer.MAX_VALUE) return 0d;
                int er = Integer.parseInt(re);
                if(er > 100) {
                    double amount = 0;
                    if(er >= 100) {
                        while(er >= 100) {
                            amount++;
                            er = er + -100;
                        }
                    }
                    if(er < 100) {
                        if(er < 10) {
                            String ea = amount + "";
                            if(("" + amount).contains(".")) {
                                ea = ("" + amount).replace(".", ",").split(",")[0];
                            }
                            amount = Double.parseDouble(ea + ".0" + er);
                        }else {
                            String ea = amount + "";
                            if(("" + amount).contains(".")) {
                                ea = ("" + amount).replace(".", ",").split(",")[0];
                            }
                            if((er+"").charAt(1) == '0')
                                amount = Double.parseDouble(ea + "." + er + "0");
                            else
                                amount = Double.parseDouble(ea + "." + er);
                        }
                    }
                    ret += amount;
                }else return Double.parseDouble(intToGet);
            }
        } catch (Exception e) {
            ret = 0;
            e.printStackTrace();
        }
        return ret;
    }

    public static Double getDouble(String intToGet) {
        double ret = 0;
        try {
            ret = Double.parseDouble(intToGet);
        } catch (Exception e) {
            ret = 0;
        }
        return ret;
    }

    public static Integer getInt(String intToGet) {
        int ret = 1;
        try {
            ret = Integer.parseInt(intToGet);
        } catch (Exception e) {
        }
        return ret;
    }

    public static boolean isLong(String intToGet) {
        try {
            Long.parseLong(intToGet);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
