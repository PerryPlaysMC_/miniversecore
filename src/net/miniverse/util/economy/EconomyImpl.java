package net.miniverse.util.economy;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.miniverse.core.Miniverse;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import org.bukkit.OfflinePlayer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class EconomyImpl implements Economy {
    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getName() {
        return "MiniverseEconomy";
    }

    @Override
    public boolean hasBankSupport() {
        return false;
    }

    @Override
    public int fractionalDigits() {
        return 0;
    }

    @Override
    public String format(double v) {
        return "$" + new DecimalFormat("###,###.##").format(v);
    }

    @Override
    public String currencyNamePlural() {
        return "$";
    }

    @Override
    public String currencyNameSingular() {
        return "$";
    }

    @Override
    public boolean hasAccount(String s) {
        return hasAccount(s, "");
    }

    @Override
    public boolean hasAccount(OfflinePlayer offlinePlayer) {
        return hasAccount(offlinePlayer, "");
    }

    @Override
    public boolean hasAccount(String s, String s1) {
        return Miniverse.getUser(s) != null && Miniverse.getUser(s).hasAccount();
    }

    @Override
    public boolean hasAccount(OfflinePlayer offlinePlayer, String s) {
        return Miniverse.getOfflineUser(offlinePlayer.getUniqueId()) != null && Miniverse.getOfflineUser(offlinePlayer.getUniqueId()).hasAccount();
    }

    @Override
    public double getBalance(String s) {
        return getBalance(s, "");
    }

    @Override
    public double getBalance(OfflinePlayer offlinePlayer) {
        return getBalance(offlinePlayer, "");
    }

    @Override
    public double getBalance(String s, String s1) {
        return !hasAccount(s) ? 0 : Miniverse.getUser(s).getBalanceRaw();
    }

    @Override
    public double getBalance(OfflinePlayer offlinePlayer, String s) {
        return !hasAccount(offlinePlayer) ? 0 : Miniverse.getOfflineUser(offlinePlayer.getUniqueId()).getBalanceRaw();
    }

    @Override
    public boolean has(String s, double v) {
        return has(s, "", v);
    }

    @Override
    public boolean has(OfflinePlayer offlinePlayer, double v) {
        return has(offlinePlayer, "", v);
    }

    @Override
    public boolean has(String s, String s1, double v) {
        return Miniverse.getUser(s) != null && Miniverse.getUser(s).hasEnough(v);
    }

    @Override
    public boolean has(OfflinePlayer offlinePlayer, String s, double v) {
        return Miniverse.getOfflineUser(offlinePlayer.getUniqueId()) != null && Miniverse.getOfflineUser(offlinePlayer.getUniqueId()).hasEnough(v);
    }

    @Override
    public EconomyResponse withdrawPlayer(String s, double v) {
        return withdrawPlayer(s, "", v);
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, double v) {
        return withdrawPlayer(offlinePlayer, "", v);
    }

    @Override
    public EconomyResponse withdrawPlayer(String s, String s1, double v) {
        User u = Miniverse.getUser(s);
        if(u == null)
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, "Invalid user");
        u.withDrawMoney(v);
        return new EconomyResponse(v,u.getBalanceRaw(), EconomyResponse.ResponseType.SUCCESS, "Balance edited DEPOSIT");
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, String s, double v) {
        OfflineUser u = Miniverse.getOfflineUser(offlinePlayer.getUniqueId());
        if(u == null)
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, "Invalid user");
        u.withDrawMoney(v);
        return new EconomyResponse(v,u.getBalanceRaw(), EconomyResponse.ResponseType.SUCCESS, "Balance edited WITHDRAW");
    }

    @Override
    public EconomyResponse depositPlayer(String s, double v) {
        return depositPlayer(s, "", v);
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, double v) {
        return depositPlayer(offlinePlayer, "", v);
    }

    @Override
    public EconomyResponse depositPlayer(String s, String s1, double v) {
        User u = Miniverse.getUser(s);
        if(u == null)
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, "Invalid user");
        u.depositMoney(v);
        return new EconomyResponse(v,u.getBalanceRaw(), EconomyResponse.ResponseType.SUCCESS, "Balance edited DEPOSIT");
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, String s, double v) {
        OfflineUser u = Miniverse.getOfflineUser(offlinePlayer.getUniqueId());
        if(u == null)
            return new EconomyResponse(0,0, EconomyResponse.ResponseType.FAILURE, "Invalid user");
        u.depositMoney(v);
        return new EconomyResponse(v,u.getBalanceRaw(), EconomyResponse.ResponseType.SUCCESS, "Balance edited DEPOSIT");
    }

    public EconomyResponse createBank(String name, String player) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    @Override
    public EconomyResponse createBank(String s, OfflinePlayer offlinePlayer) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    public EconomyResponse deleteBank(String name) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    public EconomyResponse bankHas(String name, double amount) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    public EconomyResponse bankWithdraw(String name, double amount) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    public EconomyResponse bankDeposit(String name, double amount) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    public EconomyResponse isBankOwner(String name, String playerName) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    @Override
    public EconomyResponse isBankOwner(String s, OfflinePlayer offlinePlayer) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    public EconomyResponse isBankMember(String name, String playerName) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    @Override
    public EconomyResponse isBankMember(String s, OfflinePlayer offlinePlayer) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    public EconomyResponse bankBalance(String name) {
        return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "MiniverseEconomy does not support bank accounts!");
    }

    @Override
    public List<String> getBanks() {
        return new ArrayList<>();
    }

    @Override
    public boolean createPlayerAccount(String s) {
        return createPlayerAccount(s,"");
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer offlinePlayer) {
        return createPlayerAccount(offlinePlayer,"");
    }

    @Override
    public boolean createPlayerAccount(String s, String s1) {
        User u = Miniverse.getUser(s);
        if(u == null) return false;
        u.createAccount();
        return true;
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer offlinePlayer, String s) {
        OfflineUser u = Miniverse.getOfflineUser(offlinePlayer.getUniqueId());
        if(u == null) return false;
        u.createAccount();
        return true;
    }
}
