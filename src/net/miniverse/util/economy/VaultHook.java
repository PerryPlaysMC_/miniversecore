package net.miniverse.util.economy;

import net.milkbowl.vault.economy.Economy;
import net.miniverse.core.IMiniverse;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicePriority;

public class VaultHook {

    public static Economy econ = null;
    private static EconomyImpl impl;

    public static boolean setupEconomy() {
        if (Miniverse.getManager().getPlugin("Vault") == null) {
            Miniverse.log(IMiniverse.LogType.WARNING, "Could not find Vault!");
            return false;
        }
        impl = new EconomyImpl();
        Bukkit.getServer().getServicesManager().register(Economy.class, impl, MiniverseCore.getAPI(), ServicePriority.Highest);
        Miniverse.log(IMiniverse.LogType.INFO, "Loaded Economy");
        return true;
    }

}
