package net.miniverse.util;

import net.miniverse.util.config.Config;
import net.miniverse.core.Miniverse;
import net.miniverse.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
@SuppressWarnings("all")
public class StringUtils {

    private static String s = "/";

    private static Config getSettings() {
        return Miniverse.getSettings();
    }

    public static String translate(String toTrans) {
        String message = toTrans;
        String msg = message;
        if(toTrans != null && getSettings() != null && getSettings().getString("Colors.prefix") != "" && getSettings().getString("Colors.prefix") != null) {
            String prefix = (!toTrans.contains("[p] ") ? getSettings().getString("Colors.prefix") + " " : getSettings().getString("Colors.prefix"));

            msg = message
                    .replace("[p]".toLowerCase(), prefix)
                    .replace("[pc]".toLowerCase(), getSettings().getString("Colors.playerColor"))
                    .replace("[c]".toLowerCase(), getSettings().getString("Colors.defaultColor"))
                    .replace("[a]".toLowerCase(), getSettings().getString("Colors.argumentColor"))
                    .replace("[o]".toLowerCase(), getSettings().getString("Colors.optionalArgument"))
                    .replace("[r]".toLowerCase(), getSettings().getString("Colors.requiredArgument"))
                    .replace("[n]", "\n")
                    .replace("\\n", "\n");
        }

        return translateColors('&', msg);
    }


    public static String translateColors(char altColor, String text) {
        for(ChatColor c : ChatColor.values()) {
            text = text.replace((altColor+"") + c.getChar(), "§" + c.getChar());
        }
        return text;
    }


    public static String formatList(List<String> str) {
        return formatList(false, false, str);
    }
    public static String formatList(boolean comma, List<String> str) {
        return formatList(false, comma, str);
    }
    public static String formatList(boolean color, boolean comma, List<String> str) {
        StringBuilder sb = new StringBuilder();
        for(String x : str) {
            if(color) {
                if(comma) {
                    sb.append("[pc]"+x).append("[c], ");
                }else {
                    sb.append("[pc]"+x).append("[c] ");
                }
            }else {
                if(comma) {
                    sb.append(x).append(", ");
                }else {
                    sb.append(x).append(" ");
                }
            }
        }
        return sb.toString().trim();
    }

    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty() || str.length() == 0;
    }

    public static String locationToString(Location l) { return l.getWorld().getName() + s + l.getX() + s + l.getY() + s + l.getZ() + s + l.getYaw() + s + l.getPitch(); }

    public static Location stringToLocation(String l) {
        if (!l.contains(s)) return null;
        if(l.split(s).length == 6) return null;
        String[] x = l.split(s);
        return new Location(Bukkit.getWorld(x[0]),
                Double.parseDouble(x[1]),
                Double.parseDouble(x[2]),
                Double.parseDouble(x[3]),
                Float.parseFloat(x[4]),
                Float.parseFloat(x[5]));
    }

    public static String stripColor(String str) {
        String r = translate(str);
        for(ChatColor c : ChatColor.values()) {
            if(str.contains("§"+c.getChar()))
                r = r.replace("§" + c.getChar(), "");
            if(str.contains("&"+c.getChar()))
                r = r.replace("&" + c.getChar(), "");
        }
        return ChatColor.stripColor(r);
    }
    public static String changeColor(String str) {
        String r = str;
        if(str.contains("§") || str.contains("&"))
            for(ChatColor c : ChatColor.values()) {
                if(str.contains("§"+c.getChar()))
                    r = r.replace("§" + c.getChar(), "&" + c.getChar());
            }
        return r;
    }

    public static ChatColor getLastColorForChat(String message, ChatColor def) {
        String a = stripColor(message);
        if(a.toCharArray().length < 1) return def;
        int l = a.toCharArray().length;
        ChatColor c = ChatColor.getByChar(a.charAt(l+-1));
        int i = l+-1;
        while(c == null || !c.isColor()) {
            i = i+-2;
            if(l >= l+- i) {
                c = ChatColor.getByChar(a.charAt(l +- i));
            }else {
                break;
            }
        }
        if(c == null || !c.isColor()) return def;
        return c;
    }

    public static String getLastColors(String msg) {
        String last = "", l2 = "";
        if(msg.contains("&") || msg.contains("§")) {
            msg = msg.replace("&", "§");
            for (int c = 1; c < msg.split("§").length; c++) {
                String[] d = msg.split("§");
                if(d.length == 0 || d[c] == null || d[c].length() == 0) continue;
                String ch = d[c].charAt(0) + "";
                if(ChatColor.getByChar(d[c].charAt(0)) != null) {
                    if(isColor(last)) {
                        if(last.length()+-2>0)
                            last = last.substring(0, last.length()+-2);
                    }
                    if(!last.contains("§"+ch))
                        if(isColor("§"+ch))
                            last = "§" + ch;
                        else
                            last += "§" + ch;
                }
            }
        }
        return last.replace("§§","§");
    }

    public static String getNameFromEnum(Enum e) {
        String f = e.name();
        String a = f.charAt(0) + f.substring(1).toLowerCase().split("_")[0];
        if(f.contains("_")) {
            for(int z = 1; z < f.split("_").length; z++) {
                String p = f.split("_")[z];
                a+=" " + (p.charAt(0)+"").toUpperCase() + p.substring(1).toLowerCase();
            }
        }
        return a;
    }

    public static String getNameFromCaps(String e) {
        String f = e;
        String a = f.charAt(0) + f.substring(1).toLowerCase().split("_")[0];
        if(f.contains("_")) {
            for(int z = 1; z < f.split("_").length; z++) {
                String p = f.split("_")[z];
                a+=" " + (p.charAt(0)+"").toUpperCase() + p.substring(1).toLowerCase();
            }
        }
        return a;
    }

    public static boolean isColor(String last) {
        return !isBold(last) && !isItalic(last) && !isStrikThrough(last) && !isUnderline(last);
    }

    public static boolean hasColor(String last) {
        for(String x : changeColor(last).split("&")) {
            if(isColor(x)) {
                return true;
            }
        }
        return false;
    }
    public static String replaceFirstColor(String last) {
        String f = changeColor(last);
        if(f.length()>0) {
            f.split("&")[0] = "";
            return f.substring(2);
        }
        return f;
    }

    private static boolean isBold(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        if(x.charAt(x.length()+-1) == 'l')
            return true;
        return false;
    }
    private static boolean isItalic(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        if(x.charAt(x.length()+-1) == 'o')
            return true;
        return false;
    }
    private static boolean isUnderline(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        if(x.charAt(x.length()+-1) == 'n')
            return true;
        return false;
    }
    private static boolean isStrikThrough(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        if(x.charAt(x.length()+-1) == 'm')
            return true;
        return false;
    }

    public static String formatted(Config cfg, String location, Object... objects) {
        if(cfg.get(location) != null) {
            String msg = ChatColor.translateAlternateColorCodes('&', cfg.getString(location));
            if(objects.length > 0) {
                for (int i = 0; i < objects.length; i++) {
                    if(objects[i] != null) {
                        msg = msg.replace("{" + i + "}", objects[i].toString());
                    }
                }
            }
            return StringUtils.translate(msg);
        }
        return "Error cannot find: " + location;
    }


    public static String translateUserPerms(User u, String original, String ms) {
        if(u == null) {
            return translateChatColor(ms);
        }
        String og = !original.startsWith("&") & !original.startsWith("§") ? "§"+original : original;
        for(ChatColor c : ChatColor.values()) {
            if(ms.contains("&" + c.getChar())) {
                if(u.hasPermission("allcodes", "chatcolor." + c.getChar())) {
                    if(c.getChar()=='r') {
                        ms = ms.replace("&r", og).replace("§r", og).replace(og, og.replace("&", "§"));
                        continue;
                    }
                    ms = ms.replace("&" + c.getChar(), "§" + c.getChar());
                }
            }
        }
        return ms;
    }

    public static String translateChatColor(String str) {
        return translateColors('&', str);
    }


    public static boolean checkForDomain(String str) {
        String domainPattern = "[a-zA-Z0-9](([a-zA-Z0-9\\-]{0,61}[A-Za-z0-9])?\\.)+(com|net|org|co|gg|io|pro)";
        Pattern r = Pattern.compile(domainPattern);
        Matcher m = r.matcher(str);
        Pattern invPat = Pattern.compile("(?:https?://)?discord(?:app\\.com/invite|\\.gg)/([a-z0-9-]+)", 2);
        Matcher m2 = invPat.matcher(str);
        return m.find() || m2.find();
    }

}
