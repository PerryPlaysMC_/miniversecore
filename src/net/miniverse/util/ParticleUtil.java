package net.miniverse.util;

import net.miniverse.core.MiniverseCore;
import net.miniverse.user.User;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.scheduler.BukkitRunnable;

public class ParticleUtil {


	public static void spiral(User u, Location loc, Particle part, int size) {
		float y = (float) loc.getY() + 2.0f;
		float r = 1f;
		for(double t = 0; t < 30; t = t + 0.5) {
			float x = r*(float)Math.sin(t);
			float z = r*(float)Math.cos(t);
			sendParticle(u, loc.getX() + x, y, loc.getZ() + z, part, size);
			y = y+- 0.04f;
		}
	}

	public static void spiralSlow(User u, Location loc, Particle part, int size) {
		(new BukkitRunnable() {
			float y = (float) loc.getY() + 2.0f;
			float r = 1f;
			double t = 0;
			@Override
			public void run() {
				float x = r*(float)Math.sin(t);
				float z = r*(float)Math.cos(t);
				sendParticle(u, loc.getX() + x, y, loc.getZ() + z, part, size);
				y = y+- 0.04f;
				t = t + 0.5;
				if(t == 30) cancel();
			}
		}).runTaskTimer(MiniverseCore.getAPI(), 0, 5);
	}

	public static void reverseSpiral(User u, Location loc, Particle part, int size) {
		float y = (float) loc.getY();
		float r = 1f;
		for(double t = 0; t < 30; t = t + 0.5) {
			float x = r*(float)Math.cos(t);
			float z = r*(float)Math.sin(t);
			sendParticle(u, loc.getX() + x, y, loc.getZ() + z, part, size);
			y = y += 0.04f;
		}
	}

	public static void reverseSpiralSlow(User u, Location loc, Particle part, int size) {

		(new BukkitRunnable() {
			float y = (float) loc.getY();
			float r = 1f;
			double t = 0;
			@Override
			public void run() {
				float x = r*(float)Math.cos(t);
				float z = r*(float)Math.sin(t);
				sendParticle(u, loc.getX() + x, y, loc.getZ() + z, part, size);
				y = y+= 0.04f;
				t = t + 0.5;
				if(t == 30) cancel();
			}
		}).runTaskTimer(MiniverseCore.getAPI(), 0, 5);
	}
	
	public static void circle(User u, Location loc, Particle part, int size) {
		float y = (float) loc.getY();
		float r = 1f;
		for(double t = 0; t < 25; t+= 0.5) {
			float x = r*(float)Math.sin(t);
			float z = r*(float)Math.cos(t);
			sendParticle(u, loc.getX() + x, y, loc.getZ() + z, part, size);
		}
	}
	
	public static void circle(User u, Location loc, double addToY, Particle part, int size) {
		float y = (float) ((float) loc.getY() + addToY);
		float r = 1f;
		for(double t = 0; t < 25; t+= 0.5) {
			float x = r*(float)Math.sin(t);
			float z = r*(float)Math.cos(t);
			sendParticle(u, loc.getX() + x, y, loc.getZ() + z, part, size);
		}
	}


	public static void sendParticle(User u, double x, double y, double z, Particle particle, int size) {
		u.getBase().getWorld().spawnParticle(particle, x, y, z, 0, 0, 0, size, 0);
	}

}
