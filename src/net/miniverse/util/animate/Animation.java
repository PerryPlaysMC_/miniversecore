package net.miniverse.util.animate;

import net.miniverse.core.MiniverseCore;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class Animation {

    private String[] animates;
    private double current;
    private int index = 0;

    public Animation(double speed, String... animates) {
        this.animates = animates;
        current = speed;
        (new BukkitRunnable() {
            @Override
            public void run() {
                if(current > 0.00) {
                    current += -0.01;
                    return;
                }
                index++;
                if(index >= animates.length) index = 0;
                current = speed;
            }
        }).runTaskTimer(MiniverseCore.getAPI(), 0, 1);
    }


    public String[] getAnimates() {
        return animates;
    }


    public String getCurrentString() {
        return getAnimates()[index];
    }



}
