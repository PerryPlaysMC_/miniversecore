package net.miniverse.util;

import net.miniverse.core.events.user.UserJoinEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

public interface Listener extends org.bukkit.event.Listener {

	@EventHandler
	default void Event(UserJoinEvent e) {
		
	}
	

	@EventHandler
	default void Event(AsyncPlayerChatEvent e) {
		
	}

	@EventHandler
	default void Event(EntityDamageByEntityEvent e) {
		
	}

	@EventHandler
	default void Event(PlayerInteractAtEntityEvent e) {
		
	}
	
	@EventHandler
	default void Event(EntityDeathEvent e) {
		
	}
	
}
