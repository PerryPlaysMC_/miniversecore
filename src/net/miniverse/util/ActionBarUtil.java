package net.miniverse.util;

import net.minecraft.server.v1_14_R1.*;
import net.miniverse.core.MiniverseCore;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class ActionBarUtil {

    private static Plugin plugin;
    private static String nmsver;
    private static boolean useOldMethods = false;

    static {
        nmsver = Bukkit.getServer().getClass().getPackage().getName();
        nmsver = nmsver.substring(nmsver.lastIndexOf(".") + 1);
        if ((nmsver.equalsIgnoreCase("v1_8_R1")) || (nmsver.startsWith("v1_7_"))) {
            useOldMethods = true;
        }
    }

    public static void sendAction(Player p, String message, boolean loop, int interval) {
        if(loop) {
            (new BukkitRunnable() {

                @Override
                public void run() {
                    sendActionBar(p, StringUtils.translate(message));
                }
            }).runTaskTimer(MiniverseCore.getAPI(), 0, interval);
        }else{
            sendActionBar(p, StringUtils.translate(message));
        }
    }

    public static void sendActionBar(Player player, String message)
    {
        if (!player.isOnline()) {
            return;
        }
        try
        {
            message = StringUtils.translate(message);


            PacketPlayOutChat send = new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + message + "\"}"), ChatMessageType.GAME_INFO);

            ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(send);

//            Class<?> craftPlayerClass = Class.forName("org.bukkit.craftbukkit.entity.CraftPlayer");
//            Object craftPlayer = craftPlayerClass.cast(player);
//
//            Class<?> packetPlayOutChatClass = Class.forName("net.minecraft.server.PacketPlayOutChat");
//            Class<?> packetClass = Class.forName("net.minecraft.server.Packet");
//            Object packet;
//            if (useOldMethods)
//            {
//                Class<?> chatSerializerClass = Class.forName("net.minecraft.server." + nmsver + ".ChatSerializer");
//                Class<?> iChatBaseComponentClass = Class.forName("net.minecraft.server." + nmsver + ".IChatBaseComponent");
//                Method m3 = chatSerializerClass.getDeclaredMethod("a", new Class[] { String.class });
//                Object cbc = iChatBaseComponentClass.cast(m3.invoke(chatSerializerClass, new Object[] { "{\"text\": \"" + message + "\"}" }));
//                packet = packetPlayOutChatClass.getConstructor(new Class[] { iChatBaseComponentClass, Byte.TYPE }).newInstance(new Object[] { cbc, Byte.valueOf((byte)2) });
//            }
//            else
//            {
//                Class<?> chatComponentTextClass = Class.forName("net.minecraft.server." + nmsver + ".ChatComponentText");
//                Class<?> iChatBaseComponentClass = Class.forName("net.minecraft.server." + nmsver + ".IChatBaseComponent");
//                try
//                {
//                    Class<?> chatMessageTypeClass = Class.forName("net.minecraft.server." + nmsver + ".ChatMessageType");
//                    Object[] chatMessageTypes = chatMessageTypeClass.getEnumConstants();
//                    Object chatMessageType = null;
//                    for (Object obj : chatMessageTypes) {
//                        if (obj.toString().equals("GAME_INFO")) {
//                            chatMessageType = obj;
//                        }
//                    }
//                    Object chatCompontentText = chatComponentTextClass.getConstructor(new Class[] { String.class }).newInstance(new Object[] { message });
//                    packet = packetPlayOutChatClass.getConstructor(new Class[] { iChatBaseComponentClass, chatMessageTypeClass }).newInstance(new Object[] { chatCompontentText, chatMessageType });
//                }
//                catch (ClassNotFoundException cnfe)
//                {
//                    Object chatCompontentText = chatComponentTextClass.getConstructor(new Class[] { String.class }).newInstance(new Object[] { message });
//                    packet = packetPlayOutChatClass.getConstructor(new Class[] { iChatBaseComponentClass, Byte.TYPE }).newInstance(new Object[] { chatCompontentText, (byte)2 });
//                }
//            }
//            Method craftPlayerHandleMethod = craftPlayerClass.getDeclaredMethod("getHandle", new Class[0]);
//            Object craftPlayerHandle = craftPlayerHandleMethod.invoke(craftPlayer, new Object[0]);
//            Field playerConnectionField = craftPlayerHandle.getClass().getDeclaredField("playerConnection");
//            Object playerConnection = playerConnectionField.get(craftPlayerHandle);
//            Method sendPacketMethod = playerConnection.getClass().getDeclaredMethod("sendPacket", new Class[] { packetClass });
//            sendPacketMethod.invoke(playerConnection, new Object[] { packet });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void sendActionBar(Player player, final String message, int duration)
    {
        sendActionBar(player, message);
        if (duration >= 0) {
            new BukkitRunnable()
            {
                public void run()
                {
                    sendActionBar(player, "");
                }
            }

                    .runTaskLater(plugin, duration + 1);
        }
        while (duration > 40)
        {
            duration -= 40;

            new BukkitRunnable()
            {
                public void run()
                {
                    sendActionBar(player, message);
                }
            }

                    .runTaskLater(plugin, duration);
        }
    }

}
