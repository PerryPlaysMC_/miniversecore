package net.miniverse.util.reflection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author DPOH-VAR
 * @version 1.0
 */
@SuppressWarnings("UnusedDeclaration")
public class ReflectionUtils {
    private static String MINECRAFT_SERVER;

    static {
        MINECRAFT_SERVER = "net.minecraft.server." + Bukkit.getServer().getClass().getPackage().getName().substring(23);
    }

    public static void sendMessageWithJSON(Player player, String json) {
        Object packetPlayOutChat = null;

        try {
            packetPlayOutChat = getClass("PacketPlayOutChat").getConstructor(getClass("IChatBaseComponent"),
                    byte.class).newInstance(getIChatBaseComponentFromJSONString(json), (byte) 1);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }

        sendPacket(player, packetPlayOutChat);
    }

    public static Object getIChatBaseComponentFromJSONString(String json) {
        try {
            return getMethod(getChatSerializer(), "a", String.class).invoke(null, json);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return json;
    }

    private static void sendPacket(Player player, Object packet) {
        Object nmsPlayer = null;

        try {
            nmsPlayer = getMethod(player.getClass(), "getHandle").invoke(player);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
            e1.printStackTrace();
        }

        Object playerConnection = getFieldValue(nmsPlayer.getClass(), nmsPlayer, "playerConnection");

        try {
            getMethod(playerConnection.getClass(), "sendPacket", getClass("Packet")).invoke(playerConnection, packet);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private static Class<?> getClass(String className) {
        try {
            return Class.forName(MINECRAFT_SERVER + "." + className);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return null;
    }

    private static Class<?> getChatSerializer() {
        Class<?> serializer = getClass("IChatBaseComponent$ChatSerializer");

        try {
            return serializer.newInstance().getClass();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Method getMethod(Class<?> clazz, String methodName, Class<?>... parameters) {
        try {
            return clazz.getMethod(methodName, parameters);
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Object getFieldValue(Class<?> clazz, Object object, String fieldName) {
        Field field;

        try {
            field = clazz.getField(fieldName);

            return field.get(object);
        } catch (IllegalAccessException | NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }

        return null;
    }
}

