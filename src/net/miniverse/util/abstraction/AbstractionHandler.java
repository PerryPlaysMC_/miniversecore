package net.miniverse.util.abstraction;

import net.miniverse.util.abstraction.handlers.jsonMessager.v1_8.*;
import net.miniverse.util.abstraction.handlers.jsonMessager.v1_9.*;
import net.miniverse.util.abstraction.handlers.jsonMessager.v1_10.*;
import net.miniverse.util.abstraction.handlers.jsonMessager.v1_11.*;
import net.miniverse.util.abstraction.handlers.jsonMessager.v1_12.*;
import net.miniverse.util.abstraction.handlers.jsonMessager.v1_13.*;
import net.miniverse.util.abstraction.handlers.jsonMessager.v1_14.*;
import org.bukkit.Bukkit;

public class AbstractionHandler {

    private static JsonMessager messager;

    public static void load() {
        String pack =  Bukkit.getServer().getClass().getPackage().getName();
        String version = pack.substring(pack.lastIndexOf('.')+1).replaceFirst("v", "");

        System.out.println("Loading " + version);
        switch(version) {
            case "1_8_R1": {
                messager = new JsonMessager_1_8_R1();
                break;
            }
            case "1_8_R2": {
                messager = new JsonMessager_1_8_R2();
                break;
            }
            case "1_8_R3": {
                messager = new JsonMessager_1_8_R3();
                break;
            }
            case "1_9_R1": {
                messager = new JsonMessager_1_9_R1();
                break;
            }
            case "1_9_R2": {
                messager = new JsonMessager_1_9_R2();
                break;
            }
            case "1_10_R1": {
                messager = new JsonMessager_1_10_R1();
                break;
            }
            case "1_11_R1": {
                messager = new JsonMessager_1_11_R1();
                break;
            }
            case "1_12_R1": {
                messager = new JsonMessager_1_12_R1();
                break;
            }
            case "1_13_R1": {
                messager = new JsonMessager_1_13_R1();
                break;
            }
            case "1_13_R2": {
                messager = new JsonMessager_1_13_R2();
                break;
            }
            case "1_14_R1": {
                messager = new JsonMessager_1_14_R1();
                break;
            }
            default: {
                System.out.println("Unsupported version '" + version + "'");
                break;
            }
        }

    }


    public static JsonMessager getMessager() {
        return messager;
    }
}
