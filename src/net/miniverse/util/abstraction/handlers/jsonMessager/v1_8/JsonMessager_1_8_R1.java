package net.miniverse.util.abstraction.handlers.jsonMessager.v1_8;

import net.miniverse.util.abstraction.JsonMessager;
import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JsonMessager_1_8_R1 implements JsonMessager {

    @Override
    public void sendMessage(Player player, String json) {
        IChatBaseComponent c = ChatSerializer.a(json);
        PacketPlayOutChat p = new PacketPlayOutChat(c, (byte)0);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
    }
}
