package net.miniverse.util.abstraction.handlers.jsonMessager.v1_13;

import net.miniverse.util.abstraction.JsonMessager;
import net.minecraft.server.v1_13_R1.ChatMessageType;
import net.minecraft.server.v1_13_R1.IChatBaseComponent;
import net.minecraft.server.v1_13_R1.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_13_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JsonMessager_1_13_R1 implements JsonMessager {

    @Override
    public void sendMessage(Player player, String json) {
        IChatBaseComponent c = IChatBaseComponent.ChatSerializer.a(json);
        PacketPlayOutChat p = new PacketPlayOutChat(c, ChatMessageType.CHAT);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
    }
}
