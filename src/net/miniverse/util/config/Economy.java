package net.miniverse.util.config;

import net.miniverse.core.Miniverse;

public class Economy
{
  public Economy() {}
  
  public static boolean isMaxBalance() {
    return Miniverse.getSettings().getBoolean("Commands.Economy.maxBalance");
  }
  
  public static boolean isBalanceGreaterThanMax(double money) {
    return (isMaxBalance()) && (money > Miniverse.getSettings().getDouble("Commands.Economy.maxBal"));
  }
  
  public static boolean isBalanceGreaterThanMax(net.miniverse.user.User u, double money) {
    return (isMaxBalance()) && (u.getBalanceRaw().doubleValue() + money > Miniverse.getSettings().getDouble("Commands.Economy.maxBal"));
  }
}
