package net.miniverse.util.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.miniverse.core.MiniverseCore;
import net.miniverse.util.StringUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class Config implements IConfig {

    private File f, dir;
    private String name, copy, local;
    private YamlConfiguration cfg;
    private boolean reloadOnce = false, isCopy;
    private MiniverseCore core;

    public Config(MiniverseCore core, File dir, String copy, String name) {
        this.core = core;
        if (dir == null) {
            dir = new File("plugins", "Miniverse");
        }
        if (!dir.isDirectory()) {
            dir.mkdirs();
            dir.mkdir();
            this.dir = dir;
        }
        this.name = name;
        f = new File(dir, name);
        if ((copy == null) || (copy.isEmpty())) {
            if (!f.exists()) {
                try {
                    if (core != null) {
                        if (core.getResource(f.getName()) != null) {
                            FileUtils.copyInputStreamToFile(core.getResource(f.getName()), f);
                        } else {
                            f.createNewFile();
                        }
                    } else {
                        f.createNewFile();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            isCopy = true;
            this.copy = copy;
            if (!f.exists()) {
                try {
                    if (core != null) {
                        if (core.getResource(f.getName()) != null) {
                            FileUtils.copyInputStreamToFile(core.getResource(copy), f);
                        } else {
                            f.createNewFile();
                        }
                    } else {
                        f.createNewFile();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (name.endsWith(".yml")) {
            reloadOnce();
        }
        local = name;
        if (local.endsWith(".yml")) {
            local = local.replace(".yml", "");
        }
        ConfigManager.addConfig(this);
    }

    public Config(MiniverseCore core, File dir, String name) {
        this(core, dir, "", name);
    }

    public Config(File dir, String fileCopy, String name) {
        this(MiniverseCore.getAPI(), dir, fileCopy, name);
    }

    public Config(File dir, String name) {
        this(MiniverseCore.getAPI(), dir, "", name);
    }

    public Config(String dir, String name) {
        this(MiniverseCore.getAPI(), new File(dir), "", name);
    }

    public Config(MiniverseCore core, String name) {
        this(core,  null, "", name);
    }

    public Config(String name) {
        this(MiniverseCore.getAPI(),null, "", name);
    }

    public MiniverseCore getCore() {
        return core;
    }

    public Config resetConfig() {
        Config cfg = null;
        try {
            f.delete();
            ConfigManager.removeConfig(this);
            cfg = new Config(core, dir, copy, name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cfg;
    }

    public String getName() {
        return name;
    }

    public String getLocalName() {
        return local;
    }

    public void setLocal(String newLocal) {
        this.local = newLocal;
    }

    public File getDirectory()
    {
        return dir;
    }

    public File getFile()
    {
        return f;
    }

    public YamlConfiguration getConfig()
    {
        return cfg;
    }

    public String getString(String path)
    {
        return cfg.getString(path);
    }

    public boolean getBoolean(String path)
    {
        return cfg.getBoolean(path);
    }

    public Object get(String path) {
        if (cfg.get(path) instanceof String) {
            if((isSet(path) && getString(path).contains("/") && getString(path).split("/").length == 6)) {
                return getLocation(getString(path));
            }
            return getString(path);
        }
        return cfg.get(path);
    }

    public ConfigurationSection getSection(String path)
    {
        return cfg.getConfigurationSection(path);
    }

    public ConfigurationSection createSection(String path)
    {
        return cfg.createSection(path);
    }

    public int getInt(String path)
    {
        return cfg.getInt(path);
    }

    public double getDouble(String path)
    {
        return cfg.getDouble(path);
    }

    public List<String> getStringList(String path)
    {
        return cfg.isSet(path) ? cfg.getStringList(path) : new ArrayList();
    }

    public Location getLocation(String path) {
        return (isSet(path) && getString(path).contains("/") && getString(path).split("/").length == 6 ? StringUtils.stringToLocation(getString(path)) : null);
    }


    public List<Object> getObjectList(String path) {
        return (List<Object>) cfg.getList(path);
    }

    public Config set(String path, Object var) {
        if ((var instanceof Location)) {
            cfg.set(path, StringUtils.locationToString((Location)var));
            save();
            return this;
        }
        cfg.set(path, var);
        save();
        return this;
    }

    public List<?> getList(String path) {
        return cfg.getList(path);
    }

    private void saveItem(ConfigurationSection section, ItemStack item) {
        if (item == null) return;
        section.set("type", item.getType().name());
        section.set("amount", item.getAmount());
        if (item.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)item.getItemMeta();
            if (im.getOwner() != null)
                section.set("Owner", im.getOwner());
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
            return;
        }
        if (item.hasItemMeta()) {
            ItemMeta im = item.getItemMeta();
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
        }
        section.set("data", item.getDurability());
    }

    private ItemStack loadItem(ConfigurationSection section) {
        ItemStack i = new ItemStack(Material.valueOf(section.getString("type")), section.getInt("amount"));
        if (i.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)i.getItemMeta();
            if (section.getString("Owner") != null)
                im.setOwner(section.getString("Owner"));
            if (section.getStringList("enchants") != null) {
                for (String a : section.getStringList("enchants"))
                    im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
            }
            if (section.getStringList("lore") != null) {
                im.setLore(section.getStringList("lore"));
            }
            im.setUnbreakable(section.getBoolean("unbreakable"));
            i.setDurability((short)section.getInt("data"));
            i.setItemMeta(im);
            return i;
        }
        ItemMeta im = i.getItemMeta();
        if (section.getStringList("enchants") != null) {
            for (String a : section.getStringList("enchants"))
                im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
        }
        if (section.getStringList("lore") != null) {
            im.setLore(section.getStringList("lore"));
        }
        im.setUnbreakable(section.getBoolean("unbreakable"));
        i.setDurability((short)section.getInt("data"));
        i.setItemMeta(im);
        return i;
    }

    public Config setItemStack(String path, ItemStack item)
    {
        saveItem(createSection(path), item);
        save();
        return this;
    }

    public boolean isSet(String path) {
        return cfg.isSet(path);
    }

    public ItemStack getItemStack(String path) {
        return loadItem(getSection(path));
    }

    public void reloadOnce() {
        if (!reloadOnce) {
            reloadOnce = true;
            cfg = YamlConfiguration.loadConfiguration(f);
        }
    }

    public void deleteConfig() {
        String name = getLocalName();
        ConfigManager.removeConfig(this);
        getFile().delete();
        this.name = "";
        MiniverseCore.getAPI().debug(name + " Has been deleted");
    }

    public Long getLong(String path) {
        return cfg.getLong(path);
    }

    public void reload() {
        cfg = YamlConfiguration.loadConfiguration(f);
        MiniverseCore.getAPI().debug("Reloaded: " + f.getName() + " for Plugin: " + core
                .getName());
    }

    public void save()
    {
        try {
            cfg.save(f);
            if (dir == new File("plugins/Miniverse", "userInfo")) {
                ConfigManager.copyFolderFiles(false, "plugins/Miniverse/userInfo", "plugins/Miniverse/userInfoBackup");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
