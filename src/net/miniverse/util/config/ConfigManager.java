package net.miniverse.util.config;

import net.miniverse.core.IMiniverse;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.OfflineUser;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

public class ConfigManager {

    private static Set<Config> cfgs;
    private static Set<Config> all;


    public static Set<Config> getConfigs() {
        if (cfgs == null) {
            cfgs = new HashSet<>();
        }
        return cfgs;
    }
    public static Set<Config> getAllConfigs(boolean newLoad) {
        if (all == null) {
            all = new HashSet<>();
        }
        if(!newLoad) {
            return all;
        }
        File main = new File(".", "plugins");
        List<File> files = getFiles(main);
        C:for(File f : files) {
            if(f.getName().endsWith(".yml")) {
                boolean continu = false;
                A:for(OfflineUser u : Miniverse.getAllUsers()) {
                    if(f.getName().contains(u.getUUID().toString())) {
                        continue C;
                    }
                }
                String name = f.getPath().substring(1, f.getPath().lastIndexOf('/'))
                        .replace("/plugins/","").replace("/", "-") + "-" + f.getName();
                Config cfg = new Config(new File(f.getPath().substring(0, f.getPath().lastIndexOf('/')+1)), f.getName());
                cfg.setLocal(name);
                if(!all.contains(cfg))
                all.add(cfg);
                removeConfig(cfg);
            }
        }
        all.addAll(getConfigs());
        return all;
    }

    public static List<File> getFiles(File dir) {
        List<File> files = new ArrayList<>();
        File[] fls = dir.listFiles();
        if(fls == null) return files;
        for(File f : fls) {
            if(f.isDirectory()) {
                files.addAll(getFiles(f));
                continue;
            }
            if(!files.contains(f))
                files.add(f);
        }
        return files;
    }

    public static void addConfig(Config cfg) {
        if ((cfg != null) && (!cfg.getFile().getName().contains("null"))) {
            getConfigs().add(cfg);
        }
    }

    public static void reloadAll() {
        for (Config cfg : getConfigs()) {
            cfg.reload();
        }
    }

    public static void deleteAll() {
        for (Config cfg : getConfigs()) {
            cfg.deleteConfig();
        }
    }

    public static void removeConfig(Config cfg) {
        getConfigs().remove(cfg);
    }

    public static Config getConfig(String name) {
        for (Config cfg : getConfigs()) {
            if ((cfg.getName().equalsIgnoreCase(name)) || (cfg.getLocalName().equalsIgnoreCase(name))) {
                return cfg;
            }
        }
        return null;
    }

    public static Config getConfigFromAll(String name) {
        if(getConfig(name) != null) return getConfig(name);
        for (Config cfg : getAllConfigs(false)) {
            if ((cfg.getName().equalsIgnoreCase(name)) || (cfg.getLocalName().equalsIgnoreCase(name))) {
                return cfg;
            }
        }
        return null;
    }

    public static boolean copyFolder(String worldName, String dest) {
        if (new File("." + File.separator + dest).exists()) {
            MiniverseCore.getConsole().sendMessage(Level.INFO.getLocalizedName() + " " + dest + " already exists, Deleting!");
            new File("." + File.separator + dest).delete();
        }
        try {
            FileUtils.copyDirectory(new File("." + File.separator + worldName), new File("." + File.separator + dest));
            MiniverseCore.getConsole().sendMessage(Level.INFO.getLocalizedName() + " File " + worldName + " has been copied!");
            return true;
        } catch (java.io.IOException e) {
            MiniverseCore.getConsole().sendMessage(Level.SEVERE.getLocalizedName() + " Error duplicating world!"); }
        return false;
    }

    public static boolean copyFolderFiles(boolean print, String worldName, String dest) {
        try
        {
            File folder1 = new File("." + File.separator + worldName);
            File folder2 = new File("." + File.separator + dest);
            if (!folder1.exists()) folder1.mkdir();
            if (!folder2.exists()) folder2.mkdir();
            if ((folder2.listFiles() == null) || (!folder2.exists()) || (!folder2.isDirectory()) || (folder2.length() == 0L) || (folder2.listFiles().length == 0)) {
                copyFolder(worldName, dest);
                if (print)
                    Miniverse.log(IMiniverse.LogType.INFO, "Loading files");
            }
            if ((folder1.listFiles() == null) || (folder2.listFiles() == null) || (folder1.listFiles().length != folder2.listFiles().length)) {
                copyFolder(worldName, dest);
                if (print)
                    Miniverse.log(IMiniverse.LogType.ERROR, "Folder stuff " + (
                            (folder1.listFiles() == null) || (folder2.listFiles() == null) ? 1 : false) + "/" + (folder1.listFiles().length != folder2.listFiles().length ? 1 : false));
            }
            for (int i = 0; i < folder1.listFiles().length; i++) {
                File f1 = folder1.listFiles()[i];
                File f2 = folder2.listFiles()[i];
                if (f1.length() == 0L) {
                    FileUtils.copyFile(f2, f1);
                    if (print)
                        Miniverse.log(IMiniverse.LogType.INFO, "Copied file (" + f2
                                .getParentFile().getName() + ")" + f2.getName() + " To (" + f1.getParentFile().getName() + ")" + f1.getName());
                }
                if (!FileUtils.contentEquals(f2, f1)) {
                    FileUtils.copyFile(f1, f2);
                    if (print)
                        Miniverse.log(IMiniverse.LogType.INFO, "Files are not equal, Copied file (" + f1
                                .getParentFile().getName() + ")" + f1.getName() + " To (" + f2.getParentFile().getName() + ")" + f2.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (print)
                Miniverse.log(IMiniverse.LogType.ERROR, "Error");
            return false;
        }
        return true;
    }

    public static boolean copyFolderFiles(String worldName, String dest) { return copyFolderFiles(true, worldName, dest); }
}
