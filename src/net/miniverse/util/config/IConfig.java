package net.miniverse.util.config;

import java.io.File;
import java.util.List;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

public abstract interface IConfig
{
  public abstract YamlConfiguration getConfig();
  
  public abstract String getString(String paramString);
  
  public abstract boolean getBoolean(String paramString);
  
  public abstract Object get(String paramString);
  
  public abstract ConfigurationSection getSection(String paramString);
  
  public abstract ConfigurationSection createSection(String paramString);
  
  public abstract int getInt(String paramString);
  
  public abstract double getDouble(String paramString);
  
  public abstract List<String> getStringList(String paramString);
  
  public abstract List<Object> getObjectList(String paramString);
  
  public abstract File getFile();
  
  public abstract File getDirectory();
  
  public abstract void reload();
  
  public abstract void save();
}
