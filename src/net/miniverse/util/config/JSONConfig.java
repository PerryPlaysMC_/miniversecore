package net.miniverse.util.config;

import com.google.common.collect.Lists;
import net.miniverse.core.MiniverseCore;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

public class JSONConfig {

    private JSONParser parser;
    private Object parsed;
    private JSONObject object;
    private MiniverseCore core;
    private String name;
    private File f, dir;

    public JSONConfig(MiniverseCore core, File dir, String name) {
        this.core = core;
        if(dir == null) {
            dir = new File("plugins", "Miniverse");
        }
        if(!dir.isDirectory()) {
            dir.mkdirs();
            this.dir = dir;
        }
        if(!name.endsWith(".json")){
            name = name + ".json";
        }
        this.name = name;
        this.f = new File(dir, name);
        if(!f.exists()) {
            try {
                if(core != null) {
                    if(core.getResource(f.getName()) != null) {
                        FileUtils.copyInputStreamToFile(core.getResource(f.getName()), f);
                    }else {
                        f.createNewFile();
                    }
                }else {
                    f.createNewFile();
                }
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.parser = new JSONParser();
        parsed = null;
        try {
            parsed = parser.parse(new FileReader(f.getPath()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        object = (JSONObject) parsed;
    }

    public Object get(String path) {
        return object.get(path);
    }

    public Object getSection(String path) {
        List<String> array = Lists.newArrayList();
        String a = String.valueOf(object.get(path));
        JSONArray loreArray = (JSONArray) object.get(a);
        try {
            if(loreArray != null && loreArray.toArray().length > 0) {
                for (Object lorelines : loreArray.toArray()) {
                    JSONObject loredata = (JSONObject) lorelines;

                    array.add(loredata.get(lorelines.toString()).toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String json="";
        JSONObject o = null;
        for(String e : array) {
            try {
                o= (JSONObject) parser.parse(e);
            } catch (ParseException e1) {
                e1.printStackTrace();
                continue;
            }
            json+=o.get(e) +"\n";
        }
        return json;
    }

    public void set(String path) {

    }

    private List<String> getLines()
    {
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(this.f));
            try
            {
                List<String> lines = new ArrayList<>();
                String line;
                for (;;)
                {
                    line = reader.readLine();
                    if (line == null) {
                        break;
                    }
                    lines.add(line);
                }
                return lines;
            }
            finally
            {
                reader.close();
            }
        }
        catch (IOException ex)
        {
            Bukkit.getLogger().log(Level.SEVERE, ex.getMessage(), ex);
        }
        return Collections.emptyList();
    }



}
