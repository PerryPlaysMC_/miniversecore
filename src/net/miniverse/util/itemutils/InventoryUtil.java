package net.miniverse.util.itemutils;

import net.miniverse.core.Miniverse;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class InventoryUtil {


    public static void remove(Inventory inv, ItemStack item, int amount) {
        for (ItemStack i : inv.getContents()) {
            if ((i != null) &&
                    (i.isSimilar(item))) {
                if (i.getAmount() + -1 > 0) {
                    i.setAmount(i.getAmount() + -amount);
                    break;
                }
                i.setAmount(0);
                break;
            }
        }
    }

    public static void remove(Inventory inv, Material item, int amount) {
        for (ItemStack i : inv.getContents()) {
            if ((i != null) &&
                    (i.getType() == item)) {
                if (i.getAmount() + -1 > 0) {
                    i.setAmount(i.getAmount() + -amount);
                    break;
                }
                i.setAmount(0);
                break;
            }
        }
    }


    public static boolean hasItems(Inventory inv, ItemStack item, int amount) {
        List<ItemStack> items = new ArrayList<>();
        for(ItemStack i : inv.getContents()) {
            if(i==null||i.getType().name().contains("AIR"))continue;
            if(i.isSimilar(item))items.add(i);
        }
        return items.size() == amount;
    }
    public static boolean hasItems(Inventory inv, ItemStack... itemS) {
        List<ItemStack> items = new ArrayList<>();
        for(ItemStack item : itemS)
            for(ItemStack i : inv.getContents()) {
                if(i==null||i.getType().name().contains("AIR"))continue;
                if(i.isSimilar(item))items.add(i);
            }
        return items.size() == itemS.length;
    }


    public static boolean hasItems(Inventory inv, Material item, int amount) {
        List<ItemStack> items = new ArrayList<>();
        for(ItemStack i : inv.getContents()) {
            if(item.name().contains("AIR")) {
                if(i == null || i.getType().name().contains("AIR")) {
                    items.add(i);
                    continue;
                }
            }
            if(i==null|| i.getType().name().contains("AIR"))continue;
            if(i.getType() == item)items.add(i);
        }
        return items.size() == amount || inv.contains(item, amount);
    }
    public static boolean hasItems(Inventory inv, Material... itemS) {
        List<ItemStack> items = new ArrayList<>();
        for(Material item : itemS)
            for(ItemStack i : inv.getContents()) {
                if(item.name().contains("AIR")) {
                    if(i == null || i.getType().name().contains("AIR")) {
                        items.add(i);
                        continue;
                    }
                }
                if(i==null || i.getType().name().contains("AIR"))continue;
                if(i.getType() == item)items.add(i);
            }
        return items.size() == itemS.length;
    }
}
