package net.miniverse.util.itemutils;

public enum TagType {


    INT, DOUBLE, STRING, BOOLEAN, BYTE, BYTE_ARRAY, FLOAT, INT_ARRAY, LONG, SHORT

}
