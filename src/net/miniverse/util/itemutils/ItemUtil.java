package net.miniverse.util.itemutils;


import net.minecraft.server.v1_14_R1.*;
import net.miniverse.util.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
public class ItemUtil {

    HashMap<String, NBTTag> data = new HashMap<>();
    ItemStack item;
    ItemMeta im;

    public ItemUtil(ItemStack item) {
        this.item = item;
        im = item.getItemMeta();
        net.minecraft.server.v1_14_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound d = nmsStack.hasTag() ? nmsStack.getTag() : new NBTTagCompound();
        if(d.getKeys()!=null && d.getKeys().size()>0)
            for(String key : d.getKeys()) {
                NBTBase a = d.get(key);
                if(a instanceof NBTTagByte) data.put(key, new NBTTag(TagType.BYTE, d.getByte(key)));
                if(a instanceof NBTTagString) data.put(key, new NBTTag(TagType.STRING, d.getString(key)));
                if(a instanceof NBTTagByteArray) data.put(key, new NBTTag(TagType.BYTE_ARRAY, d.getByteArray(key)));
                if(a instanceof NBTTagShort) data.put(key, new NBTTag(TagType.SHORT, d.getShort(key)));
                if(a instanceof NBTTagDouble) data.put(key, new NBTTag(TagType.DOUBLE, d.getDouble(key)));
                if(a instanceof NBTTagIntArray) data.put(key, new NBTTag(TagType.INT_ARRAY, d.getIntArray(key)));
                if(a instanceof NBTTagInt) data.put(key, new NBTTag(TagType.INT, d.getInt(key)));
                if(a instanceof NBTTagLong) data.put(key, new NBTTag(TagType.LONG, d.getLong(key)));
                if(a instanceof NBTTagFloat) data.put(key, new NBTTag(TagType.FLOAT, d.getFloat(key)));
            }
    }

    public static ItemUtil setItem(ItemStack item) {
        return new ItemUtil(item);
    }

    public static ItemUtil getData(ItemStack item) {
        return ItemUtil.setItem(item);
    }

    public NBTTag get(String key) {
        if(data.containsKey(key)) return data.get(key);
        return null;
    }
    public String getString(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.STRING) return (String)data.get(key).getValue();
        return "";
    }
    public double getDouble(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.DOUBLE)return (double) data.get(key).getValue();
        return 0.0;
    }
    public int getInt(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.INT) return (int)data.get(key).getValue();
        return 0;
    }
    public boolean getBoolean(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (boolean)data.get(key).getValue();
        return false;
    }
    public long getLong(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (long)data.get(key).getValue();
        return 0L;
    }
    public short getShort(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (short) data.get(key).getValue();
        return 0;
    }
    public byte getByte(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (byte)data.get(key).getValue();
        return 0;
    }
    public float getFloat(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (float) data.get(key).getValue();
        return 0;
    }
    public byte[] getByteArray(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (byte[])data.get(key).getValue();
        return new byte[] {0};
    }
    public int[] getIntArray(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (int[])data.get(key).getValue();
        return new int[]{0};
    }

    public boolean hasKey(String key) {
        return data.containsKey(key);
    }

    public ItemUtil setString(String key, String value) {
        data.put(key, new NBTTag(TagType.STRING, value));
        return this;
    }
    public ItemUtil setDouble(String key, double value) {
        data.put(key, new NBTTag(TagType.DOUBLE, value));
        return this;
    }
    public ItemUtil setInt(String key, int value) {
        data.put(key, new NBTTag(TagType.INT, value));
        return this;
    }
    public ItemUtil setBoolean(String key, boolean value) {
        data.put(key, new NBTTag(TagType.BOOLEAN, value));
        return this;
    }
    public ItemUtil setLong(String key, long value) {
        data.put(key, new NBTTag(TagType.LONG, value));
        return this;
    }
    public ItemUtil setShort(String key, short value) {
        data.put(key, new NBTTag(TagType.SHORT, value));
        return this;
    }
    public ItemUtil setFloat(String key, float value) {
        data.put(key, new NBTTag(TagType.FLOAT, value));
        return this;
    }
    public ItemUtil setByteArray(String key, Byte[] value) {
        data.put(key, new NBTTag(TagType.BYTE_ARRAY, value));
        return this;
    }
    public ItemUtil setIntArray(String key, Integer[] value) {
        data.put(key, new NBTTag(TagType.INT_ARRAY, value));
        return this;
    }

    public ItemUtil setDisplayName(String name) {
        return setDisplayName(false, name);
    }
    public ItemUtil setDisplayName(boolean translateColor, String name) {
        if(translateColor) {
            im.setDisplayName(StringUtils.translate(name));
            return this;
        }
        im.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        return this;
    }
    public ItemUtil setLore(String... lore) {
        return setLore(false, lore);
    }
    public ItemUtil setLore(boolean miniColor, String... lore) {
        List<String> a = new ArrayList<>();
        for(String i : lore) {
            if(miniColor) {
                a.add(StringUtils.translate(i));
                continue;
            }
            a.add(ChatColor.translateAlternateColorCodes('&',i));
        }
        im.setLore(a);
        return this;
    }

    public ItemStack finish() {
        item.setItemMeta(im);
        net.minecraft.server.v1_14_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound d = nmsStack.getOrCreateTag();
        if(data.size() > 0) {
            for (Map.Entry<String, NBTTag> key : data.entrySet()) {
                switch (key.getValue().getType()) {
                    case INT:
                        d.set(key.getKey(), new NBTTagInt(getInt(key.getKey())));
                        break;
                    case DOUBLE:
                        d.set(key.getKey(), new NBTTagDouble(getDouble(key.getKey())));
                        break;
                    case STRING:
                        d.set(key.getKey(), new NBTTagString(getString(key.getKey())));
                        break;
                    case BOOLEAN:
                        d.set(key.getKey(), new NBTTagByte((byte)(getBoolean(key.getKey()) ? 1 : 0)));
                        break;
                    case BYTE:
                        d.set(key.getKey(), new NBTTagByte(getByte(key.getKey())));
                        break;
                    case BYTE_ARRAY:
                        d.set(key.getKey(), new NBTTagByteArray(getByteArray(key.getKey())));
                        break;
                    case FLOAT:
                        d.set(key.getKey(), new NBTTagFloat(getFloat(key.getKey())));
                        break;
                    case INT_ARRAY:
                        d.set(key.getKey(), new NBTTagIntArray(getIntArray(key.getKey())));
                        break;
                    case LONG:
                        d.set(key.getKey(), new NBTTagLong(getLong(key.getKey())));
                        break;
                    case SHORT:
                        d.set(key.getKey(), new NBTTagShort(getShort(key.getKey())));
                        break;
                }
            }
            nmsStack.setTag(d);
            item = CraftItemStack.asBukkitCopy(nmsStack);
        }
        return item;
    }


}
