package net.miniverse.util.itemutils;

import net.miniverse.enchantment.CustomEnchant;
import net.miniverse.enchantment.EnchantHandler;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.StringUtils;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

@SuppressWarnings("all")
public class ItemEnchantUtil {


    public static ItemMeta addEnchantment(ItemStack item, CustomEnchant ench, int xp) {
        if(item==null || ench==null) return null;
        double chance = ench.getChance();
        int maxLevel = ench.getMaxLevel();
        String enchantmentName = ench.getName();
        double successRate = new Random().nextInt(100)+1;
        if(successRate<=chance) {
            int level = maxLevel == ench.getStartLevel() ? maxLevel : new Random().nextInt(maxLevel) + ench.getStartLevel();//Math.round(xp/Math.round(30/maxLevel));
            List<String> newLore = new ArrayList<>();
            if(item.hasItemMeta()) {
                if(item.getItemMeta().hasLore()) {
                    newLore.addAll(item.getItemMeta().getLore());
                }
            }
            newLore.add(StringUtils.translate(enchantmentName) + " " + NumberUtil.convertToRoman(level));
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setLore(newLore);
            itemMeta.addEnchant(ench, level, true);
            return itemMeta;
        }
        return item.getItemMeta();
    }
    public static ItemMeta forceEnchant(ItemStack item, Enchantment ench, int lvl) {
        if(item==null || ench==null) return null;
        List<String> newLore = new ArrayList<>();
        ItemMeta itemMeta = item.getItemMeta();
        List<String> toAdd = new ArrayList<>();
        String name = ench instanceof CustomEnchant ? ench.getName() : fixName(ench);
        String level = ench.getMaxLevel() == 1 && itemMeta.getEnchantLevel(ench) == 1 ? "" : " " + NumberUtil.convertToRoman(itemMeta.getEnchantLevel(ench));
        if(item.containsEnchantment(ench)) {
            toAdd.add(StringUtils.translate("&7" + (name) + level));
            if(item.hasItemMeta())
                for(Enchantment c : item.getItemMeta().getEnchants().keySet()) {
                    String level2 = c.getMaxLevel() == 1 && itemMeta.getEnchantLevel(c) == 1 ? "" : " " + NumberUtil.convertToRoman(itemMeta.getEnchantLevel(c));
                    String x = StringUtils.translate("&7" + name + level2);
                    if(!newLore.contains(x)) {
                        newLore.add(x);
                        toAdd.add(x);
                    }
                }
            newLore.remove(StringUtils.translate("&7" + (name) + level));
            toAdd.remove(StringUtils.translate("&7" + (name) + level));
            itemMeta.removeEnchant(ench);
        }
        if(lvl == 0){
            if(item.hasItemMeta())
                if(item.getItemMeta().hasLore()) {
                    List<String> loop =item.getItemMeta().getLore();
                    loop.removeAll(toAdd);
                    for(String x : loop) {
                        if(!newLore.contains(x) && !toAdd.contains(x)) newLore.add(x);
                    }
                }
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            itemMeta.setLore(newLore);
            return itemMeta;
        }
        itemMeta.addEnchant(ench, lvl, true);
        level = lvl == 1 && ench.getMaxLevel() == 1 ? "" : " " + NumberUtil.convertToRoman(itemMeta.getEnchantLevel(ench));
        newLore.add(StringUtils.translate("&7"+(name) + level));
        toAdd.add(StringUtils.translate("&7"+(name) + level));
        if(item.hasItemMeta())
            if(item.getItemMeta().hasLore()) {
                List<String> loop =item.getItemMeta().getLore();
                loop.removeAll(toAdd);
                for(String x : loop) {
                    if(!newLore.contains(x) && !toAdd.contains(x)) newLore.add(x);
                }
            }
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        itemMeta.setLore(newLore);
        return itemMeta;
    }


    public static ItemMeta forceAddBukkitEnchantment(ItemStack item, Enchantment ench, int lvl) {
        HashMap<Enchantment, Integer> enchs = new HashMap<>();
        for(Enchantment c : item.getEnchantments().keySet()) {
            enchs.put(c, item.getEnchantmentLevel(c));
            item.removeEnchantment(c);
        }
        for(Map.Entry<Enchantment, Integer> i : enchs.entrySet())  {
            item.setItemMeta(forceEnchant(item, i.getKey(), i.getValue()));
        }
        item.setItemMeta(forceEnchant(item,ench,lvl));
        ItemMeta itemMeta = item.getItemMeta();


        return itemMeta;
    }


    public static ItemMeta fixEnchantments(ItemStack item) {
        HashMap<Enchantment, Integer> enchs = new HashMap<>();
        for(Enchantment c : item.getEnchantments().keySet()) {
            enchs.put(c, item.getEnchantmentLevel(c));
            item.removeEnchantment(c);
        }
        for(Map.Entry<Enchantment, Integer> i : enchs.entrySet())  {
            item.setItemMeta(forceEnchant(item, i.getKey(), i.getValue()));
        }
        ItemMeta itemMeta = item.getItemMeta();


        return itemMeta;
    }


    public static ItemMeta removeEnchants(ItemStack item) {
        HashMap<Enchantment, Integer> enchs = new HashMap<>();
        for(Enchantment c : item.getEnchantments().keySet()) {
            item.removeEnchantment(c);
            removeBukkitEnchant(item, c);
        }
        ItemMeta itemMeta = item.getItemMeta();


        return itemMeta;
    }

    public static ItemMeta removeAllEnchants(ItemStack item) {
        HashMap<Enchantment, Integer> enchs = new HashMap<>();
        for(Enchantment c : getEnchants(item.getItemMeta()).keySet()) {
            item.removeEnchantment(c);
            item.setItemMeta(removeBukkitEnchant(item, c));
        }
        ItemMeta itemMeta = item.getItemMeta();


        return itemMeta;
    }

    public static ItemMeta removeAllEnchantments(ItemStack item) {
        if(item==null) return null;
        List<Enchantment> ec = new ArrayList<>();
        ec.addAll(EnchantHandler.getEnchantments());
        ec.addAll(Arrays.asList(Enchantment.values()));
        List<String> newLore = new ArrayList<>();
        if(item.hasItemMeta()) {
            if(item.getItemMeta().hasLore()) {
                for(String x : item.getItemMeta().getLore()) {
                    for(Enchantment ench : ec) {
                        String enchantmentName = fixName(ench);
                        if(StringUtils.translate(x).toLowerCase().contains(StringUtils.translate(enchantmentName.toLowerCase()))) {
                            String p2 = x.toLowerCase().split(StringUtils.translate(enchantmentName))[1],
                                    p1 = x.toLowerCase().split(p2)[0] + (ench.getMaxLevel() == 1 & p2 == "" ? "" : " ");
                            if(x.equalsIgnoreCase(p1 + p2)) {
                                continue;
                            }
                        }
                        newLore.add(StringUtils.translate(x));
                    }
                }
            }
        }
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setLore(newLore);
        return itemMeta;
    }

    public static ItemMeta forceAddEnchantment(ItemStack item, CustomEnchant ench, int lvl) {
        if(item==null || ench==null) return null;
        String enchantmentName = ench.getName();
        List<String> newLore = new ArrayList<>();
        if(item.hasItemMeta()) {
            if(item.getItemMeta().hasLore()) {
                newLore.addAll(item.getItemMeta().getLore());
            }
        }
        newLore.add(StringUtils.translate(enchantmentName) + " " + NumberUtil.convertToRoman(lvl));
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setLore(newLore);
        itemMeta.addEnchant(ench, lvl, true);
        return itemMeta;
    }

    public static ItemMeta removeEnchant(ItemStack item, CustomEnchant ench, int level) {
        if(item==null || ench==null) return null;
        String enchantmentName = ench.getName();
        List<String> newLore = new ArrayList<>();
        if(item.hasItemMeta()) {
            if(item.getItemMeta().hasLore()) {
                for(String x : item.getItemMeta().getLore()) {
                    if(StringUtils.translate(x).equalsIgnoreCase(StringUtils.translate(enchantmentName) + " " + NumberUtil.convertToRoman(level))) {
                        continue;
                    }
                    newLore.add(StringUtils.translate(x));
                }
            }
        }
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setLore(newLore);
        itemMeta.removeEnchant(ench);
        return itemMeta;
    }
    public static ItemMeta removeBukkitEnchant(ItemStack item, Enchantment ench) {
        return removeBukkitEnchant(item.getItemMeta(), ench);
    }
    public static ItemMeta removeBukkitEnchant(ItemMeta item, Enchantment ench) {
        if(item==null || ench==null) return null;
        String enchantmentName = fixName(ench);
        List<String> newLore = new ArrayList<>();
        if(item.hasLore()) {
            for(String x : item.getLore()) {
                if(StringUtils.translate(x).toLowerCase().contains(StringUtils.translate(enchantmentName.toLowerCase()))) {
                    String p2 = x.toLowerCase().split(StringUtils.translate(enchantmentName))[1],
                            p1 = x.toLowerCase().split(p2)[0];
                    if(x.equalsIgnoreCase(p1 + p2)) {
                        continue;
                    }
                }
                newLore.add(StringUtils.translate(x));
            }
        }
        ItemMeta itemMeta = item;
        itemMeta.setLore(newLore);
        itemMeta.removeEnchant(ench);
        return itemMeta;
    }


    public static boolean hasEnchant(ItemStack item, CustomEnchant ench) {
        if(item == null) return false;
        return getEnchant(item, ench) > -1 || item.getItemMeta().hasEnchant(ench);
    }

    public static int getEnchant(ItemStack i, Enchantment ench) {
        ItemMeta im = i.getItemMeta();
        if(im.hasEnchant(ench)) {
            return im.getEnchantLevel(ench);
        }
        String f = StringUtils.stripColor(fixName(ench)).toLowerCase();
        try {
            if(im.hasLore()) {
                for(String x : im.getLore()) {
                    if(StringUtils.stripColor(x).toLowerCase().contains(f)) {
                        String p2 = x.toLowerCase().split(StringUtils.stripColor(f))[1],
                                p1 = x.toLowerCase().split(p2)[0];
                        if(x.equalsIgnoreCase(p1 + p2)) {
                            if(p2.replace(" ", "") == "") {
                                im.addEnchant(ench, 1, true);
                                return 1;
                            }
                            im.addEnchant(ench, NumberUtil.intToRoman(p2.replace(" ", "")), true);
                            i.setItemMeta(im);
                            return NumberUtil.intToRoman(p2.replace(" ", ""));
                        }
                    }
                }
            }
        }catch(Exception e) {

        }
        return -1;
    }

    public static Map<Enchantment, Integer> getEnchants(ItemMeta im) {
        HashMap<Enchantment, Integer> enchants = new HashMap<>();
        List<Enchantment> ec = new ArrayList<>();
        ec.addAll(EnchantHandler.getEnchantments());
        ec.addAll(Arrays.asList(Enchantment.values()));
        try {
            if(im.hasLore()) {
                for(String x : im.getLore()) {
                    for(Enchantment ench : ec)
                        if(StringUtils.translate(x).toLowerCase().contains(StringUtils.translate(ench.getName().toLowerCase()))) {
                            String p2 = x.toLowerCase().split(StringUtils.translate(fixName(ench)))[1],
                                    p1 = x.toLowerCase().split(p2)[0];
                            if(x.equalsIgnoreCase(p1 + p2)) {

                                enchants.put(ench, NumberUtil.intToRoman(StringUtils.stripColor(p2)));
                            }
                        }
                }
            }
        }catch(Exception e) {

        }
        return enchants.isEmpty() & im.hasEnchants() ? im.getEnchants() : enchants;
    }


    public static String fixName(Enchantment ench) {
        String name = ench.getName();
        if(ench instanceof CustomEnchant) return name;
        if(name.toLowerCase().contains("binding_curse")) {
            name = "Curse of Binding";
        }
        if(name.toLowerCase().contains("vanishing_curse")) {
            name = "Curse of Vanishing";
        }
        if(name.toLowerCase().contains("dig_speed")) {
            name = "Efficiency";
        }
        if(name.toLowerCase().contains("arrow_damage")) {
            name = "Power";
        }
        if(name.toLowerCase().contains("arrow_knockback")) {
            name = "Punch";
        }
        if(name.toLowerCase().contains("arrow_fire")) {
            name = "Flame";
        }
        if(name.toLowerCase().contains("arrow_infinite")) {
            name = "Infinity";
        }
        if(name.toLowerCase().contains(Enchantment.DAMAGE_UNDEAD.getName().toLowerCase())) {
            name = "Smite";
        }
        if(name.toLowerCase().contains(Enchantment.DAMAGE_ALL.getName().toLowerCase())) {
            name = "Sharpness";
        }
        if(name.toLowerCase().contains(Enchantment.DAMAGE_ARTHROPODS.getName().toLowerCase())) {
            name = "Bane of Arthropods";
        }
        if(name.toLowerCase().contains(Enchantment.DURABILITY.getName().toLowerCase())) {
            name = "Unbreaking";
        }
        if(name.toLowerCase().contains(Enchantment.LOOT_BONUS_BLOCKS.getName().toLowerCase())) {
            name = "Fortune";
        }
        if(name.toLowerCase().contains(Enchantment.LOOT_BONUS_MOBS.getName().toLowerCase())) {
            name = "Looting";
        }
        if(name.toLowerCase().contains(Enchantment.OXYGEN.getName().toLowerCase())) {
            name = "Respiration";
        }
        if(name.toLowerCase().contains(Enchantment.PROTECTION_ENVIRONMENTAL.getName().toLowerCase())) {
            name = "Protection";
        }
        if(name.toLowerCase().contains(Enchantment.PROTECTION_EXPLOSIONS.getName().toLowerCase())) {
            name = "Blast Protection";
        }
        if(name.toLowerCase().contains(Enchantment.PROTECTION_FALL.getName().toLowerCase())) {
            name = "Feather Falling";
        }
        if(name.toLowerCase().contains(Enchantment.PROTECTION_FIRE.getName().toLowerCase())) {
            name = "Fire Protection";
        }
        if(name.toLowerCase().contains(Enchantment.PROTECTION_PROJECTILE.getName().toLowerCase())) {
            name = "Projectile Protection";
        }
        if(name.toLowerCase().contains(Enchantment.WATER_WORKER.getName().toLowerCase())) {
            name = "Aqua Affinity";
        }
        return StringUtils.getNameFromCaps(name);
    }
}
