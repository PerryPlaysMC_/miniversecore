package net.miniverse.util.itemutils;

import net.miniverse.util.StringUtils;
import net.miniverse.util.config.Config;
import net.miniverse.core.MiniverseCore;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("deprecation")
public class MiniItem {

	public static Config cfg;
	private static MiniverseCore core;
	public static double getVersion() {
		String vString = getVersionStr().replace("v", "");
		double v = 0;
		if (!vString.isEmpty()){
			String[] array = vString.split("_");
			v = Double.parseDouble(array[0] + "." + array[1]);
		}
		return v;
	}

	private static String getVersionStr(){
		String[] array = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",");
		if (array.length == 4)
			return array[3] + ".";
		return "";
	}

	public MiniItem() {
		core = MiniverseCore.getAPI();
		cfg = new Config(MiniverseCore.getAPI(), MiniverseCore.getAPI().getFolder(), "items.yml");
		for(Material m : Material.values()) {
			if(cfg.get("Materials." + m.name().toLowerCase()) == null) {
				String na = m.name().toLowerCase();
//				if(cfg.get("Materials." + na + ".id") == null) {
//					cfg.set("Materials." + na + ".id", m.getId());
//				}

				if(getVersion() == 1.13){
					if(cfg.get("Materials." + na + ".isLegacy") == null) {
						cfg.set("Materials." + na + ".isLegacy", m.isLegacy());
					}
				}
				if(cfg.get("Materials." + na + ".returnType") == null) {
					cfg.set("Materials." + na + ".returnType", m.name());
				}
			}
			//NO UNDERSCORE
			if(cfg.get("Materials." + m.name().toLowerCase().replace("_", "")) == null) {
				String na = m.name().toLowerCase().replace("_", "");
//				if(cfg.get("Materials." + na + ".id") == null) {
//					cfg.set("Materials." + na + ".id", m.getId());
//				}

				if(getVersion() == 1.13){
					if(cfg.get("Materials." + na + ".isLegacy") == null) {
						cfg.set("Materials." + na + ".isLegacy", m.isLegacy());
					}
				}
				if(cfg.get("Materials." + na + ".returnType") == null) {
					cfg.set("Materials." + na + ".returnType", m.name());
				}
			}
		}
	}

	public static ItemStack getItem(String name) {
		Material m = null;
		name = StringUtils.stripColor(name);
		if(name == null)
			return null;
		if(name.isEmpty())
			return null;

        if(cfg.getSection("Materials").contains(name.toLowerCase())) {
            m = Material.getMaterial(cfg.getString("Materials." + name.toLowerCase() + ".returnType"),
                    cfg.getBoolean("Materials." + name.toLowerCase() + ".isLegacy"));
            if(m == null) m = Material.matchMaterial(cfg.getString("Materials." + name.toLowerCase()));
            return new ItemStack(m, 1);
        }

		if(Material.getMaterial(name.toUpperCase()) != null) {
			return new ItemStack(Material.getMaterial(name.toUpperCase()), 1);
		}
		if(Material.matchMaterial(name.toUpperCase()) != null) {
			return new ItemStack(Material.matchMaterial(name.toUpperCase()), 1);
		}
		return null;
	}

	public static ItemStack getItem(String name, int quantity) {
		ItemStack i = getItem(name);
		i.setAmount(quantity);
		return i;
	}

	public static ItemStack getItem(String name, int quantity, int data) {
		ItemStack r = getItem(name, quantity);
		return new ItemStack(r.getType(), quantity, (short)0, (byte)data);
	}







}
