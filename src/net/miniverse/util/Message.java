package net.miniverse.util;

public class Message {

    String msg;

    public Message(String msg) {
        this.msg = msg;
    }

    public com.mojang.brigadier.Message create() {
        return () -> msg;
    }

}
