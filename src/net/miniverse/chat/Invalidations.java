package net.miniverse.chat;

public enum Invalidations {

    SPECIFY_PLAYER("Commands.Invalidations.specify-player"),
    INVALID_PLAYER("Commands.Invalidations.invalid-player"),
    MORE_ARGUMENTS("Commands.Invalidations.more-arguments"),
    TOO_MANY_ARGUMENTS("Commands.Invalidations.too-many-args"),
    INVALID_NUMBER("Commands.Invalidations.invalid-number"),
    INVALID_COORD("Commands.Invalidations.invalid-coord"),
    INVALID_PERMISSION("Commands.Invalidations.invalid-permission"),
    UNKNOWN_ITEM("Commands.Invalidations.unknown-item"),
    MUST_BE_PLAYER("Commands.Invalidations.must-be-Player");
    /*
     specify-player: '[c]Please specify a valid player'
    invalid-player: '[p]Unknown Player'
    more-arguments: '[c]Not enough args'
    too-many-args: '[c]Too many arguments'
    invalid-number: '[c]Please choose a valid number'
    invalid-coord: '[c]Invalid coordinate: [pc]{0}[c].'
    invalid-permission: '[c]You do not have permission for /{0} \n[c]Required permission: [pc]{1}'
    unknown-item: '[c]The item {0} is not a valid item.'
     */
    String path;
    Invalidations(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public String toString() {
        return path;
    }

}
