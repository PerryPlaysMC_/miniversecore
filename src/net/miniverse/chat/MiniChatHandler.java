package net.miniverse.chat;

import net.miniverse.chat.Message;
import net.miniverse.command.MiniChatCommand;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.events.user.UserChatEvent;
import net.miniverse.user.User;
import net.miniverse.user.UserBase;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_14_R1.CraftServer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;
import org.spigotmc.AsyncCatcher;

import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MiniChatHandler implements Listener {



    String original = "§r";
    ArrayList<User> sent = new ArrayList<>();
    @EventHandler
    void onCommand(PlayerCommandPreprocessEvent e) {
        User u = Miniverse.getUser(e);
        for (User us : Miniverse.getOnlineUsers()) {
            if(us != u && us.commandSpy() && !sent.contains(us)) {
                String f = Miniverse.format(Miniverse.getSettings(), "Events.CommandSpy.message", u.getName(), e.getMessage());
                Message msg = new Message(f);
                msg.suggest(fix(Miniverse.format("Events.CommandSpy.suggest", u.getName(), e.getMessage(),
                        u.getLocation().getBlockX()
                        , u.getLocation().getBlockY()
                        , u.getLocation().getBlockZ())))
                        .tooltip(Miniverse.format("Events.CommandSpy.hover", u.getName(), e.getMessage()
                                , u.getLocation().getBlockX()
                                , u.getLocation().getBlockY()
                                , u.getLocation().getBlockZ()));
                msg.send(us);
            }
        }
    }

    String getMessage(User u) {
        return Miniverse.format(Miniverse.getChat(),"Chat.ClickedFormat.message"
                , u.getName(),
                u.getBalance()).split("%message%")[0]
                .replace("%balance%", u.getBalance())
                .replace("%player%", u.getName())
                .replace("%display%", u.getBase().getDisplayName())
                .replace("%pro%", u.getCraftPlayer().getHandle().getProfile().getName())
                .replace("%rank%", u.getPrefix())
                .replace("% rank%", !u.getPrefix().isEmpty() ? " " + u.getPrefix() : "")
                .replace("%rank %", !u.getPrefix().isEmpty() ? u.getPrefix() + " " : "")
                .replace("% rank %", !u.getPrefix().isEmpty() ? " " + u.getPrefix() + " " : "");
    }

    String getHover(User u, String msg) {
        String day = "";
        Date d = new Date(System.currentTimeMillis());
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        switch(dayOfWeek) {
            case 1:
                day = "Sunday";
                break;
            case 2:
                day = "Monday";
                break;
            case 3:
                day = "Tuesday";
                break;
            case 4:
                day = "Wednesday";
                break;
            case 5:
                day = "Thursday";
                break;
            case 6:
                day = "Friday";
                break;
            case 7:
                day = "Saturday";
                break;
        }
        int hour = d.getHours(), min = d.getMinutes(), sec = d.getSeconds();
        return Miniverse.format(Miniverse.getChat(),"Chat.ClickedFormat.hover"
                , u.getName(), msg, u.getBalance(), day, hour, min, sec)
                .replace("%balance%", u.getBalance()).replace("%player%", u.getName())
                .replace("%message%", msg).replace("%rank%", u.getPrefix()).replace("%day%", day+"")
                .replace("%hour%", hour+"")
                .replace("%minute%", min+"")
                .replace("%min%", min+"")
                .replace("%second%", sec+"")
                .replace("%sec%", sec+"");
    }

    String getSuggest(User u, String msg) {
        return Miniverse.getChat().getString("Chat.ClickedFormat.onClick")
                .replace("%balance%",
                        u.getBalance())
                .replace("%player%", u.getName())
                .replace("%display%", u.getBase().getDisplayName())
                .replace("%pro%", u.getCraftPlayer().getHandle().getProfile().getName())
                .replace("%message%", msg)
                .replace("%rank%", u.getPrefix())
                .replace("% rank%", !u.getPrefix().isEmpty() ? " " + u.getPrefix() : "")
                .replace("%rank %", !u.getPrefix().isEmpty() ? u.getPrefix() + " " : "")
                .replace("% rank %", !u.getPrefix().isEmpty() ? " " + u.getPrefix() + " " : "");
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void onChat(AsyncPlayerChatEvent e) {
        User u = Miniverse.getUser(e.getPlayer());
        if(u==null) {
            System.out.println("USER SUCKS");
            return;
        }
        if(!Miniverse.getChat().getBoolean("Chat.clickable")) {
            original = StringUtils.getLastColors(Miniverse.getChat().getString("Chat.format").split("%message%")[0]);
            String f = StringUtils.translateUserPerms(u, original,
                    Miniverse.getChat().getString("Chat.format").replace("%player%", u.getName()).replace("%message%", e.getMessage()));
            e.setFormat(f);
            return;
        }
        if(e.isCancelled()) return;
        String message = StringUtils.translate(getMessage(u)),
                hoverString = StringUtils.translate(getHover(u, e.getMessage())),
                suggest = StringUtils.translate(getSuggest(u, e.getMessage()));
        Message msg = new Message(StringUtils.stripColor(message).endsWith(" ") ? message.substring(0, message.lastIndexOf(" ")) : message);
        msg.suggest(suggest);
        if(!hoverString.isEmpty() && hoverString != ""){
            msg.hover(hoverString);
        }
        if(StringUtils.stripColor(message).endsWith(" ")) {
            msg.then(" ");
        }
        original = StringUtils.getLastColors(message).replace("&", "§").replace(" ", "");
        e.setMessage(original.replace("&", "§") + StringUtils.changeColor(e.getMessage()));
        msg.original = original.replace("&", "§");
        String[] messageSplit = e.getMessage().split(" ");
        boolean invalidItem = false;
        e.setCancelled(true);
        A:for(int i = 0; i < messageSplit.length; i++) {
            String m = StringUtils.changeColor(messageSplit[i]).replace(original.replace("§", "&"), original.replace("&", "§"));
            String og = !StringUtils.stripColor(m).startsWith("https://") ? "https://" + StringUtils.stripColor(m) : StringUtils.stripColor(m);
            m = StringUtils.translateUserPerms(u, original, m);
            if(StringUtils.checkForDomain(og)) {
                msg.then(u, m, TextOptions.KEEP_COLORS).hover("[c]Click to go to: ", "[pc]" + og).link(og);
                if(i < messageSplit.length+-1)msg.then(" ");
                continue;
            }
            og = og.replace("https://", "");
            B:for(int j = 1; j < 10; j++) {
                if(og.equalsIgnoreCase("["+j+"]")) {
                    ItemStack x = u.getInventory().getItem(j+-1);
                    if(x == null || x.getType() == Material.AIR) {
                        u.sendMessage("[c]Invalid item at HotBar Slot '[pc]" + j + "[c]'");
                        msg.then("[pc]Air" + original);
                        if(i < messageSplit.length+-1)msg.then(" ");
                        continue A;
                    }

                    String name =  "[pc]"+(x.hasItemMeta() & (x.getItemMeta().hasDisplayName()
                            | x.getItemMeta().hasLocalizedName()
                    ) ? (x.getItemMeta().hasLocalizedName() ? x.getItemMeta().getLocalizedName()
                            : x.getItemMeta().getDisplayName()) : StringUtils.getNameFromEnum(x.getType()).replace(" ", " [pc]"));
                    msg.then("[pc]" + name + msg.original + " x" + x.getAmount() + msg.last).tooltip(x);
                    if(i < messageSplit.length+-1)msg.then(" ");
                    continue A;
                }
            }
            if(og.equalsIgnoreCase("[item]") || og.equalsIgnoreCase("[i]")) {
                ItemStack x = u.getItemInHand();
                if(x.getType() == Material.AIR) {
                    invalidItem = true;
                    u.sendMessage("[c]Invalid item.");
                    break;
                }

                String name = "[pc]"+(x.hasItemMeta() & (x.getItemMeta().hasDisplayName()
                        | x.getItemMeta().hasLocalizedName()
                ) ? (x.getItemMeta().hasLocalizedName() ? x.getItemMeta().getLocalizedName()
                        : x.getItemMeta().getDisplayName()) : StringUtils.getNameFromEnum(u.getItemInHand().getType()).replace(" ", " [pc]"));
                msg.then("[pc]" + name + " x" + x.getAmount() + msg.last).tooltip(x);
                if(i < messageSplit.length+-1)msg.then(" ");
                continue A;
            }
            msg.then(u, m, TextOptions.KEEP_COLORS);
            if(i < messageSplit.length+-1)msg.then(" ");
        }
        AtomicBoolean b = new AtomicBoolean(invalidItem);
        Bukkit.getScheduler().runTaskLater(MiniverseCore.getAPI(), () -> {
            UserChatEvent e2 = new UserChatEvent(u, e.getMessage().substring(2), Miniverse.getOnlineUsers());
            Bukkit.getPluginManager().callEvent(e2);
            if(e2.isCancelled() || b.get()) return;
            for(User us : e2.getRecipients()) {
                msg.send(us);
            }
            msg.send(Miniverse.getConsole());
        }, 1);

    }


    private String replace(User u, String ms) {
        return Miniverse.replace(u, original, ms);
    }

    public boolean linkIsValid(String targetUrl) {
        HttpURLConnection httpUrlConn;
        try {
            httpUrlConn = (HttpURLConnection) new URL(targetUrl)
                    .openConnection();
            httpUrlConn.setRequestMethod("HEAD");
            httpUrlConn.setConnectTimeout(10000);
            httpUrlConn.setReadTimeout(10000);

            return (httpUrlConn.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            return false;
        }
    }
    public static String fix(String a) {
        String r = a;
        for (ChatColor c : ChatColor.values()) {
            if (r.contains("&" + c.getChar())) {
                r = r.replace("&" + c.getChar(), "");
            }
            if (r.contains("§" + c.getChar())) {
                r = r.replace("§" + c.getChar(), "");
            }
        }
        return r;
    }

    public boolean checkForDomain(String str) {
        String domainPattern = "[a-zA-Z0-9](([a-zA-Z0-9\\-]{0,61}[A-Za-z0-9])?\\.)+(com|net|org|co|gg)";
        Pattern r = Pattern.compile(domainPattern);
        Matcher m = r.matcher(str);
        Pattern invPat = Pattern.compile("(?:https?://)?discord(?:app\\.com/invite|\\.gg)/([a-z0-9-]+)", 2);
        Matcher m2 = invPat.matcher(str);
        return m.find() || m2.find();
    }

    private class holder {
        private Message msg;
        private boolean isBroke;

        holder(Message msg, boolean isTrue, boolean isBroke) {
            this.msg = msg;
            this.isBroke = isBroke;
        }

        public Message getMsg() {
            return msg;
        }

        public boolean isBroke() {
            return isBroke;
        }
    }
}