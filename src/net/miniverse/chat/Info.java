package net.miniverse.chat;

import net.miniverse.command.MiniCommand;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;

public class Info {


    MiniCommand cmd;
    Message fb;
    CommandSource s;
    int amount = 0;

    public Info(MiniCommand cmd) {
        this.cmd = cmd;
        fb = new Message("[c]"+((cmd.getName().charAt(0)+"").toUpperCase()+cmd.getName().substring(1))+" Commands: ");
        fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
    }

    public Info(MiniCommand cmd, String msg) {
        this.cmd = cmd;
        fb = new Message(msg);
        fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
    }
    public Info(MiniCommand cmd, CommandSource s) {
        this.cmd = cmd;
        this.s = s;
        fb = new Message("[c]"+((cmd.getName().charAt(0)+"").toUpperCase()+cmd.getName().substring(1))+": ");
        fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
    }

    public Info(MiniCommand cmd, CommandSource s, String msg) {
        this.cmd = cmd;
        this.s = s;
        fb = new Message(msg);
        fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
    }

    public Info(MiniCommand cmd, CommandSource s, String msg, String[] permissions) {
        this.cmd = cmd;
        this.s = s;
        fb = new Message(msg);
        if(s.hasPermission(permissions)) {
            fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
        }else {
            fb.then("\n    [c]- You do not have permission to view these commands.");
        }
    }

    public Info(MiniCommand cmd, CommandSource s, String[] permissions) {
        this.cmd = cmd;
        this.s = s;
        fb = new Message("[c]"+((cmd.getName().charAt(0)+"").toUpperCase()+cmd.getName().substring(1))+": ");
        if(s.hasPermission(permissions)) {
            fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
        }else {
            fb.then("\n    [c]- You do not have permission to view these commands.");
        }
    }

    public Info setSource(CommandSource s) {
        this.s = s;
        return this;
    }

    String[] perms = new String[]{};
    public Info addSub(String sub) {
        amount++;
        fb.then("\n [c]-/[pc]" + cmd.getName() + " " + sub);
        return this;
    }

    public Info addSub(String[] permissions, String sub) {
        if(s != null) {
            perms = permissions;
            if(s.hasPermission(permissions))
                return addSub(sub);
            else return this;
        }
        perms = new String[]{};
        return addSub(sub);
    }
    
    public Info tooltip(String... messages) {
        if(s != null) {
            if(s.hasPermission(perms) || perms.length == 0) {
                fb.tooltip(messages);
            }
        }else {
            fb.tooltip(messages);
        }
        return this;
    }

    public Info command(String command) {
        if(s != null) {
            if(s.hasPermission(perms) || perms.length == 0) {
                fb.command(command);
            }
        }else {
            fb.command(command);
        }
        return this;
    }

    public Info suggest(String command) {
        if(s != null) {
            if(s.hasPermission(perms) || perms.length == 0) {
                fb.suggest(command);
            }
        }else {
            fb.suggest(command);
        }
        return this;
    }

    public Info insert(String command) {
        if(s != null) {
            if(s.hasPermission(perms) || perms.length == 0) {
                fb.insert(command);
            }
        }else {
            fb.insert(command);
        }
        return this;
    }

    public void send(CommandSource s) {
        if(amount == 0) {

        }
        fb.send(s);
    }
    public void send(OfflineUser s) {
        if(amount == 0) {

        }
        fb.send(s);
    }



}
