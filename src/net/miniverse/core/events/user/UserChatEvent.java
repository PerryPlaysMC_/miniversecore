package net.miniverse.core.events.user;

import java.util.List;
import net.miniverse.user.User;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class UserChatEvent extends UserEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean isCancelled = false;
    private String message;
    private List<User> recipients;

    public UserChatEvent(User user, String message, List<User> recipients) {
        super(user, false);
        this.message = message;
        this.recipients = recipients;
    }

    public boolean isCancelled()
    {
        return isCancelled;
    }

    public void setCancelled(boolean b)
    {
        isCancelled = b;
    }



    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public List<User> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<User> recipients) {
        this.recipients = recipients;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
