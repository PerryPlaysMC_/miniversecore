package net.miniverse.core.events.user;

import net.miniverse.user.User;
import net.miniverse.util.StringUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class UserJoinEvent extends Event
{
  private static final HandlerList handlers = new HandlerList();
  private User u;
  private String message;
  
  public UserJoinEvent(User user, String message) {
    u = user;
    this.message = StringUtils.translate(message);
  }
  
  public String getMessage() {
    return StringUtils.translate(message);
  }
  
  public void setMessage(String message) {
    this.message = StringUtils.translate(message);
  }
  
  public User getUser() {
    return u;
  }
  
  public Player getPlayer() {
    return u.getBase();
  }
  
  public HandlerList getHandlers()
  {
    return handlers;
  }
  

  public static HandlerList getHandlerList()
  {
    return handlers;
  }
}
