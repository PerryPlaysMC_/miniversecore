package net.miniverse.core.events.user;

import net.miniverse.user.User;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class UserActionEvent extends UserEvent implements Cancellable
{
  private static final HandlerList handlers = new HandlerList();
  private Action action;
  private Action actionType;
  private Block clicked_underPlayerBlock; private boolean isCancelled = false;
  private Location to;
  private Location from;
  
  public UserActionEvent(User user, Action action, Action actionType, Location to, Location from, Block clicked_underPlayerBlock) {
    super(user);
    this.action = action;
    this.actionType = actionType;
    this.to = to;
    this.from = from;
    this.clicked_underPlayerBlock = clicked_underPlayerBlock;
  }
  
  public Block getBlock() { return clicked_underPlayerBlock; }
  
  public Action getAction()
  {
    return action;
  }
  
  public Action getActionType() { return actionType; }
  

  public boolean isCancelled()
  {
    return isCancelled;
  }
  
  public void setCancelled(boolean b)
  {
    isCancelled = b;
  }
  
  public HandlerList getHandlers()
  {
    return handlers;
  }
  
  public static HandlerList getHandlerList() {
    return handlers;
  }
  
  public Location getTo() {
    return to;
  }
  
  public void setTo(Location to) {
    this.to = to;
  }
  
  public Location getFrom() {
    return from;
  }
  
  public void setFrom(Location from) {
    this.from = from;
  }
  
  public static enum Action
  {
    LEFT_CLICK,  RIGHT_CLICK, 
    RIGHT_CLICK_BLOCK,  LEFT_CLICK_BLOCK, 
    RIGHT_CLICK_AIR,  LEFT_CLICK_AIR, 
    MOVE,  MOVE_X, 
    MOVE_Y,  MOVE_Z, 
    MOVE_X_Z,  MOVE_Z_Y, 
    MOVE_X_Y,  JUMP, 
    PITCH,  YAW, 
    YAW_RIGHT,  YAW_LEFT, 
    PITCH_UP,  PITCH_DOWN, 
    PITCH_YAW,  FALL, 
    PITCH_UP_YAW_LEFT, 
    PITCH_UP_YAW_RIGHT, 
    PITCH_DOWN_YAW_LEFT, 
    PITCH_DOWN_YAW_RIGHT,  NULL;
    
    private Action() {}
  }
}
