package net.miniverse.core;

import net.miniverse.chat.MiniChatHandler;
import net.miniverse.command.MiniCommand;
import net.miniverse.command.MiniCommandHandler;
import net.miniverse.util.config.Config;
import net.miniverse.util.config.ConfigManager;
import net.miniverse.core.events.ShopInteract;
import net.miniverse.core.events.user.UserSlotChangeEvent;
import net.miniverse.gui.shops.MiniShopHandler;
import net.miniverse.user.permissions.Group;
import net.miniverse.user.permissions.GroupManager;
import net.miniverse.user.*;
import net.miniverse.user.kits.KitManager;
import net.miniverse.util.*;
import net.miniverse.util.AdvancementUtil.FrameType;
import net.miniverse.util.economy.VaultHook;
import net.miniverse.util.itemutils.MiniItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.help.HelpTopic;
import org.bukkit.material.MaterialData;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.SimplePluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static net.miniverse.core.TopicHandler.*;

@SuppressWarnings("all")
public abstract class MiniverseCore extends JavaPlugin implements IMiniverse, Listener {

    private static MiniverseCore api;
    private static CommandSource console;
    private ResourceBundle defaultBundle;
    private String name;
    private Scoreboard sb;
    private Team t;
    private ArrayList<String> com;
    protected static Scoreboard userSB;
    protected static List<OfflineUser> offlineUsers;
    protected static List<OfflineUser> allUsers;
    protected static List<User> onlineUsers;
    protected static List<MiniverseCore> plugins;
    protected static Config shop, items, permLoader;
    protected Config settings, worlds, perm, chat, bannedPlayers, whiteList;
    protected SimplePluginManager spm;
    protected SimpleCommandMap scm;
    protected List<org.bukkit.event.Listener> listeners;

    public SimpleCommandMap getCommandMap() {
        return scm;
    }

    public SimplePluginManager getPluginManager() {
        return spm;
    }

    private void setupSimpleCommandMap() {
        spm = (SimplePluginManager) this.getServer().getPluginManager();
        Field f = null;
        try {
            f = SimplePluginManager.class.getDeclaredField("commandMap");
        } catch (Exception e) {
            e.printStackTrace();
        }
        f.setAccessible(true);
        try {
            scm = (SimpleCommandMap) f.get(spm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Config getWorlds() {
        return worlds;
    }

    public ResourceBundle getDefaultBundle() {
        return defaultBundle;
    }

    public String formatted(Config cfg, String location, Object... objects) {
        if(cfg.get(location) != null) {
            String msg = StringUtils.translateColors('&', cfg.getString(location));
            if(objects.length > 0) {
                for (int i = 0; i < objects.length; i++) {
                    if(objects[i] != null) {
                        msg = msg.replace("{" + i + "}", objects[i].toString());
                    }
                }
            }
            return StringUtils.translate(msg);
        }
        return "Error cannot find: " + location;
    }

    public String translate(String location, Object... objects) {
        if(settings.get(location) == null || settings.get(location) == "") {
            return "Error cannot find: " + location ;
        }
        return StringUtils.translate(MessageFormat.format(settings.getString(location), objects));

    }

    public static List<User> getOnline() {
        return onlineUsers;
    }

    @Override
    public File getFolder() {
        File f = new File("plugins","Miniverse");
        if(!f.isDirectory()) {
            f.mkdirs();
        }
        return f;
    }

    public static List<OfflineUser> getOffline() {
        return offlineUsers;
    }

    @Override
    public List<User> getOnlineUsers() {
        return onlineUsers;
    }

    @Override
    public List<OfflineUser> getOfflineUsers() {
        return offlineUsers;
    }

    @Override
    public void registerListeners(net.miniverse.util.Listener... listeners) {
        Arrays.stream(listeners).forEach(l -> Bukkit.getPluginManager().registerEvents(l, this));
    }

    @Override
    public void registerListeners(org.bukkit.event.Listener... listeners) {
        if(this.listeners == null) this.listeners = new ArrayList<>();
        for(org.bukkit.event.Listener l : listeners) {
            if(l instanceof org.bukkit.event.Listener) {
                if(!this.listeners.contains(l)) {
                    Bukkit.getPluginManager().registerEvents(l, this);
                    this.listeners.add(l);
                }
            }
        }
    }
    public void registerListeners(String packageName) {
        if(getAPI() == null) {
            api = this;
        }
        try {
            for(Class<?> c : getClasses(getFile(), packageName)) {
                if(c != null && c.newInstance() != null && c.newInstance() instanceof Listener) {
                    registerListeners((Listener) c.newInstance());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addCommands(String packageName) {
        if(getAPI() == null) {
            api = this;
        }
        try {
            HashMap<String, String> commands = new HashMap<>();
            for(Class<?> c : getClasses(getFile(), packageName)) {
                if(c != null && c.newInstance() != null && c.newInstance() instanceof MiniCommand) {
                    MiniCommand cmd = (MiniCommand) c.newInstance();
                    addCommands(cmd);
                    StringBuilder sb = new StringBuilder();
                    for(String a : ((MiniCommand)c.newInstance()).getAliases()) {
                        sb.append(a + " ");
                    }
                    String alias = sb.toString().trim();
                    commands.put(cmd.getName(), cmd.getDescription());
                    debug("Loading command: [pc]" + ((MiniCommand)c.newInstance()).getName() + " -Aliases: " + alias);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        reloadTopics();
    }

    public File getFile() {
        return super.getFile();
    }

    public MiniverseCore getPlugin(String name) {
        for(MiniverseCore core : Miniverse.getPlugins()) {
            if(name.equalsIgnoreCase(core.getName())) {
                return core;
            }
        }
        return null;
    }

    public static void debug(Object... messages) {
        if(api.getSettings().getBoolean("settings.isDebug")) {
            LogType type = LogType.DEBUG;
            Bukkit.getConsoleSender().sendMessage(StringUtils.translate("&8[" + type.getName() + "&8] &6"
                    + messages[0].toString() + "\n").trim());
            for(int i = 1; i < messages.length; i++) {
                Bukkit.getConsoleSender().sendMessage(StringUtils.translate(" &a-- &6" + messages[i].toString() + "\n").trim());
            }
        }
    }

    public Set<Class<?>> getClasses(String packageName) {
        return getClasses(getFile(), packageName);
    }

    public Set<Class<?>> getClasses(java.io.File jarFile, String packageName) {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        List<String> names = new ArrayList<>();
        try {
            JarFile file = new JarFile(jarFile);
            for (Enumeration<JarEntry> entry = file.entries(); entry.hasMoreElements();) {
                JarEntry jarEntry = entry.nextElement();
                String name = jarEntry.getName().replace("/", ".");
                if(name.startsWith(packageName) && !name.contains("$") && name.endsWith(".class") && !names.contains(name.split(packageName + ".")[1].replace(".class", ""))) {
                    names.add(name.split(packageName + ".")[1].replace(".class", ""));
                }
            }
            Collections.sort(names);
            for(String name : names) {
                classes.add(Class.forName(packageName + "." + name));
            }
            file.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return classes;
    }

    @Override
    public void addCommands(MiniCommand... commands) {
        for(MiniCommand cmd : commands) {
            addCommand(cmd);
        }
    }

    @Override
    public void addCommand(MiniCommand cmd) {
        Field commandMapField = null;
        CommandMap cMap = scm;
        MiniCommandHandler ccc = null;
        try {
            String name = cmd.getName();
            ccc = new MiniCommandHandler(name);
            ccc.setPermission(cmd.getPermission());
            cMap.register(ccc.getName(), "miniverse", ccc);
            if (cmd.getAliases().size() > 0) {
                for (String alias : cmd.getAliases()) {
                    name = alias;
                    MiniCommandHandler c2 = new MiniCommandHandler(name);
                    c2.setPermission(cmd.getPermission());
                    cMap.register(c2.getName(), "miniverse", c2);
                }
            }
            reloadTopics();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        MiniCommandHandler.addCommand(cmd);
    }

    public InputStream getResource(File f) {
        return getResource(f.getName());
    }

    public InputStream getResource(String f) {
        return super.getResource(f);
    }

    public Config getCustomConfig(String name) {
        return ConfigManager.getConfig(name);
    }

    public void reloadConfig(Config cfg) {
        getCustomConfig(cfg.getLocalName()).reload();
    }
    public void resetConfig(Config cfg) {
        getCustomConfig(cfg.getLocalName()).resetConfig();
    }

    public void reloadConfig(String name) {
        getCustomConfig(name).reload();
    }

    public void reloadAll() {
        for(Config cfgs : ConfigManager.getConfigs()) {
            cfgs.reload();
        }
    }

    public void loadPlayersAndPermissions() {
        for(String perm : permLoader.getStringList("perms")) {
            Permission pe = Bukkit.getPluginManager().getPermission(perm);
            if(pe == null) {
                Bukkit.getPluginManager().addPermission(new Permission(perm));
            }
        }
        Permission pe = Bukkit.getPluginManager().getPermission("*");
        if(pe == null) {
            Bukkit.getPluginManager().addPermission(new Permission("*"));
            pe = Bukkit.getPluginManager().getPermission("*");
        }
        for(Permission p : Bukkit.getPluginManager().getPermissions()) {
            if(p.getName() != "*") {
                pe.getChildren().put(p.getName(), true);
                p.addParent(pe, true);
                p.recalculatePermissibles();
            }
        }
        pe.setDefault(PermissionDefault.FALSE);
        if(onlineUsers != null && onlineUsers.isEmpty()) {
            for(Player p : Bukkit.getOnlinePlayers()) {
                User u = new UserBase(p);

                removeOfflineUser(u);
                addUser(u);
                if(perm.getSection("Permissions.Groups") != null) {
                    for(String group : perm.getSection("Permissions.Groups").getKeys(false)) {
                        Group g = new Group(group, perm.getStringList("Permissions.Groups." + group + ".permissions"),
                                perm.getInt("Permissions.Groups." + group + ".rank"));
                        g.updateInheritance();
                        if(u.getGroups().contains(g)) {
                            for(String per : g.getPermissions()) {
                                u.addPermissions(per);
                            }
                        }
                    }
                }
            }
        }
        if(offlineUsers != null && offlineUsers.isEmpty() || offlineUsers.size() == 0 && Bukkit.getOfflinePlayers() != null && Bukkit.getOfflinePlayers().length > 0) {
            for(OfflinePlayer p : Bukkit.getOfflinePlayers()) {
                OfflineUser u = new OfflineUserBase(p, false);
                if(!allUsers.contains(u)) {
                    allUsers.add(u);
                }
                if(!offlineUsers.contains(u)) {
                    addOfflineUser(u);
                }
            }
        }
    }

    private void loadArrays() {
        if(TopicHandler.topics == null) {
            TopicHandler.topics = new ArrayList<HelpTopic>();
        }
        if(TopicHandler.topics2 == null) {
            TopicHandler.topics2 = new ArrayList<HelpTopic>();
        }
        if(TopicHandler.aliases == null) {
            TopicHandler.aliases = new ArrayList<HelpTopic>();
        }
        if(com == null) {
            com = new ArrayList<>();
        }
        if(offlineUsers == null) {
            offlineUsers = new ArrayList<>();
        }
        if(onlineUsers == null) {
            onlineUsers = new ArrayList<>();
        }
        if(allUsers == null) {
            allUsers = new ArrayList<>();
        }
        if(listeners == null) listeners = new ArrayList<>();
    }

    @Override
    public void onLoad() {
    }

    void loadConfigs() {
        if(settings == null)
            settings = new Config("settings.yml");
        if(worlds == null)
            worlds = new Config("worlds.yml");
        if(perm == null)
            perm = new Config("permissions.yml");
        if(permLoader == null)
            permLoader = new Config("permissionLoader.yml");
        if(shop == null)
            shop = new Config("ShopGui.yml");
        if(chat == null)
            chat = new Config("chat.yml");
        if(bannedPlayers == null)
            bannedPlayers = new Config(new File(".","playerData"), "Banned-Players.yml");
        if(whiteList == null)
            whiteList = new Config(new File(".","playerData"), "whitelist.yml");
        //new Config(new File("plugins/Miniverse/Inventories"), "inventories/BuildTools.yml", "BuildTools.yml");
        //new Config(new File("plugins/Miniverse/Inventories"), "inventories/Food.yml", "Food.yml");
        new MiniItem();
    }

    public void init() {
        if(userSB == null)
            userSB = Bukkit.getScoreboardManager().getNewScoreboard();
        api = this;
        if(console == null) {
            console = new CommandSourceBase(Bukkit.getConsoleSender());
        }
        loadConfigs();
        loadArrays();
        new Miniverse(this);
        if(!Miniverse.getPlugins().contains(this)) {
            Miniverse.getPlugins().add(this);
        }
        GroupManager.reload();
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new TPS(), 100L, 1L);
        registerListeners(new MiniChatHandler(), new ShopInteract());
        PollManager.runPolls();
        VaultHook.setupEconomy();
    }

    @Override
    public void initialize(String name) {
        api = this;
        loadConfigs();
        this.name = name;
        if(!Miniverse.getPlugins().contains(this)) {
            Miniverse.getPlugins().add(this);
        }
        registerListeners(this);
        setupSimpleCommandMap();
        setCommandHelp();
        //MiniGuiManager.reload();
        MiniShopHandler.reload();
        //PlayerInventoryClick.load();
        ShopInteract.load();
        KitManager.loadKits();
        loadPlayersAndPermissions();
    }

    @Override
    public void onDisable() {

    }

    private void setCommandHelp() {
        Collection<Command> coll = scm.getCommands();
        List<String> commandss = new ArrayList<>();
        for(Command cm : scm.getCommands()) {
            if(cm.getAliases() != null) {
                List<String> ali = cm.getAliases();
                for(String a : ali) {
                    TopicHandler.Topic cmm = new TopicHandler.Topic("/"+a, "&eAlias for &r/" + cm.getName());
                    if(!aliases.contains(cmm)) {
                        aliases.add(cmm);
                    }
                }
            }
        }
        List<String> a = new ArrayList<>();
        List<HelpTopic> tp  = new ArrayList<>();
        for(HelpTopic ht : aliases) {
            if(!a.contains(ht.getName() + ":" + ht.getFullText(Bukkit.getConsoleSender()))) {
                a.add(ht.getName() + ":" + ht.getFullText(Bukkit.getConsoleSender()));
            }
        }
        Collections.sort(a);
        for(String al : a) {
            TopicHandler.Topic cmm = new TopicHandler.Topic(al.split(":")[0], al.split(":")[1]);
            if(!tp.contains(cmm)) {
                tp.add(cmm);
            }
        }
        List<HelpTopic> a2 = new ArrayList<>();
        TopicHandler.Index in = new TopicHandler.Index("Aliases", "Aliases", "", tp);
        Bukkit.getHelpMap().addTopic(in);
    }


    @EventHandler
    void slotChange(PlayerItemHeldEvent e) {
        UserSlotChangeEvent event = new UserSlotChangeEvent(Miniverse.getUser(e.getPlayer()), e.getNewSlot(), e.getPreviousSlot());
        Bukkit.getPluginManager().callEvent(event);
        e.setCancelled(event.isCancelled());
    }


    public List<OfflineUser> getAllUsers(){
        return allUsers;
    }

    public static CommandSource getConsole(){
        return console;
    }

    @Override
    public Config getSettings() {
        return settings;
    }

    public static Config getShop() {
        return shop;
    }

    public Config getPermissions() {
        return perm;
    }


    protected Config getBannedPlayers() {
        return bannedPlayers;
    }

    public void removeUser(User u) {
        if(!allUsers.contains(u)) {
            allUsers.add(u);
        }
        if(onlineUsers.contains(u)) {
            onlineUsers.remove(u);
        }
    }

    public void removeOfflineUser(OfflineUser u) {
        OfflineUser a = null;
        for(OfflineUser us : offlineUsers) {
            if(us.getName().equals(u.getName())) {
                a = us;
                break;
            }
        }
        if(a == null) {
            if(!allUsers.contains(u)) {
                allUsers.add(u);
            }
            if(offlineUsers.contains(u))
                offlineUsers.remove(u);
        }
    }

    public void addUser(User u) {
        if(u.getBase().isOnline()) {
            User a = null;
            for(User us : onlineUsers) {
                if(us.getName().equals(u.getName())) {
                    a = us;
                    break;
                }
            }
            if(a == null) {
                if(!allUsers.contains(u)) {
                    allUsers.add(u);
                }
                if(!onlineUsers.contains(u)) {
                    onlineUsers.add(u);
                }
            }
            for(Group g : u.getGroups()) {
                for(String perm : g.getPermissions()) {
                    u.addPermissions(perm);
                }
            }
        }
    }

    public void addOfflineUser(OfflineUser u) {
        if(!u.getBase().isOnline()) {
            OfflineUser a = null;
            for(OfflineUser us : offlineUsers) {
                if(us.getName().equals(u.getName())) {
                    a = us;
                    break;
                }
            }
            if(a == null) {
                if(!allUsers.contains(u)) {
                    allUsers.add(u);
                }
                if(!offlineUsers.contains(u)) {
                    offlineUsers.add(u);
                }
            }
        }
    }


    @Override
    public void log(LogType type, Object... messages) {
        boolean hasPrinted = false;
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        if(messages.length > 0) hasPrinted = true;
        String fullMessage = messages.length > 0 ? StringUtils.translate("&8[" + type.getName() + "&8] &6"
                + messages[0].toString()).trim() : "";
        for(int i = 1; i < messages.length; i++) {
            fullMessage+="\n"+StringUtils.translate("&8[" + type.getName() + "&8] &6" + messages[i].toString()).trim();
        }

        if(stack != null) {
            for(int i = 0; i < stack.length; i++) {
                StackTraceElement s = stack[i];

                String x = "&8[&bStackTrace&8]";
                if( i < 6) {
                    fullMessage+=(i == 0 & !hasPrinted ? "" : "\n") +
                            StringUtils.translate(
                                    x+" &6 "  + s.getFileName() + ":"
                                            + s.getClassName() + "[" + s.getMethodName() + "(" + s.getLineNumber() + ")]").trim();
                }
            }
        }
        getConsole().getConsole().sendMessage(fullMessage);
    }

    public static MiniverseCore getAPI() {
        return api;
    }

}
