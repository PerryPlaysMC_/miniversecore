package net.miniverse.core;

import net.md_5.bungee.api.chat.ComponentBuilder;
import net.miniverse.chat.json.FancyMessage;
import net.miniverse.command.MiniCommand;
import net.miniverse.command.MiniCommandHandler;
import net.miniverse.util.config.Config;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.OfflineUserBase;
import net.miniverse.user.User;
import net.miniverse.util.BanData;
import net.miniverse.util.PlayerData;
import net.miniverse.util.StringUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.command.Command;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("all")
public final class Miniverse {
    private static Miniverse inst;
    private static MiniverseCore main;
    private static org.bukkit.scoreboard.Objective obj;
    private static List<MiniverseCore> plugins;
    public static Character[] OPERATORS = { Character.valueOf('/'), Character.valueOf('*'), Character.valueOf('+'), Character.valueOf('-') };

    public static final String REGEXOPERATORS = "[/+,-,/*,//,-]";

    public static final String REGEXDIGITS = "(\\p{Nd}+)";

    public static ArrayList<Character> operators = new ArrayList<>();

    public static ArrayList<Double> digits = new ArrayList<>();

    protected Miniverse(MiniverseCore main) {
        if (Miniverse.main == null) {
            setMain(main);
            main.loadConfigs();
            plugins = new ArrayList<>();
        }
        inst = this;
    }

    public static org.bukkit.scoreboard.Scoreboard registerNewScoreBoard() { return Bukkit.getScoreboardManager().getNewScoreboard(); }

    public static void log(IMiniverse.LogType type, Object... messages) {
        main.log(type, messages);
    }

    public static void printStackTrace(Exception e) {
        StringBuilder sb = new StringBuilder(6);
        StackTraceElement ea = e.getStackTrace()[0];
        sb.append("\n[c]Error Caused at: Class=[pc]" + ea.getClassName() + "[c](Method=[pc]" + ea.getMethodName() + "[c]:Line=[pc]" + ea.getLineNumber() + "[c])");
        for (int i = 1; i < e.getStackTrace().length; i++) {
            StackTraceElement st = e.getStackTrace()[i];
            sb.append("\n   [c]-Class=[pc]" + st.getClassName() + "[c](Method=[pc]" + st.getMethodName() + "[c]:Line=[pc]" + st.getLineNumber() + "[c])");
        }
        getConsole().sendMessage(sb.toString());
    }

    public static List<MiniverseCore> getPlugins() {
        return plugins;
    }

    public static Miniverse broadCastPerm(String permission, Object... messages) {
        for (Object msg : messages) {
            for (User s : getOnlineUsers()) {
                if (s != null) {
                    if ((permission == "") || (permission == null)) {
                        s.sendMessage(msg + "");

                    }
                    else if (s.hasPermission(permission)) {
                        s.sendMessage(msg + "");
                    }
                }
            }
            Bukkit.getConsoleSender().sendMessage((StringUtils.translate(msg.toString()) + "\n").trim());
        }
        return inst;
    }

    public static Miniverse broadCastPerm(User exclude, String permission, Object... messages) {
        List<User> users = new ArrayList<>();
        for(User u : getOnlineUsers()) {
            if(!users.contains(u) && u.getName() != exclude.getName())
            users.add(u);
        }
        for (Object msg : messages) {
            for (User s : users) {
                if (s != null) {
                    if ((permission == "") || (permission == null)) {
                        s.sendMessage(msg + "");
                    }
                    if ((permission != "") && (permission != null)) if (s.hasPermission(permission)) {
                        s.sendMessage(msg + "");
                    }
                }
            }
            Bukkit.getConsoleSender().sendMessage((StringUtils.translate(msg.toString()) + "\n").trim());
        }
        return inst;
    }

    public static Miniverse broadCast(Object... messages) {
        broadCastPerm("", messages);
        return inst;
    }

    public static Miniverse broadCast(User exclue, Object... messages) {
        broadCastPerm(exclue, "", messages);
        return inst;
    }

    public static String format(Config cfg, String location, Object... objects) {
        return main.formatted(cfg, location, objects);
    }

    public static String format(String location, Object... objects) {
        return main.formatted(getSettings(), location, objects);
    }

    public static boolean checkNull(Object obj) {
        return obj == null;
    }

    public static net.md_5.bungee.api.chat.BaseComponent[] createComponents(String... parts) {
        ComponentBuilder cb = new ComponentBuilder(StringUtils.translate(parts[0]));
        for (int i = 1; i < parts.length; i++) {
            cb.append("\n" + StringUtils.translate(parts[i]));
        }
        return cb.create();
    }

    public static String replace(User u, String original, String ms) {
        for (ChatColor c : ChatColor.values()) {
            if ((u.hasPermission("allcodes", "chatcolor." + c.getChar())) && (ms.contains("&" + c.getChar()))) {
                ms = ms.replace("&" + c.getChar(), "§" + c.getChar());
            }
        }
        return ms;
    }

    public static List<OfflineUser> getAllUsers() {
        return main.getAllUsers();
    }

    public static Config getSettings() {
        return main.settings;
    }

    public static Config getChat() {
        return main.chat;
    }

    public static Config getBannedPlayers() {
        return main.bannedPlayers;
    }

    public static Config getPermissions() {
        return main.perm;
    }

    public static void broadCastJson(FancyMessage... fancymessages) {
        for (FancyMessage fm : fancymessages) {
            if (fm != null) {
                for (User u : getOnlineUsers()) {
                    fm.send(u.getBase());
                }
                fm.send(Bukkit.getConsoleSender());
            }
        }
    }

    public static void broadCastJson(String permission, FancyMessage... fancymessages) {
        for (FancyMessage fm : fancymessages)
            if (fm != null) {
                for (User u : getOnlineUsers()) {
                    if (u.hasPermission(permission)) {
                        fm.send(u.getBase());
                    }
                }
                fm.send(Bukkit.getConsoleSender());
            }
    }

    public static User getUser(HumanEntity uuid) {
        return getUser(uuid.getUniqueId());
    }

    public static User getUser(String name) {
        for (User u : getOnlineUsers()) {
            if ((u.getName().equalsIgnoreCase(name)) || (u.getName().toLowerCase().startsWith(name.toLowerCase()))) {
                return u;
            }
        }
        return null;
    }

    public static Config getPermLoader() {
        return MiniverseCore.permLoader;
    }

    public static User getUserByAll(String name1) {
        String name = StringUtils.stripColor(name1);
        for (User u : getOnlineUsers()) {
            String uName = StringUtils.stripColor(u.getName());
            if (u.getBase().getCustomName() != null) {
                String uCustomName = StringUtils.stripColor(u.getBase().getCustomName());
                if ((uCustomName.equalsIgnoreCase(name)) ||
                        (uCustomName.toLowerCase().startsWith(name.toLowerCase()))) return u;
            }
            if (u.getBase().getDisplayName() != null) {
                String uDisplay = StringUtils.stripColor(u.getBase().getDisplayName());
                if ((uDisplay.equalsIgnoreCase(name)) ||
                        (uDisplay.toLowerCase().startsWith(name.toLowerCase()))) return u;
            }
            String uList = StringUtils.stripColor(u.getBase().getPlayerListName());
            if ((uName.equalsIgnoreCase(name)) ||
                    (uName.toLowerCase().startsWith(name.toLowerCase())) ||
                    (uList.equalsIgnoreCase(name)) ||
                    (uList.toLowerCase().startsWith(name.toLowerCase())) ||
                    (u.getCraftPlayer().getHandle().getProfile().getName().equalsIgnoreCase(name)) ||
                    (u.getCraftPlayer().getHandle().getProfile().getName().toLowerCase().startsWith(name.toLowerCase()))) {
                return u;
            }
        }
        return null;
    }

    public static List<User> getUsers(String name) {
        List<User> users = new ArrayList<>();
        if (name.contains(" ")) {
            for (int a = 0; a < name.split(" ").length; a++) {
                if (getUser(name.split(" ")[a]) != null) {
                    users.add(getUser(name.split(" ")[a]));
                }
            }
        }
        return users;
    }

    public static net.miniverse.util.plugin.Manager getManager() {
        return new net.miniverse.util.plugin.SimpleManager();
    }

    public static User getUser(Player base) {
        return getUser(base.getUniqueId());
    }

    public static User getUser(PlayerEvent e) {
        return getUser(e.getPlayer());
    }

    public static User getUser(UUID uuid) {
        for (User u : getOnlineUsers()) {
            if (u.getUUID().toString().equalsIgnoreCase(uuid.toString())) {
                return u;
            }
        }
        return null;
    }

    public static OfflineUser getOffline(String name) {
        if (getUser(name) != null) return getUser(name);
        if (getOfflineUser(name) != null) { return getOfflineUser(name);
        }
        UUID uuid;
        try
        {
            uuid = PlayerData.getUUID(name);
        } catch (Exception e1) {
            uuid = null;
        }

        if (uuid != null) {
            main.addOfflineUser(new OfflineUserBase(Bukkit.getOfflinePlayer(uuid), false));
            return new OfflineUserBase(Bukkit.getOfflinePlayer(uuid), false);
        }
        return null;
    }

    public static CommandSource getSource(String name) {
        if (getUser(name) != null)
            return getUser(name);
        if ((getConsole().getName().equalsIgnoreCase(name)) || (name.equalsIgnoreCase("server"))) {
            return getConsole();
        }
        return null;
    }

    public static User getUserExact(String name) {
        for (User u : getOnlineUsers()) {
            if (u.getName().equals(name)) {
                return u;
            }
        }
        return null;
    }

    public static TNTPrimed summonTnt(Location loc, int fuseTicks, boolean removeAfterHitGround, boolean explosion) {
        TNTPrimed tnt = loc.getWorld().spawn(loc, TNTPrimed.class);
        tnt.setFuseTicks(fuseTicks);
        tnt.setVelocity(new org.bukkit.util.Vector(0, 0, 0));
        tnt.teleport(tnt.getLocation().add(0.5D, 0.0D, 0.5D));
        if (removeAfterHitGround)
        {
            new org.bukkit.scheduler.BukkitRunnable()
            {
                public void run()
                {
                    if(tnt.isOnGround() || tnt.getFuseTicks() == 1) {
                        if(explosion)
                            tnt.getWorld().createExplosion(tnt.getLocation(), 1.0F);
                        tnt.remove();
                        cancel();
                    }
                }
            }

                    .runTaskTimer(main, 0L, 1L);
        }
        return tnt;
    }

    public static List<Block> getEveryOther3Blocks(Location loc, int rad) {
        List<Block> blocks = new ArrayList<>();
        int radius = rad;
        int a = 0;int i = 0;
        for (int z = loc.getBlockZ() - radius; z <= loc.getBlockZ() + radius; z++) {
            for (int x = loc.getBlockX() - radius; x <= loc.getBlockX() + radius; x++) {
                i++;
                Block b = loc.getWorld().getBlockAt(x, loc.getBlockY(), z);
                if (i >= 63) {
                    i = 0;
                }
                if ((a == 1) && (i <= 21)) {
                    blocks.add(b);
                }
                a++;
                if (a == 3) a = 0;
                blocks.add(null);
            }
        }
        return blocks;
    }

    public static CommandSource getConsole() {
        return MiniverseCore.getConsole();
    }

    public static Block getNearestBlock(int radiusStart, Location loc, Material type) {
        Block r = null;
        int rad = radiusStart;
        for(int radius = 0; radius < rad; radius++) {
            for(int z = loc.getBlockZ() - radius; z <= loc.getBlockZ() + radius; z++) {
                for(int x = loc.getBlockX() - radius; x <= loc.getBlockX() + radius; x++) {
                    Block b = loc.getBlock().getLocation().clone().add(x, 0.0D, z).getBlock();
                    if(radius >= 100) return b;
                    if(b.getType() == type) {
                        r = b;
                        break;
                    }
                }
            }
        }
        if (r == null) {
            r = getNearestBlock(rad + 1, loc, type);
        }
        return r;
    }

    public static OfflinePlayer getOfflinePlayerAddUser(String name) {
        UUID uuid;
        try {
            uuid = PlayerData.getUUID(name);
        } catch (Exception e1) {
            uuid = null;
        }
        if (uuid != null) {
            main.addOfflineUser(new OfflineUserBase(Bukkit.getOfflinePlayer(uuid), false));
            return Bukkit.getOfflinePlayer(uuid);
        }
        return null;
    }

    public static String getOfflinePlayerName(String name) {
        String nameExact = name;
        try {
            UUID uuid = PlayerData.getUUID(name);
            nameExact = PlayerData.getName(uuid);
        }
        catch (Exception localException) {}
        return nameExact;
    }

    public static OfflinePlayer getOfflinePlayer(String name) {
        UUID uuid;
        try {
            uuid = PlayerData.getUUID(name);
        } catch (Exception e1) {
            uuid = null;
        }
        if (uuid != null) {
            return Bukkit.getOfflinePlayer(uuid);
        }
        return null;
    }

    public static String getName(UUID name) {
        String uuid;
        try {
            uuid = PlayerData.getName(name);
        } catch (Exception e1) {
            uuid = null;
        }
        if (uuid != null) {
            return uuid;
        }
        return "Unknown";
    }

    public static OfflinePlayer getOfflinePlayer(UUID uuid) {
        return Bukkit.getOfflinePlayer(uuid);
    }

    public static OfflineUser getOfflineUser(String name) {
        for (OfflineUser u : getOfflineUsers()) {
            if (u.getName().equals(name)) {
                return u;
            }
        }
        if (getUser(name) != null) {
            return getUser(name);
        }
        if (getOfflinePlayer(name) != null) {
            return new OfflineUserBase(getOfflinePlayer(name), false);
        }
        return null;
    }

    public static OfflineUser getOfflineUser(UUID uuid) {
        for (OfflineUser u : getOfflineUsers()) {
            if (u.getUUID().toString() == uuid.toString()) {
                return u;
            }
        }
        return null;
    }

    public static List<User> getOnlineUsers() {
        return main.getOnlineUsers();
    }




    @Deprecated
    public static List<OfflineUser> getOfflineUsers() {
        return main.getOfflineUsers();
    }

    public static Config getWhiteList() {
        return main.whiteList;
    }

    public static MiniCommand parseMini(String command) {
        MiniCommand ret = MiniCommandHandler.getCommand(command);
        if (ret == null) {
            if (MiniCommandHandler.getCommandFromAlias(command.replace("/", "")) == null) {
                ret = MiniCommandHandler.getCommand(command.replace("/", ""));
            } else {
                ret = MiniCommandHandler.getCommandFromAlias(command.replace("/", ""));
            }
        }
        return ret;
    }

    public static Command parseCommand(String command) {
        Command cmd = main.getCommandMap().getCommand(command);
        if (cmd == null) {
            if (command.startsWith("//")) {
                cmd = main.getCommand(command.replaceFirst("/", ""));
            } else if (command.startsWith("/")) {
                cmd = main.getCommandMap().getCommand(command.replace("/", ""));
            }
        }
        return cmd;
    }

    public static Command parseCmd(String command) {
        for (Command cmd : main.getCommandMap().getCommands()) {
            if (cmd.getName().toLowerCase().startsWith(command.replaceFirst("/", ""))) return cmd;
        }
        return null;
    }

    public static Command parseCmdFull(String command) {
        for (Command cmd : main.getCommandMap().getCommands()) {
            if ((cmd.getAliases() != null) &&
                    (cmd.getAliases().size() > 0)) {
                for (String a : cmd.getAliases()) {
                    if (a.toLowerCase().startsWith(command.replaceFirst("/", ""))) { return cmd;
                    }
                }
            }
            if (cmd.getName().toLowerCase().startsWith(command.replaceFirst("/", ""))) return cmd;
        }
        return null;
    }

    public static void removeItem(User u, Material mat, int amount) {
        for (ItemStack i : u.getInventory().getContents()) {
            if ((i != null) &&
                    (i.getType() == mat)) {
                if (i.getAmount() + -1 > 0) {
                    i.setAmount(i.getAmount() + -amount);
                    break;
                }
                i.setAmount(0);
                break;
            }
        }
    }


    public static void removeItem(User u, ItemStack mat, int amount) {
        for (ItemStack i : u.getInventory().getContents()) {
            if ((i != null) &&
                    (i.isSimilar(mat))) {
                if (i.getAmount() + -1 > 0) {
                    i.setAmount(i.getAmount() + -amount);
                    break;
                }
                i.setAmount(0);
                break;
            }
        }
    }


    public static List<Block> getNearbyBlocks(Location location, int radius) {
        List<Block> blocks = new ArrayList<>();
        for (int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
            for (int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; y++) {
                for (int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
                    blocks.add(location.getWorld().getBlockAt(x, y, z));
                }
            }
        }
        return blocks;
    }

    public enum BanType {
        IP,  NAME,  REGULAR;

        BanType() {}
    }

    public static BanEntry banPlayer(BanType type, String banner, String target, String reason, int week, int day, int hour, int min, int sec) { Date d = new Date();
        d.setHours(hour);
        d.setMinutes(min);
        d.setSeconds(sec);
        d.setHours(d.getHours() + day * 24);
        d.setHours(d.getHours() + week * 24 * 7);
        return Bukkit.getBanList(type == BanType.IP ? BanList.Type.IP : ((type == BanType.REGULAR ? 1 : 0) | (type == BanType.NAME ? 1 : 0)) != 0 ? BanList.Type.NAME : BanList.Type.NAME)
                .addBan(target, reason, d, banner == "CONSOLE" ? "Console" : banner);
    }

    public static BanEntry getBan(BanType type, String targetSource) {
        return


                Bukkit.getBanList(type == BanType.IP ? BanList.Type.IP : ((type == BanType.REGULAR ? 1 : 0) | (type == BanType.NAME ? 1 : 0)) != 0 ? BanList.Type.NAME : BanList.Type.NAME).getBanEntry(targetSource);
    }

    public static void BanPlayer(BanType type, String banner, UUID target, String reason, long expire) {
        new BanData(type, banner, target, reason, expire);
    }

    public static void BanPlayer(BanType type, String banner, UUID target, String reason) { new BanData(type, banner, target, reason); }

    public static BanData getBan(UUID user) {
        if ((getBannedPlayers().getSection("Banned") == null) || (getBannedPlayers().getSection("Banned").getKeys(false).size() < 1) || (!getBannedPlayers().getSection("Banned").contains(user.toString()))) return null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        BanData ban = null;
        try {
            ban = new BanData(BanType.valueOf(getBannedPlayers().getString("Banned." + user + ".BanType")), getBannedPlayers().getString("Banned." + user + ".BannedBy"),
                    user, getBannedPlayers().getString("Banned." + user + ".Reason"), format.parse(getBannedPlayers().getString("Banned." + user + ".Expires")).getTime());
        }
        catch (ParseException e) {
            ban = new BanData(BanType.valueOf(getBannedPlayers().getString("Banned." + user + ".BanType")),
                    getBannedPlayers().getString("Banned." + user + ".BannedBy"), user, getBannedPlayers().getString("Banned." + user + ".Reason"));
        }
        return ban;
    }

    public static void pardonPlayer(UUID uuid) {
        getBannedPlayers().set("Banned." + uuid, null);
    }

    public static BanEntry banPlayer(BanType type, String banner, String target, String reason, Date expire) {
        return Bukkit.getBanList(type == BanType.IP ? BanList.Type.IP :
                ((type == BanType.REGULAR ? 1 : 0) | (type == BanType.NAME ? 1 : 0)) != 0 ? BanList.Type.NAME : BanList.Type.NAME)
                .addBan(target, reason, expire, banner == "CONSOLE" ? "Console" : banner);
    }

    public static BanEntry banPlayer(BanType type, String banner, String target, String reason) {
        return Bukkit.getBanList(type == BanType.IP ? BanList.Type.IP :
                ((type == BanType.REGULAR ? 1 : 0) | (type == BanType.NAME ? 1 : 0)) != 0 ? BanList.Type.NAME : BanList.Type.NAME)
                .addBan(target, reason, null, banner == "CONSOLE" ? "Console" : banner);
    }

    private static void setMain(MiniverseCore mainMain) {
        main = mainMain;
    }

    public static double calculate(String math) {
        double r = 0;
        if(math.contains("(") && math.contains(")"))
            for(String x : math.split("\\(")) {
                r += calculateFromString(x.split("\\)")[0].replaceFirst("\\(", ""));
            }
        return r;
    }

    public static double calculateFromString(String math) {
        double ret = 0.0D;
        getDigits(math);
        getOperators(math);
        getNextOperator(operators);
        Iterator<Double> i = digits.iterator();

        while (i.hasNext()) {
            ret += i.next().doubleValue();
        }
        digits.clear();
        operators.clear();
        return ret;
    }

    private static void getNextOperator(ArrayList<Character> operators) {
        for (Character op : OPERATORS) {
            for (int i = 0; i < operators.size(); i++)
            {
                if (operators.get(i).charValue() == '/') {
                    operators.remove(i);
                    digits.set(i, Double.valueOf(digits.get(i).doubleValue() / digits.get(i + 1).doubleValue()));
                    digits.remove(i + 1);
                    i--;
                }
            }

            for (int i = 0; i < operators.size(); i++)
            {
                if (operators.get(i).charValue() == '*') {
                    operators.remove(i);
                    digits.set(i, Double.valueOf(digits.get(i).doubleValue() * digits.get(i + 1).doubleValue()));
                    digits.remove(i + 1);
                    i--;
                }
            }

            for (int i = 0; i < operators.size(); i++) {
                if (operators.get(i).charValue() == '+') {
                    operators.remove(i);
                    digits.set(i, Double.valueOf(digits.get(i).doubleValue() + digits.get(i + 1).doubleValue()));
                    digits.remove(i + 1);
                    i--;
                }
            }

            for (int i = 0; i < operators.size(); i++)
            {
                if (operators.get(i).charValue() == '-') {
                    operators.remove(i);
                    digits.set(i, Double.valueOf(digits.get(i).doubleValue() - digits.get(i + 1).doubleValue()));
                    digits.remove(i + 1);
                    i--;
                }
            }
        }
    }


    public static void getDigits(String math) {
        Pattern r = Pattern.compile("(\\p{Nd}+)");
        Matcher m = r.matcher(math);
        while (m.find()) {
            Double t = Double.valueOf(Double.parseDouble(math.substring(m.start(), m.end())));
            digits.add(t);
        }
    }

    public static void getOperators(String math) {
        Pattern r = Pattern.compile("[/+discord,-,/*,//,-]");
        Matcher m = r.matcher(math);
        while (m.find()) {
            operators.add(Character.valueOf(math.charAt(m.start())));
        }
    }


    public static Set<Block> getNearbyContainers(int max, Block start) {
        return getNearbyContainers(max, start, new HashSet());
    }

    private static Set<Block> getNearbyContainers(int max, Block start, Set<Block> blocks) {
        for (int y = -1; y < 2; y++) {
            for (int x = -1; x < 2; x++) {
                for (int z = -1; z < 2; z++) {
                    if ((blocks.size() >= max) && (max != -1)) {
                        return blocks;
                    }
                    Block block = start.getLocation().clone().add(x, y, z).getBlock();
                    if ((!blocks.contains(block)) && block.getState() instanceof Container) {
                        blocks.add(block);
                        if ((blocks.size() >= max) && (max != -1)) {
                            return blocks;
                        }
                        try {
                            String a = getNearbyContainers(max, block, blocks).size() + "/" + blocks.size();
                            blocks.addAll(getNearbyContainers(max, block, blocks));
                        }
                        catch (Exception localException) {}
                    }
                }
            }
        }
        return blocks;
    }

}
