package net.miniverse.core;

import java.io.File;
import java.util.List;
import net.miniverse.command.MiniCommand;
import net.miniverse.util.config.Config;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import org.bukkit.Server;
import org.bukkit.plugin.Plugin;




public abstract interface IMiniverse
  extends Plugin
{
  public abstract void initialize(String paramString);
  
  public abstract void onEnable();
  
  public abstract void onLoad();
  
  public abstract String translate(String paramString, Object... paramVarArgs);
  
  public abstract void onDisable();
  
  public abstract Server getServer();
  
  public abstract Config getSettings();
  
  public abstract List<User> getOnlineUsers();
  
  public abstract List<OfflineUser> getOfflineUsers();
  
  public abstract void registerListeners(net.miniverse.util.Listener... paramVarArgs);
  
  public abstract void registerListeners(org.bukkit.event.Listener... paramVarArgs);
  
  public abstract File getFolder();
  
  public abstract void addCommand(MiniCommand paramMiniCommand);
  
  public abstract void addCommands(MiniCommand... paramVarArgs);
  
  public abstract void addCommands(String paramString);
  
  public abstract void log(LogType paramLogType, Object... paramVarArgs);
  
  public static enum LogType
  {
    DEBUG("&5&lDebug"), 
    WARNING("&c&lWarning"), 
    ERROR("&4&lError"), 
    INFO("&a&lInfo");
    
    private String name;
    
    private LogType(String name)
    {
      this.name = name;
    }
    
    public String getName() { return name; }
  }
}
