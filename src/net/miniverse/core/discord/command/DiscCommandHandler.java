package net.miniverse.core.discord.command;

import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.miniverse.core.Miniverse;
import net.miniverse.core.discord.DiscordUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DiscCommandHandler extends ListenerAdapter {

    private static List<DiscCommand> cmds;


    public static List<DiscCommand> getCommands() {
        if(cmds == null) cmds = new ArrayList<>();
        return cmds;
    }


    public static void addCommand(DiscCommand cmd) {
        if(!getCommands().contains(cmd)) getCommands().add(cmd);
    }

    public static DiscCommand getCommand(String name) {
        for(DiscCommand cmd : getCommands()) {
            if(cmd.getName().equalsIgnoreCase(name))return cmd;
            if(cmd.getAliases().size() >0) for(String c : cmd.getAliases()) if(c.equalsIgnoreCase(name))return cmd;
        }

        return null;
    }


    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        if(e.getMessage().getAuthor().isBot() || e.getMessage().getAuthor().isFake() || e.getMessage().getContentRaw().isEmpty()) return;
        String prefix = Miniverse.getSettings().isSet("Discord.prefix") ? Miniverse.getSettings().getString("Discord.prefix") : "!";
        if(!e.getMessage().getContentRaw().startsWith(prefix)) {
            return;
        }
        String ogCl = e.getMessage().getContentRaw().split(" ")[0];
        String cl = ogCl.split(prefix)[1];
        String[] ogArgs = e.getMessage().getContentRaw().split(" ");
        String[] args = Arrays.copyOfRange(ogArgs, 1, ogArgs.length);
        DiscCommand c = getCommand(cl);
        if(c==null) {
            e.getMessage().delete().queue();
            DiscordUtils.sendFail(e.getChannel(), "**Invalid command**");
            return;
        }
        if(!e.getChannel().getName().equalsIgnoreCase(c.channel)) {
            e.getMessage().delete().queue();
            if(DiscordUtils.getChannel(c.channel) == null) {
                DiscordUtils.sendError(e.getChannel(), "**Error invalid text channel requirement**", "'" + c.channel + "'");
                return;
            }
            DiscordUtils.sendFail(e.getChannel(), "**Invalid channel**", "Send a command in " + DiscordUtils.getChannel(c.channel).getAsMention());
            return;
        }
        c.u = e.getMessage().getAuthor();
        c.c = e.getChannel();
        c.run(e, e.getChannel(), e.getMessage(), e.getMessage().getAuthor(), cl, args);
        e.getMessage().delete().queue();

    }
}
