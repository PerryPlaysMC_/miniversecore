package net.miniverse.core.discord.command;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.miniverse.core.discord.DiscordUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class DiscCommand {

    protected String name;
    protected List<String> aliases;
    protected String channel;
    protected TextChannel c;
    protected User u;

    public DiscCommand(String name) {
        this(name, new String[] {""});
    }

    public DiscCommand(String name, String... aliases) {
        this("bot-commands", name, aliases);
    }
    public DiscCommand(String channel, String name, String... aliases) {
        this.name=name;
        this.aliases=new ArrayList<>();
        this.aliases.addAll(Arrays.asList(aliases));
        this.channel = channel;
    }


    public abstract void run(GuildMessageReceivedEvent e, TextChannel c, Message msg, User s, String cl, String[] args);

    protected void sendSuccess(String ms, String... message) {
            DiscordUtils.sendSuccess(c, ms, message);
    }

    protected void sendFail(String ms, String... message) {
        DiscordUtils.sendFail(c, ms, message);
    }

    protected void sendError(String ms, String... message) {
        DiscordUtils.sendError(c, ms, message);
    }

    protected void sendPrivateMessage(String ms, String... message) {
        DiscordUtils.sendSuccess(u.openPrivateChannel().complete(), ms, message);
    }

    public String getChannel() {
        return channel;
    }

    public String getName() {
        return name;
    }

    public List<String> getAliases() {
        return aliases;
    }
}
