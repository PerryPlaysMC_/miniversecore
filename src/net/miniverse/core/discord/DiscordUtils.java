package net.miniverse.core.discord;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.*;
import net.miniverse.util.StringUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class DiscordUtils {

    private static JDA jda = MiniverseBot.getBot();

    public static TextChannel getChannel(Guild c, String name) {
        return c.getTextChannelsByName(name, true).get(0);
    }

    public static TextChannel getChannel(String name) {
        return jda.getTextChannelsByName(name, true).get(0);
    }

    public static List<Member> getMembers(Guild c) {
        List<Member> members = new ArrayList<>();
        for(Member u : c.getMembers()) {
            if(!members.contains(u)) members.add(u);
        }
        return members;
    }


    public static Guild getGuild(String name) {
        return jda.getGuildsByName(name, true).get(0);
    }

    public static Role getRole(Guild s, String name) {
        return s.getRolesByName(name, true).get(0);
    }

    public static void scheduleDelayedTask(Runnable r, long delay) {
        MiniverseBot.getScheduler().schedule(r, delay, TimeUnit.MILLISECONDS);
    }
    public static ScheduledFuture<?> scheduleRepeatingTask(Runnable r, long delay, long interval) {
       return MiniverseBot.getScheduler().scheduleWithFixedDelay(r, delay, interval, TimeUnit.MILLISECONDS);
    }

    public static void sendSuccess(MessageChannel c, String ms, String... message) {
       EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.green);
        StringBuilder sb = new StringBuilder();
        String f = ms;
        for(String msg : message) {
            if(msg != "")
                sb.append("\n").append(msg);
        }
        eb.setDescription(ms + sb.toString());
        eb.setTitle("**Success**");
        c.sendMessage(eb.build()).queue();
    }


    public static void sendDebug(MessageChannel c, String ms, String... message) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.magenta);
        StringBuilder sb = new StringBuilder();
        for(String msg : message) {
            if(msg != "")
                sb.append("\n").append(msg);
        }
        eb.setDescription(ms + sb.toString());
        eb.setTitle("**Debug**");
        c.sendMessage(eb.build()).queue();
    }

    public static void sendMinecraft(MessageChannel c, String ms, String... message) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.green);
        StringBuilder sb = new StringBuilder();
        String f = ms;
        for(String msg : message) {
            if(msg != "")
                sb.append("\n").append(msg);
        }
        eb.setDescription(ms + sb.toString());
        eb.setTitle("Minecraft Chat: ");
        c.sendMessage(eb.build()).queue();
    }
    public static void sendMinecraftFormatted(MessageChannel c, String userAndRank, String... message) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.CYAN);
        StringBuilder sb = new StringBuilder();
        for(String msg : message) {
            if(msg != "")
                sb.append("\n").append(msg);
        }
        jda.getUsers().forEach(a -> a.getAsTag());
        eb.setDescription(StringUtils.stripColor(sb.toString().replace("@everyone", "@everyone")));
        eb.setTitle(StringUtils.stripColor(userAndRank));
        c.sendMessage(eb.build()).queue();
    }

    public static void sendError(MessageChannel c, String ms, String... message) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.yellow);
        StringBuilder sb = new StringBuilder();
        String f = ms;
        for(String msg : message) {
            if(msg != "")
                sb.append("\n").append(msg);
        }
        eb.setDescription("**Error**\n"+ms + sb.toString());
        eb.setTitle(null);
        c.sendMessage(eb.build()).queue();
    }

    public static void sendFail(MessageChannel c, String ms, String... message) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.red);
        StringBuilder sb = new StringBuilder();
        String f = ms;
        for(String msg : message) {
            if(msg != "")
                sb.append("\n").append(msg);
        }
        eb.setDescription("**Failed!**\n"+ms + sb.toString());
        eb.setTitle(null);
        c.sendMessage(eb.build()).queue();
    }

}
