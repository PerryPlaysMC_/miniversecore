package net.miniverse.core.discord;


import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.discord.command.DiscCommand;
import net.miniverse.core.discord.command.DiscCommandHandler;
import net.miniverse.util.TimeUtil;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class MiniverseBot {

    private static JDA jda;



    public static void addListeners(ListenerAdapter... adapters) {
        Arrays.stream(adapters).forEach(a -> jda.addEventListener(a));
    }

    public static void addCommand(DiscCommand cmd) {
        DiscCommandHandler.addCommand(cmd);
    }
    private static ScheduledExecutorService scheduler = null;

    public static void addCommands(String packageName) {
        try {
            for(Class<?> c : MiniverseCore.getAPI().getClasses(MiniverseCore.getAPI().getFile(), packageName)) {
                if(c != null && c.newInstance() != null && c.newInstance() instanceof DiscCommand) {
                    DiscCommand cmd = (DiscCommand) c.newInstance();
                    addCommand(cmd);
                    StringBuilder sb = new StringBuilder();
                    for(String a : ((DiscCommand)c.newInstance()).getAliases()) {
                        sb.append(a + " ");
                    }
                    String alias = sb.toString().trim();
                    MiniverseCore.getAPI().debug("Loading command: [pc]" + ((DiscCommand)c.newInstance()).getName() + " -Aliases: " + alias);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startBot() {
        if(Miniverse.getManager().getPlugin("MiniverseBotPlaceHolder") != null) {
            try {
                jda = new JDABuilder(AccountType.BOT).setToken("NDEzMDY4MzQ5Nzg2ODE2NTE1.DywJ5A.N60DrHD2iC8QMYTlLuq6PeQ0Z-A").build();
                jda.getPresence().setPresence(OnlineStatus.ONLINE, Game.of(Game.GameType.DEFAULT, "The SMP, come join!"));
                addListeners(new DiscCommandHandler());
            } catch (Exception e) {
            }
            scheduler = Executors.newScheduledThreadPool(1);
            log(LogType.INFO, "Bot started");
        }
    }

    public static void log(LogType type, Object... messages) {
        for(Object msg : messages) {
            if(jda.getTextChannelsByName("bot-log", true).size() == 0 || jda.getTextChannelsByName("bot-log", true) == null) return;
            TextChannel c = jda.getTextChannelsByName("bot-log", true).get(0);
            if(c==null)return;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            DiscordUtils.sendDebug(c, "[" + format.format(TimeUtil.getDate()) + "] " + type.name() + ": ", msg.toString());
            System.out.println("[" + format.format(TimeUtil.getDate()) + "] " + type.name() + ": " + msg.toString());
        }
    }

    public static void stopBot() {
        if(Miniverse.getManager().getPlugin("MiniverseBotPlaceHolder") != null) {
            jda.shutdownNow();
        }
    }


    public static JDA getBot() {
        return jda;
    }

    public static ScheduledExecutorService getScheduler() {
        return scheduler;
    }
}
