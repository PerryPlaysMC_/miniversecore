package net.miniverse.core.discord;

public enum LogType {
    INFO, ERROR, WARNING, DEBUG

}
