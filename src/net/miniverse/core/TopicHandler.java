package net.miniverse.core;

import net.miniverse.command.MiniCommand;
import net.miniverse.command.MiniCommandHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.help.HelpTopic;
import org.bukkit.help.IndexHelpTopic;

import java.util.*;


public class TopicHandler {
	public static class Index extends IndexHelpTopic {

		public Index(String name, String shortText, String permission, Collection<HelpTopic> topics) {
			super(name, shortText, permission, topics);
		}

		@Override
		public boolean canSee(CommandSender sender) {
			if(sender instanceof ConsoleCommandSender) {
				return true;
			}
			return sender.hasPermission(permission);
		}

		public Collection<HelpTopic> getTopics() {
			return allTopics;
		}

		public void clear() {
			allTopics.clear();
		}

		public void setTopics(Collection<HelpTopic> topics){
			this.allTopics = topics;
		}

	}

	public static class Topic extends HelpTopic {

		private String description, name;

		public Topic(String name, String description) {
			this.name = name;
			this.description = description;
		}

		@Override
		public String getFullText(CommandSender forWho) {
			return ChatColor.translateAlternateColorCodes('&', description);
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public String getShortText() {
			return ChatColor.translateAlternateColorCodes('&', description);
		}

		public void setName(String newName) {
			this.name = newName;
		}



		@Override
		public boolean canSee(CommandSender sender) {
			return true;
		}
	}

    protected static List<HelpTopic> topics, topics2, aliases;
    protected static TopicHandler.Index cc;
    public static void reloadTopics() {
        HashMap<String, String> commands = new HashMap<>();
        for(MiniCommand cmd : MiniCommandHandler.getCommands()) {
            commands.put(cmd.getName(), cmd.getDescription());
        }
        List<String> cmds = new ArrayList<>();
        for(String c : commands.keySet()) {
            if(!cmds.contains(c)) {
                cmds.add(c);
            }
        }
        Collections.sort(cmds);
        for(String c : cmds) {
            TopicHandler.Topic e = new TopicHandler.Topic(c, commands.get(c));
            if (!topics2.contains(e)) {
                topics2.add(e);
            }
        }
        if(cc == null && Bukkit.getHelpMap().getHelpTopic("Miniverse") == null) {
            cc = new TopicHandler.Index("Miniverse", "Help for Miniverse", "miniverse.help", topics2);
        }else {
            cc = (TopicHandler.Index) Bukkit.getHelpMap().getHelpTopic("Miniverse");
            List<HelpTopic> top = new ArrayList<>();
            for(HelpTopic t : topics2) {
                if(!cc.getTopics().contains(t)) {
                    top.add(t);
                }
            }
            top.addAll(cc.getTopics());
            HashMap<String, String> t = new HashMap<>();
            List<HelpTopic> ts = new ArrayList<>();
            for(MiniCommand cmd : MiniCommandHandler.getCommands()) {
                t.put(cmd.getName(), cmd.getDescription());
            }
            List<String> keys = new ArrayList<>();
            for(String a : t.keySet()) {
                keys.add(a);
            }
            Collections.sort(keys);
            for(String a : keys) {
                ts.add(new TopicHandler.Topic("/" + a, t.get(a)));
            }

            cc.setTopics(ts);
        }

        if(!Bukkit.getHelpMap().getHelpTopics().contains(cc)) {
            Bukkit.getHelpMap().addTopic(cc);
        }
        HashMap<String, List<String>> tz = new HashMap<>();
        SimpleCommandMap cMap = MiniverseCore.getAPI().getCommandMap();
        for(Command cmd : cMap.getCommands()) {
            List<String> al = cmd.getAliases();
            Collections.sort(al);
            tz.put(cmd.getName(), al);
        }
        List<String> terpix = new ArrayList<>();
        for(Map.Entry<String, List<String>> entry : tz.entrySet()) {
            for(String a : entry.getValue()) {
                String e = "/" + a + "::§eAlias for §6/" + entry.getKey();
                if(!terpix.contains(e)) {
                    terpix.add(e);
                }
            }
        }
        Collections.sort(terpix);
        List<HelpTopic> topix = new ArrayList<>();
        for(String t : terpix) {
            TopicHandler.Topic e = new TopicHandler.Topic(t.split("::")[0], t.split("::")[1]);
            topix.add(e);
        }
        TopicHandler.Index ca = null;
        if(ca == null && Bukkit.getHelpMap().getHelpTopic("Aliases") != null) {
            ca = (TopicHandler.Index) Bukkit.getHelpMap().getHelpTopic("Aliases");
        }else if(ca == null && Bukkit.getHelpMap().getHelpTopic("Aliases") == null) {
            ca = new TopicHandler.Index("Aliases", "", "", topix);
        }
        List<HelpTopic> top = new ArrayList<>();
        for(HelpTopic t : topics2) {
            if(!cc.getTopics().contains(t)) {
                top.add(t);
            }
        }
        top.addAll(cc.getTopics());
        for(HelpTopic tr : getMiniverseCommands()) {
            if(topix.contains(tr)) continue;
            topix.add(tr);
        }


        ca.setTopics(topix);


        if(Bukkit.getHelpMap().getHelpTopics().contains(ca)) {
            Bukkit.getHelpMap().getHelpTopics().remove(ca);
        }
        Bukkit.getHelpMap().addTopic(ca);
    }



    private static List<HelpTopic> getMiniverseCommands() {
        HashMap<String, List<String>> t = new HashMap<>();
        List<HelpTopic> ts = new ArrayList<>();
        for(MiniCommand cmd : MiniCommandHandler.getCommands()) {
            List<String> al = cmd.getAliases();
            Collections.sort(al);
            t.put(cmd.getName(), al);
        }
        List<String> keys = new ArrayList<>();
        for(String a : t.keySet()) {
            keys.add(a);
        }
        Collections.sort(keys);
        for(String a : keys) {
            for(String e : t.get(a)) {
                ts.add(new TopicHandler.Topic("/" + e, "§eAliase for §6/"+a));
            }
        }
        return ts;
    }
}
