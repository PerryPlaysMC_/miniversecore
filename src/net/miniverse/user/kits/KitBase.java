package net.miniverse.user.kits;

import java.util.List;
import org.bukkit.inventory.ItemStack;

public class KitBase implements Kit
{
  private List<ItemStack> contents;
  private String name;
  
  protected KitBase(List<ItemStack> contents, String name)
  {
    this.name = name;
    setContents(contents);
  }
  


  public List<ItemStack> getContents()
  {
    return contents;
  }
  
  public void setContents(List<ItemStack> contents)
  {
    this.contents = contents;
  }
  
  public String getName()
  {
    return name;
  }
}
