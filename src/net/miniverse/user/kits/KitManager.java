package net.miniverse.user.kits;

import java.util.ArrayList;
import java.util.List;
import net.miniverse.chat.Message;
import net.miniverse.core.MiniverseCore;
import net.miniverse.util.config.Config;
import net.miniverse.core.Miniverse;
import net.miniverse.user.CommandSource;
import org.bukkit.inventory.ItemStack;

public class KitManager {
    private static List<Kit> kits;

    public KitManager() {}

    public static Kit kitByName(String name) {
        if (kits == null) {
            kits = new ArrayList<>();
        }
        for (Kit k : kits) {
            if (k.getName().equalsIgnoreCase(name)) return k;
        }
        return null;
    }

    public static Kit addKit(String name, List<ItemStack> contents) {
        if (kits == null) {
            kits = new ArrayList<>();
        }
        Config cfg = new Config("kits.yml");
        int i = 0;
        for (ItemStack item : contents)
            if (item != null) {
                cfg.setItemStack("kits." + name + ".contents." + i, item);
                i++;
            }
        kits.add(new KitBase(contents, name));
        return kits.get(kits.size() + -1);
    }

    public static void removeKit(Kit kit) {
        if (kits == null) {
            kits = new ArrayList<>();
        }
        kits.remove(kit);
        Config cfg = new Config("kits.yml");
        cfg.set("kits." + kit.getName(), null);
    }

    public static void loadKits() {
        if ((kits == null) || (kits.size() > 0)) {
            kits = new ArrayList<>();
        }
        Config cfg = new Config("kits.yml");
        if (cfg.getSection("kits") == null) return;
        for (String a : cfg.getSection("kits").getKeys(false)) {
            List<ItemStack> contents = new ArrayList<>();
            for (String i : cfg.getSection("kits." + a + ".contents").getKeys(false)) {
                if (i != null) contents.add(cfg.getItemStack("kits." + a + ".contents." + i));
            }
            addKit(a, contents);
        }
    }

    public static Message getKits(CommandSource s) {
        String h = Miniverse.getSettings().getString("Commands.Kit.Header");String f = Miniverse.getSettings().getString("Commands.Kit.Footer");
        String a = h == null ? Miniverse.getSettings().getString("Commands.Kit.Message") : h;
        Message msg = new Message(a);
        int i = 0;
        List<String> names = new ArrayList<>();
        for (Kit k : kits) {
            if (s.hasPermission("miniverse.kits." + k.getName().toLowerCase())) {
                names.add(k.getName());
            }
        }
        if (names.size() > 0) {
            String m = Miniverse.getSettings().getString("Commands.Kit.Message");
            if ((m != null) && (!m.isEmpty()) && (m != "")) {
                msg.then("\n" + Miniverse.getSettings().getString("Commands.Kit.Message"));
            }
            java.util.Collections.sort(names);
        }
        msg.send(s);
        String k2 = Miniverse.getSettings().getString("Commands.Kit.Kits").split("\\{0}")[1];
        String k1 = Miniverse.getSettings().getString("Commands.Kit.Kits").split("\\{0}")[0];
        for (String kit : names) {
            i++;
            if (i == names.size()) {
                if (k2.endsWith(", "))
                    k2 = k2.substring(0, k2.length() + -2) + ".";
                if (k2.endsWith(","))
                    k2 = k2.substring(0, k2.length() + -1) + ".";
                if (k2.endsWith(", [c]"))
                    k2 = k2.substring(0, k2.length() + -5) + ".";
                if (k2.endsWith(",[c]"))
                    k2 = k2.substring(0, k2.length() + -4) + ".";
            }
            if (i == 1) {
                msg.then("\n" + k1 + kit + k2);
            } else {
                msg.then(k1 + kit + k2);
            }
            MiniverseCore.debug(k1 + kit + k2);
            msg.send(s);
        }
        if (names.size() == 0) {
            msg.then(Miniverse.getSettings().getString("Commands.Kit.No-available-kits"));
        }
        if ((f != null) && (!f.isEmpty()) && (f != "")) {
            msg.then("\n").then(f);
        }
        return msg;
    }
}
