package net.miniverse.user.kits;

import java.util.List;
import org.bukkit.inventory.ItemStack;

public abstract interface Kit
{
  public abstract List<ItemStack> getContents();
  
  public abstract void setContents(List<ItemStack> paramList);
  
  public abstract String getName();
}
