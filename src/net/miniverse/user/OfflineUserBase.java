package net.miniverse.user;

import net.miniverse.util.NumberUtil;
import net.miniverse.util.config.Config;
import net.miniverse.core.IMiniverse;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.permissions.Group;
import net.miniverse.user.permissions.GroupManager;
import net.miniverse.user.home.Home;
import net.miniverse.user.home.MiniHome;
import net.miniverse.user.shops.Shop;
import net.miniverse.user.shops.ShopType;
import net.miniverse.util.economy.EconomyManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.PermissionAttachmentInfo;

import java.io.File;
import java.text.DecimalFormat;
import java.util.*;

public class OfflineUserBase implements OfflineUser {

	private OfflinePlayer base;
	private String split = "/";
	private Config cfg, perm = Miniverse.getPermissions();
	public OfflineUserBase(OfflinePlayer base, boolean isLeave) {
		this.base = base;
		cfg = new Config(new File("plugins/Miniverse", "userInfo"), base.getUniqueId() + ".yml");
		cfg.set("name", base.getName());
		cfg.set("uuid", base.getUniqueId()+"");
		cfg.set("Last-Joined", base.getLastPlayed());

		if(!hasAccount()) {
			createAccount();
		}
		if(!EconomyManager.getBaltop().containsKey(getName())) {
			MiniverseCore.getAPI().debug("Added: " + getName() + ": " + getBalanceRaw());
			EconomyManager.getBaltop().put(getName(), getBalanceRaw());
		}
		if(isLeave) {
			Player p = Bukkit.getPlayer(base.getUniqueId());
			for(ItemStack item : p.getInventory().getContents()) {

			}
		}
	}

	public boolean isOnline() {
		return Miniverse.getUser(getName())!=null && Bukkit.getPlayer(getName())!=null;
	}

	enum InventorySection {
		ARMOR("Armor"), REGULAR("Inventory"), EXTRA("Extra");

		String name;
		InventorySection(String name) {
			this.name = name;
		}

		String getName() {
			return name;
		}

	}


	@Override
	public List<Shop> buyShops() {
		cfg.reload();
		if(cfg.getSection("ChestShop.Buy") == null || cfg.getSection("ChestShop.Buy").getKeys(false).size() == 0) return new ArrayList<>();
		List<Shop> locs = new ArrayList<>();
		for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
			locs.add(new Shop(ShopType.BUY, deSerializeLocation(cfg.getString("ChestShop.Buy."+a))));

		}
		return locs;
	}

	@Override
	public void addBuy(Location loc) {
		int i = cfg.getSection("ChestShop.Buy").getKeys(false).size()+1;
		cfg.set("ChestShop.Buy."+ i, serializeLocation(loc));
	}

	@Override
	public void removeBuy(Location loc) {
		for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
			if(check(deSerializeLocation(cfg.getString("ChestShop.Buy."+a)), loc)) {
				cfg.set("ChestShop.Buy."+a, null);
			}
		}
		List<Location> locs = new ArrayList<>();
		for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
			locs.add(deSerializeLocation(a));
		}
		for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
			cfg.set("ChestShop."+a, null);
		}
		int i = cfg.getSection("ChestShop.Buy").getKeys(false).size()+1;
		for(Location a : locs) {
			cfg.set("ChestShop.Buy."+i, serializeLocation(a));
			i++;
		}
	}
	@Override
	public List<Shop> sellShops() {
		cfg.reload();
		if(cfg.getSection("ChestShop.Sell") == null || cfg.getSection("ChestShop.Sell").getKeys(false).size() == 0) return new ArrayList<>();
		List<Shop> locs = new ArrayList<>();
		for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
			locs.add(new Shop(ShopType.SELL, deSerializeLocation(cfg.getString("ChestShop.Sell."+a))));
		}
		return locs;
	}

	@Override
	public void addSell(Location loc) {
		int i = cfg.getSection("ChestShop.Sell").getKeys(false).size()+1;
		cfg.set("ChestShop.Sell."+ i, serializeLocation(loc));
	}

	public Shop getShop(Location loc) {
		if(sellShops() != null && sellShops().size() > 0)
			for(Shop s : sellShops()) {
				if(check(s.getLoc(), loc)) return s;
			}
		if(buyShops() != null && buyShops().size() > 0)
			for(Shop s : buyShops()) {
				if(check(s.getLoc(), loc)) return s;
			}
		return null;
	}

	@Override
	public void removeSell(Location loc) {
		for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
			if(check(deSerializeLocation(cfg.getString("ChestShop.Sell."+a)), loc)) {
				cfg.set("ChestShop.Sell."+a, null);
			}
		}
		List<Location> locs = new ArrayList<>();
		for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
			locs.add(deSerializeLocation(a));
		}
		for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
			cfg.set("ChestShop."+a, null);
		}
		int i = cfg.getSection("ChestShop.Sell").getKeys(false).size()+1;
		for(Location a : locs) {
			cfg.set("ChestShop.Sell."+i, serializeLocation(a));
			i++;
		}
	}


	@Override
	public Home getHome(String name) {
		for(Home home : getHomes()) {
			if(home.getName().equalsIgnoreCase(name)) return home;
		}
		return null;
	}

	@Override
	public Home setHome(String name, Location loc) {
		MiniHome home = new MiniHome(this, name, loc);
		return home;
	}


	@Override
	public void delHome(String name) {
		cfg.set("Homes." + name + ".Location", null);
		cfg.set("Homes." + name, null);
	}

	@Override
	public List<Home> getHomes() {
		List<Home> homes = new ArrayList<>();
		if(cfg.get("Homes") != null) {
			for (String a : cfg.getSection("Homes").getKeys(false)) {
				homes.add(new MiniHome(this, a, deSerializeLocation(cfg.getString("Homes." + a + ".Location")), ""));
			}
		}
		return homes;
	}

	void setItem(InventorySection type, int i, ItemStack item) {
		cfg.set("Inventory." + type.getName() + ".Items." + item.getType().name() + ".amount", item.getAmount());
		cfg.set("Inventory." + type.getName() + ".Items." + item.getType().name() + ".displayName", item.getItemMeta().getDisplayName());
		List<String> enchants = new ArrayList<>();
		if(item.getEnchantments() != null && item.getEnchantments().size() > 0) {
			for(Map.Entry<Enchantment, Integer> ench : item.getEnchantments().entrySet()){
				String add = ench.getKey().getName() + ":" + ench.getValue();
				if(!enchants.contains(add)) {
					enchants.add(add);
				}
			}
			cfg.set("Inventory." + type.getName() + ".Items." + item.getType().name() + ".enchantments", enchants);
		}
	}

	@Override
	public List<Group> getGroups() {
		List<Group> groups = new ArrayList<>();
		for(String group : perm.getStringList("Permissions.Users." + getUUID() + ".groups")) {
			groups.add(GroupManager.getGroup(group));
		}
		return groups;
	}

	@Override@SuppressWarnings("all")
	public Group getPrimaryGroup() {
		List<Group> groups = getGroups();
		if(getGroups().size() == 0) {
			return null;
		}
		int highest = 0;
		for(int i = 1; i < getGroups().size(); i++) {
			if(groups.get(i).getRank() < groups.get(highest).getRank()) highest = i;
		}
		return groups.get(highest);
	}

	public String getPrefix() {
		String prefix = "";
		if(getPrimaryGroup() != null && getPrimaryGroup().getPrefix() != "" && getPrimaryGroup().getPrefix() != null) {
			prefix = getPrimaryGroup().getPrefix();
		}else if(perm.getString("Permissions.User." + getUUID() + ".prefix") != "" && perm.getString("Permissions.User." + getUUID() + ".prefix") != null) {
			prefix = perm.getString("Permissions.User." + getUUID() + ".prefix");
		}
		return ChatColor.translateAlternateColorCodes('&', prefix);
	}

	@Override
	public void setPrefix(String string) {

	}

	@Override
	public Config getConfig() {
		return cfg;
	}

	@Override
	public OfflinePlayer getBase() {
		return base;
	}

	//Economy START


	@Override
	public boolean balanceTooBig(double amount) {
		double money = getBalanceRaw() + amount;
		if(isOnline()) {
			User u = Miniverse.getUser(getName());
			if(u != null)
				return u.balanceTooBig(amount);
		}
		if(Miniverse.getSettings().getInt("Commands.Economy.maxBal") <= -1) {
			return false;
		}
		if(money >= Miniverse.getSettings().getInt("Commands.Economy.maxBal")) {
			return true;
		}
		return false;
	}

	@Override
	public void createAccount() {
		cfg.set("Account.balance", 0);
		if(EconomyManager.getBaltop().containsKey(getName())) {
			EconomyManager.getBaltop().remove(getName());
		}
		if(isOnline()) {
			User u = Miniverse.getUser(getName());
			if(u != null)
				u.createAccount();
		}
		EconomyManager.getBaltop().put(getName(), getBalanceRaw());
	}

	@Override
	public void deleteAccount() {
		cfg.set("Account.balance", null);
		cfg.set("Account", null);
		if(EconomyManager.getBaltop().containsKey(getName())) {
			EconomyManager.getBaltop().remove(getName());
		}
		if(isOnline()) {
			User u = Miniverse.getUser(getName());
			if(u != null)
				u.deleteAccount();
		}
	}

	@Override
	public boolean hasAccount() {
		if(isOnline()) {
			User u = Miniverse.getUser(getName());
			if(u != null)
				return u.hasAccount();
		}
		return cfg.getSection("Account") != null;
	}

	@Override
	public boolean hasEnough(double amount) {

		if(isOnline()) {
			User u = Miniverse.getUser(getName());
			if(u != null)
				return u.hasEnough(amount);
		}
		return getBalanceRaw() >= amount || (getBalanceRaw() +- amount) >= Miniverse.getSettings().getDouble("Commands.Economy.minBal");
	}

	@Override
	public Double getBalanceRaw() {
		if(isOnline()) {
			User u = Miniverse.getUser(getName());
			if(u != null)
				return u.getBalanceRaw();
		}
		return cfg.getDouble("Account.balance");
	}

    @Override
    public String getBalance() {
        if(isOnline()) {
            User u = Miniverse.getUser(getName());
            if(u != null)
                return u.getBalance();
        }
        return NumberUtil.format(getBalanceRaw());
    }


    @Override
    public void withDrawMoney(double money) {
        double d = money;
        setBal(getBalanceRaw()+-d);
    }

    @Override
    public void depositMoney(double money) {
        double d = money;
        setBal(getBalanceRaw()+d);
    }

	@Override
	public void setBal(double money) {
        if(isOnline()) {
            User u = Miniverse.getUser(getName());
            if(u != null) {
                u.setBal(money);
                return;
            }
        }
		Double d = money;
		cfg.set("Account.balance", d);
		if(EconomyManager.getBaltop().containsKey(getName())) {
			EconomyManager.getBaltop().remove(getName());
		}
		EconomyManager.getBaltop().put(getName(), getBalanceRaw());
	}

	//Economy END

	//OfflinePlayer START

	@Override
	public UUID getUUID() {
		return base.getUniqueId();
	}

	@Override
	public void addPermissions(String... permissions) {
		if(isOnline()) {
			User u = (User) this;
			u.addPermissions(permissions);
		}
	}

	@Override
	public void removePermissions(String... permissions) {
		if(isOnline()) {
			User u = (User) this;
			u.removePermissions(permissions);
			u.getBase().recalculatePermissions();
		}
	}

	@Override
	public Set<PermissionAttachmentInfo> getEffectivePermissions() {
		if(isOnline())
			return ((Player)base).getEffectivePermissions();
		return new HashSet<>();
	}

	@Override
	public void clearPermissions() {
		if(isOnline()) {
			User u = (User) this;
			u.clearPermissions();
		}
	}

	@Override
	public String getName() {
		String name = base.getName() == null ? "Invalid player" : base.getName();
		return name;
	}

	//OfflinePlayer END

	//Online User START

	@Override
	public void sendMessage(String... messages) {
		if(!isOnline()) {
			return;
		}
		Miniverse.getUser(this.getName()).sendMessage(messages);
	}

	@Override
	public void sendMessage(Config cfg, String location, Object... objects) {
		sendMessage(MiniverseCore.getAPI().formatted(cfg, location, objects));
	}

	@Override
	public void sendMessage(String location, Object... objects) {
		if(isOnline()) {
			sendMessage(Miniverse.getSettings(), location, objects);
		}
	}

	//Online User END


	@Override
	public Location deSerializeLocation(String loc) {
		if(loc != null && loc != "" && loc.contains(split)) {
			String[] l = loc.split(split);
			Location location =
					new Location(
							Bukkit.getWorld(l[0]),
							Double.parseDouble(l[1]),
							Double.parseDouble(l[2]),
							Double.parseDouble(l[3]),
							Float.parseFloat(l[4]),
							Float.parseFloat(l[5])
					);
			return location;
		}
		return null;
	}

	@Override
	public String serializeLocation(Location loc) {
		DecimalFormat df = new DecimalFormat(".000");
		String serialized = loc.getWorld().getName()
							+ split + df.format(loc.getX())
							+ split + df.format(loc.getY())
							+ split + df.format(loc.getZ())
							+ split + loc.getYaw()
							+ split + loc.getPitch();
		return serialized;
	}

	boolean check(Location l1, Location l2) {
		if(l1 == null || l2 == null){
			return false;
		}
		return l1.getBlockX() == l2.getBlockX() && l1.getBlockY() == l2.getBlockY() && l1.getBlockZ() == l2.getBlockZ();
	}

}
