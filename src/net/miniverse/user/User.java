package net.miniverse.user;

import com.mojang.authlib.GameProfile;
import java.util.List;
import net.minecraft.server.v1_14_R1.Packet;
import net.miniverse.chat.json.FancyMessage;
import net.miniverse.user.home.Home;
import net.miniverse.user.shops.Shop;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public abstract interface User extends CommandSource, OfflineUser {
  Player getBase();
  
  void setBase(Player paramPlayer);

  String getCommand(Material tool);

  void setCommand(Material key, String value);

  boolean isSet(Material key);

  void removeTool(Material key);

  PlayerInventory getInventory();
  
  void updateInventory();
  
  UserBase getSuper();
  
  Inventory getProfileInven();
  
  ItemStack getSkull();

  void openInventory(Inventory inv);
  
  void onInventoryClick(InventoryClickEvent e);
  
  void kick(CommandSource source, String reason, boolean ban);
  
  void kick(String reason);
  
  void sendPacket(Packet<?> packet);
  
  Home getHome(String home);
  
  void delHome(String home);
  
  String getLang();
  
  void setLang(String lang);
  
  List<Home> getHomes();
  
  Home setHome(String home);
  
  Home setHome(String home, Location loc);
  
  void toggleVeinMine();
  
  boolean isVeinMine();
  
  Shop getShop(Location loc);
  
  List<Shop> sellShops();
  
  void addSell(Location loc);
  
  void removeSell(Location loc);
  
  List<Shop> buyShops();
  
  void addBuy(Location loc);
  
  void removeBuy(Location loc);
  
  UserBase getMiniUser();
  
  String getOriginalName();
  
  CraftPlayer getCraftPlayer();
  
  void setProfile(GameProfile pro);
  
  boolean isPendingTpa(User user );
  
  void setTpa(User user );
  
  User getTpa();
  
  void setTpaHere(User user );
  
  User getTpaHere();
  
  double getHealth();
  
  void setHealth(double heath);
  
  double getMaxHealth();
  
  void setMaxHealth(double maxHealth);
  
  boolean commandSpy();
  
  void toggleCommandSpy();
  
  void toggleCommandSpy(boolean toggle);
  
  GameMode getGameMode();
  
  void startAFKTimer();
  
  boolean isAFK();
  
  void toggleAFK();
  
  void setGamemode(GameMode gamemode);
  
  void teleport(Location loc);
  
  void tp(Location loc);
  
  Location getLocation();
  
  int getX();
  
  int getY();
  
  int getZ();
  
  boolean isSlowFall();
  
  List<String> getFriends();
  
  boolean hasFriend(User user);
  
  void addFriend(User user);
  
  void addFriend(String user);
  
  User getFriend(String user);
  
  User getFriend(User user);
  
  void messageFriend(String user, String... messages);
  
  void messageFriend(User user, String... messages);
  
  User getFriendReply();
  
  void setFriendReply(User user);
  
  ItemStack getItemInHand();
  
  void addPermissions(String... permissions);
  
  void removePermissions(String... permissions);
  
  void clearPermissions();
  
  String getPrefix();
  
  void setPrefix(String prefix);
  
  boolean withdrawItem(double money);
  
  boolean isCooldown(String key);
  
  void startCooldown(String key, double time);
  
  double getCooldownRemains(String key);
  
  void sendFancyMSG(FancyMessage msg);
  
  String toString();
}
