package net.miniverse.user;

import java.util.HashMap;
import net.md_5.bungee.api.chat.BaseComponent;
import net.miniverse.chat.Invalidations;
import net.miniverse.chat.Message;
import net.miniverse.util.config.Config;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.util.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class CommandSourceBase implements CommandSource, Datable {
    private CommandSender s;
    private CommandSource replyTo;
    HashMap<String, Object> data = new HashMap<>();

    public CommandSourceBase(CommandSender s) {
        this.s = s;
    }

    public boolean isPlayer() {
        return s instanceof Player;
    }

    public CommandSender getSender() {
        return s;
    }

    public ConsoleCommandSender getConsole() {
        return Bukkit.getConsoleSender();
    }

    public CommandSource getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(CommandSource s) {
        replyTo = s;
    }

    public void sendMessage(BaseComponent... components) {
        s.spigot().sendMessage(components);
    }

    public boolean hasPermission(String... permissions) {
        for (String perm : permissions) {
            if ((s.hasPermission("*")) ||
                    (s.hasPermission(perm + ".*")) ||
                    (s.hasPermission("miniverse.command." + perm + ".*")) ||
                    (s.hasPermission("miniverse." + perm + ".*")) ||
                    (s.hasPermission("miniverse.*")) ||
                    (s.hasPermission(perm)) ||
                    (s.hasPermission("miniverse." + perm)) ||
                    (s.hasPermission("miniverse.command." + perm)) ||
                    (s.isOp())) {
                return true;
            }
        }
        return false;
    }


    public void sendMessage(String... messages) {
        for (String message : messages) {
            String msg = StringUtils.translate(message);
            new Message(msg).send(this);
        }
    }


    public void sendMessage(Config cfg, String location, Object... objects) {
        sendMessage(MiniverseCore.getAPI().formatted(cfg, location, objects));
    }

    public void sendMessage(String location, Object... objects) {
        sendMessage(Miniverse.getSettings(), location, objects);
    }

    public String getName() {
        return isPlayer() ? s.getName() : "Console";
    }

    public User getUser() {
        return Miniverse.getUser(s.getName());
    }

    @Override
    public void sendMessage(Invalidations... messages) {
        for (Invalidations inv : messages) {
            sendMessage(Miniverse.getSettings(), inv.getPath());
        }
    }

    public Player getBase() {
        return (Player)s;
    }

    public void setVanish(boolean vanish) {
        if (isPlayer()) {
            getUser().setVanish(vanish);
        }
    }

    public boolean isVanished() {
        if (isPlayer()) {
            return getUser().isVanished();
        }
        return false;
    }

    public Object getData(String key) {
        return data.get(key);
    }

    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(String key, Object obj) {
        data.put(key, obj);
    }
}
