package net.miniverse.user;

import java.util.HashMap;

public abstract interface Datable
{
  public abstract Object getData(String paramString);
  
  public abstract HashMap<String, Object> getData();
  
  public abstract void setData(String paramString, Object paramObject);
}
