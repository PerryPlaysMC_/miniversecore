package net.miniverse.user;

import com.mojang.authlib.GameProfile;
import net.md_5.bungee.api.chat.BaseComponent;
import net.minecraft.server.v1_14_R1.Packet;
import net.minecraft.server.v1_14_R1.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_14_R1.PacketPlayOutPlayerInfo;
import net.miniverse.chat.Invalidations;
import net.miniverse.chat.Message;
import net.miniverse.chat.json.FancyMessage;
import net.miniverse.core.IMiniverse;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.events.user.UserBanEvent;
import net.miniverse.core.events.user.UserKickEvent;
import net.miniverse.user.home.Home;
import net.miniverse.user.home.MiniHome;
import net.miniverse.user.permissions.Group;
import net.miniverse.user.permissions.GroupManager;
import net.miniverse.user.shops.Shop;
import net.miniverse.user.shops.ShopType;
import net.miniverse.util.NumberUtil;
import net.miniverse.util.ScoreboardUtil;
import net.miniverse.util.StringUtils;
import net.miniverse.util.TimeUtil;
import net.miniverse.util.config.Config;
import net.miniverse.util.economy.EconomyManager;
import net.miniverse.util.itemutils.ItemUtil;
import org.apache.commons.lang.Validate;
import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.permissions.*;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

;

@SuppressWarnings("all")
public class UserBase implements User {

    private Player base;
    private User user, friend, tpa, tpaHere;
    private HashMap<User, Boolean> isPending;
    private PermissibleBase base2;
    private Inventory inv, pro;
    private HashMap<String, Double> cooldown;
    private CommandSource replyTo;
    private MiniverseCore core;
    private Config cfg, perm = Miniverse.getPermissions();
    private String split = "/", originalName;
    private	HashMap<UUID,PermissionAttachment> attach = new HashMap<>();
    public PermissionAttachment attachment;
    private boolean isJoin = false;
    private boolean isAFK = false;
    public boolean cancelAll = false;
    private Integer i;
    private BukkitTask task;


    public UserBase(Player base) {
        this(base, false);
    }
    String proName, invName;

    public UserBase(Player base, boolean isJoin) {
        this.base = base;
        this.base2 = new PermissibleBase(base);
        proName = StringUtils.translate("[pc]" + getName() + ": ");
        invName = StringUtils.translate("[pc]" + getName() + "[c]'s Profile: ");
        isPending = new HashMap<>();
        originalName = base.getName();
        cfg = new Config(new File("plugins/Miniverse", "userInfo"),
                "DefaultPlayerFile.yml", base.getUniqueId() + ".yml");
        setInventory();
        core = MiniverseCore.getAPI();
        user = this;
        this.isJoin = isJoin;
        if(!isJoin) {
            clearPermissions();
        }
        if(attachment == null) {
            attachment = base.addAttachment(MiniverseCore.getAPI());
            for(PermissionAttachmentInfo i : base.getEffectivePermissions()) {
                attachment.setPermission(i.getPermission(), i.getValue());
            }
        }
        if(!attach.containsKey(getUUID()) && attachment != null) {
            attach.put(getUUID(), attachment);
        }
        cooldown = new HashMap<>();
        cfg.set("name", base.getName());
        cfg.set("uuid", base.getUniqueId()+"");
        cfg.set("Last-Joined", base.getLastPlayed());
        cfg.set("Join/Reload-Health", base.getHealth());
        cfg.set("max-Health", base.getMaxHealth());
        if(!cfg.isSet("IpAddress") && base.getAddress()!=null&&base.getAddress().getAddress() != null) {
            cfg.set("IpAddress",
                    base.getAddress()
                            .getAddress().toString());
        }
        if(cfg.get("Settings.vanish") == null) {
            cfg.set("Settings.vanish", false);
        }
        if(cfg.get("Settings.Chat") == null) {
            cfg.set("Settings.Chat", true);
        }
        if(cfg.get("Settings.HoverText") == null) {
            cfg.set("Settings.HoverText", false);
        }
        setVanish(isVanished());
        if(!hasAccount()) {
            createAccount();
        }
        if(!EconomyManager.getBaltop().containsKey(getName())) {
            EconomyManager.getBaltop().put(getName(), getBalanceRaw());
        }
        if(!perm.getConfig().isSet("Permissions.Users." + getUUID() + ".name") || !perm.getString("Permissions.Users." + getUUID() + ".name").equals(getName()))
            perm.set("Permissions.Users." + getUUID() + ".name", getName());
        for(String perm : perm.getStringList("Permissions.Users." + getUUID() + ".permissions")) {
            addPermissions(perm);
        }
        for(Group g : getGroups()) {
            for(String perm : g.getPermissions()) {
                addPermissions(perm);
            }
        }
        try{
            base.setScoreboard(Miniverse.registerNewScoreBoard());
        }catch (IllegalStateException ee){}
        Scoreboard x = Miniverse.registerNewScoreBoard();
        ScoreboardUtil sb = new ScoreboardUtil(getName(), "dummy");
        sb.setDisplay("[c]Info: [pc]"+getName(), DisplaySlot.SIDEBAR);
        update(sb, true);

        (new BukkitRunnable() {
            @Override
            public void run() {

                try{
                base.setScoreboard(sb.finish());
                }catch (IllegalStateException ee){}
            }
        }).runTaskLater(MiniverseCore.getAPI(), 20*5);
        boolean cancelIt = false;
        (new BukkitRunnable(){
            boolean isSet = false, isSet2 = false;
            boolean swap = false;
            int i = 0;
            @Override
            public void run() {
                if(cancelAll) {
                    cancel();
                    return;
                }
                if(cfg.getBoolean("Settings.HoverText")) {
                    update(sb, false);
                    if(!isSet) {
                        base.setScoreboard(sb.finish());
                        isSet = true;
                        isSet2 = false;
                    }
                }else {
                    if (!isSet2) {
                        isSet = false;
                        (new BukkitRunnable(){
                            @Override
                            public void run() {
                                isSet2 = true;
                            }
                        }).runTaskLater(MiniverseCore.getAPI(), 20);
                        try{
                        base.setScoreboard(x);
                        }catch (IllegalStateException ee){}
                    }
                }
            }
        }).runTaskTimer(MiniverseCore.getAPI(), 0, 2);
//
//        (new BukkitRunnable() {
//            @Override
//            public void run() {
//                if(cfg.getBoolean("Settings.HoverText")) {
//                    Date now = new Date();
//                    ZoneId zz = ZoneId.of( "America/Montreal" );
//                    ZonedDateTime zdt = ZonedDateTime.now(zz);
//                    //Date(int year, int month, int date, int hrs, int min, int sec
//                    Date d = new Date(zdt.getYear(), zdt.getMonthValue(), zdt.getDayOfMonth(), zdt.getHour(), zdt.getMinute(), zdt.getSecond());
//                    String output = TimeUtil.formatDate(zdt);
//                    String b = base.getLocation().getBlock().getBiome().name();
//                    String x, z;
//                    if(base.getWorld().getName().toLowerCase().contains("nether")) {
//                        x = getX() + "[c]([pc]" + (getX()*8) + "[c])";
//                        z = getZ() + "[c]([pc]" + (getZ()*8) + "[c])";
//                    }else {
//                        x = getX() + "[c]([pc]" + (getX()/8) + "[c])";
//                        z = getZ() + "[c]([pc]" + (getZ()/8) + "[c])";
//                    }
//                    Chunk c = base.getLocation().getChunk();
//                    ActionBarUtil.sendActionBar(base, "" +
//                                                      "[c]Biome: [pc]" + b.charAt(0) + b.substring(1).toLowerCase() + " " +
//                                                      "[c]X: [pc]" + x + " " +
//                                                      "[c]Y: [pc]" + getY() + " " +
//                                                      "[c]Z: [pc]" + z + " " +
//                                                      (c.isSlimeChunk() ? "[pc]SLIME CHUNK![c] " : "") +
//                                                      "[c]Time(EST): [pc]" + output);
//                }
//            }
//        }).runTaskTimer(MiniverseCore.getAPI(), 0, 1);
        isAFK = false;
        setName("&e", "");
        startAFKTimer();

    }

    public String x() {
        String f = cfg.getString("Settings.TimeZone");
        String r = "";
        for(int i = 0; i<f.split("/").length;i++) {
            String x = f.split("/")[i];
            if(x.contains("_")) {
                r+=(x.charAt(0)+"").toUpperCase()
                   +x.split("_")[0].toLowerCase().replaceFirst(x.charAt(0)+"", (x.charAt(0)+"").toUpperCase())
                   +x.split("_")[1].toLowerCase().replaceFirst(x.split("_")[1].charAt(0)+"", (x.split("_")[1].charAt(0)+"").toUpperCase())
                   +(i == f.split("/").length+-1?"":"/");
                continue;
            }
            r+=(x.charAt(0)+"").toUpperCase()+x.substring(1).toLowerCase()+(i == f.split("/").length+-1?"":"/");
        }
        return r;
    }

    void update(ScoreboardUtil sb, boolean is) {
        String x, z;
        if(base.getWorld().getName().toLowerCase().contains("nether")) {
            x = getX() + "[c]([pc]" + (getX()*8) + "[c])";
            z = getZ() + "[c]([pc]" + (getZ()*8) + "[c])";
        }else {
            x = getX() + "[c]([pc]" + (getX()/8) + "[c])";
            z = getZ() + "[c]([pc]" + (getZ()/8) + "[c])";
        }
        String b = StringUtils.getNameFromEnum(getLocation().getBlock().getBiome());
        Chunk c = base.getLocation().getChunk();
        if(!cfg.isSet("Settings.TimeZone")) cfg.set("Settings.TimeZone", "EST");
        ZoneId zz = ZoneId.of(
                ZoneId.SHORT_IDS.containsKey(cfg.getString("Settings.TimeZone").toUpperCase())
                        ? ZoneId.SHORT_IDS.get(cfg.getString("Settings.TimeZone").toUpperCase())
                        : x());
        ZonedDateTime zdt = ZonedDateTime.now(zz);
        String output = TimeUtil.formatDate(zdt, false);
        String s1 = TimeUtil.formatDateWithoutTime(zdt);
        String s2 = TimeUtil.formatTime(zdt, false);
        if(is) {
            sb.setTeam(10, "§0", "[c]Balance: $[pc]" + getBalance(), "");
            sb.setTeam(9, "§1", "", "");
            sb.setTeam(8, "§2", "[c]Location: ", "[pc]" + b);
            sb.setTeam(7, "§3", "[c] isSlimeChunk: ", "[pc]" + c.isSlimeChunk());
            sb.setTeam(6, "§4", "[c] X: " + x, "");
            sb.setTeam(5, "§5", "[c] Y: " + getY(), "");
            sb.setTeam(4, "§6", "[c] Z: " + z, "");
            sb.setTeam(3, "§7", "", "");
            sb.setTeam(2, "§8", "[c]Time: ", "[pc]" + cfg.getString("Settings.TimeZone").toUpperCase());
            sb.setTeam(1, "§9", "[c] Date: ", "[pc]" + s1);
            sb.setTeam(0, "§c", "[c] Time: ", "[pc]" + s2);
        }else {
            sb.setTeam("§0", "[c]Balance: $[pc]" + getBalance(), "");
            sb.setTeam("§1", "", "");
            sb.setTeam("§2", "[c]Location: ", "[pc]" + b);
            sb.setTeam("§3", "[c] isSlimeChunk: ", "[pc]" + c.isSlimeChunk());
            sb.setTeam("§4", "[c] X: " + x, "");
            sb.setTeam("§5", "[c] Y: " + getY(), "");
            sb.setTeam("§6", "[c] Z: " + z, "");
            sb.setTeam("§7", "", "");
            sb.setTeam("§8", "[c]Time: ", "[pc]" + cfg.getString("Settings.TimeZone").toUpperCase());
            sb.setTeam("§9", "[c] Date: ", "[pc]" + s1);
            sb.setTeam("§c", "[c] Time: ", "[pc]" + s2);

        }
    }

    public String getOriginalName() {
        return originalName;
    }

    public int getX() {
        return getLocation().getBlockX();
    }

    public int getY() {
        return getLocation().getBlockY();
    }

    public int getZ() {
        return getLocation().getBlockZ();
    }


    public void sendPacket(Packet<?> packet) {
        getCraftPlayer().getHandle().playerConnection.sendPacket(packet);
    }

    @Override
    public Home getHome(String name) {
        for(Home home : getHomes()) {
            if(home.getName().equalsIgnoreCase(name)) return home;
        }
        return null;
    }

    @Override
    public void delHome(String name) {
        cfg.set("Homes." + name + ".Location", null);
        cfg.set("Homes." + name, null);
    }

    @Override
    public Home setHome(String name) {
        MiniHome home = new MiniHome(this, name, getLocation());
        return home;
    }

    @Override
    public Home setHome(String name, Location loc) {
        MiniHome home = new MiniHome(this, name, loc);
        return home;
    }

    @Override
    public List<Shop> buyShops() {
        cfg.reload();
        if(cfg.getSection("ChestShop.Buy") == null || cfg.getSection("ChestShop.Buy").getKeys(false).size() == 0) return new ArrayList<>();
        List<Shop> locs = new ArrayList<>();
        for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
            locs.add(new Shop(ShopType.BUY, deSerializeLocation(cfg.getString("ChestShop.Buy."+a))));

        }
        return locs;
    }

    @Override
    public void addBuy(Location loc) {
        int i = cfg.getSection("ChestShop.Buy") == null ? 1 :cfg.getSection("ChestShop.Buy").getKeys(false).size()+1;
        cfg.set("ChestShop.Buy."+ i, serializeLocation(loc));
    }

    @Override
    public void removeBuy(Location loc) {
        for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
            if(check(deSerializeLocation(cfg.getString("ChestShop.Buy."+a)), loc)) {
                cfg.set("ChestShop.Buy."+a, null);
            }
        }
        List<Location> locs = new ArrayList<>();
        for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
            locs.add(deSerializeLocation(a));
        }
        for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
            cfg.set("ChestShop.Buy."+a, null);
        }
        int i = cfg.getSection("ChestShop.Buy").getKeys(false).size()+1;
        for(Location a : locs) {
            cfg.set("ChestShop.Buy."+i, serializeLocation(a));
            i++;
        }
    }
    @Override
    public List<Shop> sellShops() {
        cfg.reload();
        if(cfg.getSection("ChestShop.Sell") == null || cfg.getSection("ChestShop.Sell").getKeys(false).size() == 0) return new ArrayList<>();
        List<Shop> locs = new ArrayList<>();
        for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
            locs.add(new Shop(ShopType.SELL, deSerializeLocation(cfg.getString("ChestShop.Sell."+a))));
        }
        return locs;
    }

    @Override
    public void addSell(Location loc) {
        int i = cfg.getSection("ChestShop.Sell") == null ? 1 :cfg.getSection("ChestShop.Sell").getKeys(false).size()+1;
        cfg.set("ChestShop.Sell."+ i, serializeLocation(loc));
    }

    public Shop getShop(Location loc) {
        if(sellShops() != null && sellShops().size() != 0)
            for(Shop s : sellShops()) {
                if(check(s.getLoc(), loc)) return s;
            }
        if(buyShops() != null && buyShops().size() != 0)
            for(Shop s : buyShops()) {
                if(check(s.getLoc(), loc)) return s;
            }
        return null;
    }

    @Override
    public void removeSell(Location loc) {
        for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
            if(check(deSerializeLocation(cfg.getString("ChestShop.Sell."+a)), loc)) {
                cfg.set("ChestShop.Sell."+a, null);
            }
        }
        List<Location> locs = new ArrayList<>();
        for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
            locs.add(deSerializeLocation(a));
        }
        for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
            cfg.set("ChestShop.Sell."+a, null);
        }
        int i = cfg.getSection("ChestShop.Sell").getKeys(false).size()+1;
        for(Location a : locs) {
            cfg.set("ChestShop.Sell."+i, serializeLocation(a));
            i++;
        }
    }

    @Override
    public String getLang() {
        return cfg.isSet("Settings.Lang") ? cfg.getString("Settings.Lang") : "en";
    }

    @Override
    public void setLang(String lang) {
        cfg.set("Settings.Lang", lang);
    }

    @Override
    public List<Home> getHomes() {
        cfg.reload();
        List<Home> homes = new ArrayList<>();
        if(cfg.get("Homes") != null) {
            for (String a : cfg.getSection("Homes").getKeys(false)) {
                homes.add(new MiniHome(this, a, deSerializeLocation(cfg.getString("Homes." + a + ".Location")), ""));
            }
        }
        return homes;
    }
    public void toggleVeinMine() {
        cfg.set("Settings.VeinMine", !isVeinMine());
    }

    public boolean isVeinMine() {
        return cfg.get("Settings.VeinMine") == null ? true : cfg.getBoolean("Settings.VeinMine");
    }

    public void setInventory() {
        inv = Bukkit.createInventory(null, 9*6, StringUtils.translate("[pc]" + getName() + "[c]'s Profile: "));
        for(int i = 0; i < 9*6; i++) {
            //0/15
            int r = new Random().nextInt(16);
            int a = r;
            if(r == 16 || r == 0) {
                a = 0;
            }
            if(r == 8) {
                a=7;
            }
            ItemStack is = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) a);
            ItemMeta im = is.getItemMeta();
            im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            im.setDisplayName("§a");
            is.setItemMeta(im);
            if(i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8) {
                inv.setItem(i, is);
            }
            if(i == 9 || i == 17 || i == 18 || i == 26 || i == 27 || i == 35 || i == 36 || i == 44) {
                inv.setItem(i, is);
            }
            if(i == 45 || i == 46 || i == 47 || i == 48 || i == 49 || i == 50 || i == 51 || i == 52 || i == 53) {
                inv.setItem(i, is);
            }
        }
        inv.setItem(22, getSkull());
        setPro();
    }

    public void setPro() {
        pro = Bukkit.createInventory(null, 9*6, StringUtils.translate("[pc]" + getName() + ": "));
        for(int i = 0; i < 9*6; i++) {
            //0/15
            int r = new Random().nextInt(16);
            int a = r;
            if(r == 16 || r == 0) {
                a = 0;
            }
            if(r == 8) {
                a=7;
            }
            ItemStack is = new ItemStack(Material.LEGACY_STAINED_GLASS_PANE, 1, (short) a);
            ItemMeta im = is.getItemMeta();
            im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            im.setDisplayName("§a");
            is.setItemMeta(im);
            if(i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8) {
                pro.setItem(i, is);
            }
            if(i == 9 || i == 17 || i == 18 || i == 26 || i == 27 || i == 35 || i == 36 || i == 44) {
                pro.setItem(i, is);
            }
            if(i == 45 || i == 46 || i == 47 || i == 48 || i == 49 || i == 50 || i == 51 || i == 52 || i == 53) {
                pro.setItem(i, is);
            }
        }
        //CHAT STUFF
        ItemStack chat = new ItemStack(Material.PAPER, 1);
        ItemMeta im = chat.getItemMeta();
        String name = (cfg.getBoolean("Settings.Chat") ? "§cDisable§e Chat" : "§aEnable§e Chat");
        im.setDisplayName(name);
        chat.setItemMeta(im);
        pro.setItem(20, chat);
        //SLOW FALL STUFF
        ItemStack slow = new ItemStack(Material.FEATHER, 1);
        im = slow.getItemMeta();
        if(cfg.get("Settings.SlowFall") == null) {
            cfg.set("Settings.SlowFall", false);
        }
        name = (cfg.getBoolean("Settings.SlowFall") ? "§cDisable§e Slow fall" : "§aEnable§e Slow fall");
        im.setDisplayName(name);
        slow.setItemMeta(im);
        pro.setItem(22, slow);
        ItemStack vanish = new ItemStack(Material.ENDER_PEARL, 1);
        im = vanish.getItemMeta();
        if(cfg.get("Settings.vanish") == null) {
            cfg.set("Settings.vanish", false);
        }
        name = (isVanished() ? "§cDisable§e Vanish" : "§aEnable§e Vanish");
        if(isVanished()) {
            vanish.setType(Material.ENDER_EYE);
        }
        im.setDisplayName(name);
        vanish.setItemMeta(im);
        pro.setItem(24, vanish);

        ItemStack hover = new ItemStack(Material.OAK_SIGN, 1);
        im = hover.getItemMeta();
        name = cfg.getBoolean("Settings.HoverText") ? "§cDisable§e Hover Text" : "§aEnable§e Hover Text";
        im.setDisplayName(name);
        hover.setItemMeta(im);
        pro.setItem(29, hover);

        ItemStack veinmine = new ItemStack(Material.DIAMOND_PICKAXE, 1);
        im = veinmine.getItemMeta();
        name = isVeinMine() ? "§cDisable§e VeinMine" : "§aEnable§e VeinMine";
        im.setDisplayName(name);
        veinmine.setItemMeta(im);
        pro.setItem(31, veinmine);

    }

    @Override
    public ItemStack getSkull() {
        ItemStack i = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta im = (SkullMeta) i.getItemMeta();
        im.setOwner(getName());
        im.setDisplayName(StringUtils.translate("[pc]" + getName()));
        i.setItemMeta(im);
        return i;
    }

    @Override
    public void openInventory(Inventory inv) {
        Bukkit.getScheduler().runTaskLater(MiniverseCore.getAPI(), () -> {
            base.openInventory(inv);
        }, 1);
    }

    @Override
    public void setBase(Player base) {
        core.addUser(user);
        this.base = base;
        UserBase u = new UserBase(base);
        core.addUser(u);
    }

    @Override
    public String getCommand(Material tool) {
        return cfg.getString("PowerTool." + tool.name());
    }

    @Override
    public void setCommand(Material key, String value) {
        cfg.set("PowerTool." + key.name(), value);
    }

    @Override
    public void removeTool(Material key) {
        cfg.set("PowerTool." + key.name(), null);
    }

    @Override
    public boolean isSet(Material key) {
        return cfg.isSet("PowerTool." + key.name());
    }

    @Override
    public PlayerInventory getInventory() {
        return base.getInventory();
    }

    @Override
    public void updateInventory() {
        base.updateInventory();
    }

    @Override
    public UserBase getSuper() {
        return this;
    }

    @Override
    public Inventory getProfileInven() {
        return inv;
    }

    public Inventory getProfile() {
        return pro;
    }

    @Override
    public void onInventoryClick(InventoryClickEvent e) {
        if(base.getOpenInventory() != null && base.getOpenInventory().getTitle().equalsIgnoreCase(invName)
           || base.getOpenInventory().getTitle().equalsIgnoreCase(proName)) {
            if(e.isShiftClick()) {
                e.setCancelled(true);
            }
        }
        if(e.getClickedInventory() == null || e.getSlot() <= -1) return;
        if(e.getClickedInventory().getType() == InventoryType.ANVIL || e.getClickedInventory().getType() == InventoryType.CREATIVE) return;
        if(e.getClickedInventory().getType() == InventoryType.CHEST &&
           inv != null && e.getView().getTitle().equalsIgnoreCase(invName) ||
           e.getView().getTitle().equalsIgnoreCase(proName)) {
            e.setCancelled(true);
            if(e.isShiftClick()) {
                e.setCancelled(true);
            }
            ItemStack current = e.getCurrentItem();
            if(!current.hasItemMeta() ||current == null) return;
            ItemMeta im = current.getItemMeta();

//			name = isVeinMine() ? "§cDisable§e VeinMine" : "§aEnable§e VeinMine";
            if(im.getDisplayName().equalsIgnoreCase(getSkull().getItemMeta().getDisplayName())) {
                if(getProfile() == null) {
                    setPro();
                }
                e.getWhoClicked().openInventory(getProfile());
            }else if(im.getDisplayName().equalsIgnoreCase("§cDisable§e VeinMine")) {
                im.setDisplayName("§aEnable§e VeinMine");
                cfg.set("Settings.VeinMine", false);
                current.setItemMeta(im);
                e.setCurrentItem(current);
            }else if(im.getDisplayName().equalsIgnoreCase("§aEnable§e VeinMine")) {
                im.setDisplayName("§cDisable§e VeinMine");
                cfg.set("Settings.VeinMine", true);
                current.setItemMeta(im);
                e.setCurrentItem(current);
            }
            //Chat
            else if(im.getDisplayName().equalsIgnoreCase("§cDisable§e Chat")) {
                im.setDisplayName("§aEnable§e Chat");
                cfg.set("Settings.Chat", false);
                current.setItemMeta(im);
                e.setCurrentItem(current);
            }
            else if(im.getDisplayName().equalsIgnoreCase("§aEnable§e Chat")) {
                im.setDisplayName("§cDisable§e Chat");
                cfg.set("Settings.Chat", true);
                current.setItemMeta(im);
                e.setCurrentItem(current);
                //Chat
                //Hover
            }else if(im.getDisplayName().equalsIgnoreCase("§cDisable§e hover text")) {
                im.setDisplayName("§aEnable§e hover text");
                cfg.set("Settings.HoverText", false);
                current.setItemMeta(im);
                e.setCurrentItem(current);
            }
            else if(im.getDisplayName().equalsIgnoreCase("§aEnable§e hover text")) {
                im.setDisplayName("§cDisable§e hover text");
                cfg.set("Settings.HoverText", true);
                current.setItemMeta(im);
                e.setCurrentItem(current);
                //Hover
                //SlowFall
            }else if(im.getDisplayName().equalsIgnoreCase("§cDisable§e Slow fall")) {
                if(hasPermission("slowfall")) {
                    im.setDisplayName("§aEnable§e Slow fall");
                    cfg.set("Settings.SlowFall", false);
                    current.setItemMeta(im);
                    e.setCurrentItem(current);
                    return;
                }else {
                    String prevName = im.getDisplayName();
                    im.setDisplayName("§cYou don't have permission to do this.");
                    current.setItemMeta(im);
                    e.setCurrentItem(current);

                    (new BukkitRunnable() {
                        @Override
                        public void run() {
                            im.setDisplayName(prevName);
                            current.setItemMeta(im);
                            e.setCurrentItem(current);
                        }
                    }).runTaskLater(MiniverseCore.getAPI(), 60);
                }
            }else if(im.getDisplayName().equalsIgnoreCase("§aEnable§e Slow fall")) {
                if(hasPermission("slowfall")) {
                    im.setDisplayName("§cDisable§e Slow fall");
                    cfg.set("Settings.SlowFall", true);
                    current.setItemMeta(im);
                    e.setCurrentItem(current);
                    return;
                }else {
                    String prevName = im.getDisplayName();
                    im.setDisplayName("§cYou don't have permission to do this.");
                    current.setItemMeta(im);
                    e.setCurrentItem(current);
                    (new BukkitRunnable() {
                        @Override
                        public void run() {
                            im.setDisplayName(prevName);
                            current.setItemMeta(im);
                            e.setCurrentItem(current);
                        }
                    }).runTaskLater(MiniverseCore.getAPI(), 60);
                }
            }else if(im.getDisplayName().equalsIgnoreCase("§aEnable§e Vanish")) {
                if(hasPermission("vanish")) {
                    im.setDisplayName("§cDisable§e Vanish");
                    current.setItemMeta(im);
                    current.setType(Material.ENDER_EYE);
                    e.setCurrentItem(current);
                    setVanish(true);
                    return;
                }else {
                    String prevName = im.getDisplayName();
                    im.setDisplayName("§cYou don't have permission to do this.");
                    current.setItemMeta(im);
                    e.setCurrentItem(current);
                    (new BukkitRunnable() {
                        @Override
                        public void run() {
                            im.setDisplayName(prevName);
                            current.setItemMeta(im);
                            e.setCurrentItem(current);
                        }
                    }).runTaskLater(MiniverseCore.getAPI(), 60);
                }
            }else if(im.getDisplayName().equalsIgnoreCase("§cDisable§e Vanish")) {
                if(hasPermission("vanish")) {
                    im.setDisplayName("§aEnable§e Vanish");
                    current.setItemMeta(im);
                    current.setType(Material.ENDER_PEARL);
                    e.setCurrentItem(current);
                    setVanish(false);
                    return;
                }else {
                    String prevName = im.getDisplayName();
                    im.setDisplayName("§cYou don't have permission to do this.");
                    current.setItemMeta(im);
                    e.setCurrentItem(current);
                    (new BukkitRunnable() {
                        @Override
                        public void run() {
                            im.setDisplayName(prevName);
                            current.setItemMeta(im);
                            e.setCurrentItem(current);
                        }
                    }).runTaskLater(MiniverseCore.getAPI(), 60);
                }
            }
        }
    }

    public boolean isSlowFall() {
        return cfg.getBoolean("Settings.SlowFall");
    }

    @Override
    public void addPermissions(String... permissions) {
        Permission pe = Bukkit.getPluginManager().getPermission("*");
        for(String perm : permissions) {
            if(!base.hasPermission(perm) || !base.isPermissionSet(perm)) {
                Permission a = Bukkit.getPluginManager().getPermission(perm.toLowerCase());
                if(a == null) {
                    a = new Permission(perm);
                    pe.getChildren().put(perm.toLowerCase(), true);
                    a.addParent(pe, true);
                    Bukkit.getPluginManager().addPermission(a);

                }
                attachment.setPermission(new Permission(perm), true);
                attachment.getPermissions().put(perm.toLowerCase(), true);
            }
        }
    }

    @Override
    public void removePermissions(String... permissions) {
        for (String perm : permissions) {
            PermissionAttachment c = attachment;
            Permission p = new Permission(perm);
            if(Bukkit.getPluginManager().getPermission(perm) == null) {
                p.setDefault(PermissionDefault.FALSE);
                Bukkit.getPluginManager().addPermission(p);
                p.setDefault(PermissionDefault.FALSE);
            }//Don't fix what ain't broke
            p.setDefault(PermissionDefault.FALSE);
            c.setPermission(p, true);
            c.getPermissions().put(p.getName().toLowerCase(), true);
            c.setPermission(p, false);
            c.getPermissions().put(p.getName().toLowerCase(), true);
            attachment = c;
        }
    }

    @Override
    public void clearPermissions() {
        base2.clearPermissions();
        base2.getEffectivePermissions().clear();
    }

    @Override
    public List<Group> getGroups() {
        List<Group> groups = new ArrayList<>();
        for(String group : perm.getStringList("Permissions.Users." + getUUID() + ".groups")) {
            groups.add(GroupManager.getGroup(group));
        }
        return groups;
    }

    @Override
    public Group getPrimaryGroup() {
        List<Group> groups = getGroups();
        if(getGroups().size() == 0) {
            return null;
        }
        int highest = 0;
        for(int i = 1; i < getGroups().size(); i++) {
            if(groups.get(i).getRank() < groups.get(highest).getRank()) highest = i;
        }
        return groups.get(highest);
    }

    public String getPrefix() {
        String prefix = "";
        if(getPrimaryGroup() != null && getPrimaryGroup().getPrefix() != "" && getPrimaryGroup().getPrefix() != null) {
            prefix = getPrimaryGroup().getPrefix();
        }else if(perm.getString("Permissions.Users." + this.getUUID() + ".prefix") != ""
                 && perm.getString("Permissions.Users." + this.getUUID() + ".prefix") != null) {
            prefix = perm.getString("Permissions.Users." + this.getUUID() + ".prefix");
        }else {
            prefix = "";
        }
        return ChatColor.translateAlternateColorCodes('&', prefix);
    }

    @Override
    public void setPrefix(String prefix) {
        perm.set("Permissions.Users." + this.getUUID() + ".prefix", prefix);
    }

    @Override
    public boolean withdrawItem(double money) {
        boolean isFull = false;
        money = NumberUtil.getMoney(money+"");
        ItemStack paper = new ItemStack(Material.PAPER, 1);
        ItemUtil util = ItemUtil.setItem(paper);
        util.setDisplayName("&a&lBankNote &e&oRight Click!").setLore("&cOwner: &e&l" + getName(), "&cWorth: &l$&a" + NumberUtil.format(money));
        util.setDouble("money", money).setString("owner", getName());
        paper = util.finish();
        HashMap<Integer, ItemStack> excess = user.getInventory().addItem(paper);
        user.withDrawMoney(money);
        String f = NumberUtil.format(money);
        for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
            getLocation().getWorld().dropItemNaturally(getLocation().add(0.5,0,0.5), me.getValue());
            isFull = true;
        }
        return isFull;
    }

    @Override
    public Config getConfig() {
        return cfg;
    }

    @Override
    public ItemStack getItemInHand() {
        return base.getInventory().getItemInMainHand();
    }

    public CraftPlayer getCraftPlayer() {
        return (CraftPlayer) base;
    }

    public GameProfile getGP() {
        return getCraftPlayer().getProfile();
    }

    public void setProfile(GameProfile pro) {
        try {
            Method getHandle = base.getClass().getMethod("getHandle");
            Object entityPlayer = getHandle.invoke(base);
            Class<?> entityHuman = entityPlayer.getClass().getSuperclass();
            Field bH = entityHuman.getDeclaredField("h");
            PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn();
            PacketPlayOutPlayerInfo ppopi = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, getCraftPlayer().getHandle());
            for(User u : Miniverse.getOnlineUsers()) {
                u.getCraftPlayer().getHandle().playerConnection.sendPacket(ppopi);
            }
            bH.setAccessible(true);
            bH.set(entityPlayer, pro);
            ppopi = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, getCraftPlayer().getHandle());
            for(User u : Miniverse.getOnlineUsers()) {
                u.getCraftPlayer().getHandle().playerConnection.sendPacket(ppopi);
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isPendingTpa(User u) {
        return isPending.get(u);
    }

    @Override
    public void setTpa(User tpa) {
        this.tpa = tpa;
        if(tpa != null) isPending.put(tpa, true); else {
            isPending.remove(tpa);
        }
    }

    @Override
    public User getTpa() {
        return tpa;
    }

    @Override
    public void setTpaHere(User tpaHere) {
        this.tpaHere = tpaHere;
    }

    @Override
    public User getTpaHere() {
        return tpaHere;
    }

    private Class<?> getNMSClass(String nmsClassString) throws ClassNotFoundException {
        String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + ".";
        String name = "net.minecraft.server." + version + nmsClassString;
        Class<?> nmsClass = Class.forName(name);
        return nmsClass;
    }

    @Override
    public UserBase getMiniUser() {
        return this;
    }

    @Override
    public void kick(String msg) {
        String kick = StringUtils.translate(Miniverse.getSettings().getString("Commands.Kick.kickPrefix")) + msg;
        UserKickEvent event = new UserKickEvent(this,  kick);
        Bukkit.getPluginManager().callEvent(event);
        if(!event.isCancelled()) {
            base.kickPlayer(StringUtils.translate(event.getMessage()));
        }
    }
    @Override
    public void kick(CommandSource source, String msg, boolean isBan) {
        if(isBan) {
            UserBanEvent event = new UserBanEvent(source,this, StringUtils.translate(msg));
            Bukkit.getPluginManager().callEvent(event);
            if(event.isCancelled()) {
                source.sendMessage("[c]Event cancelled");
                return;
            }
            base.kickPlayer(StringUtils.translate(event.getMessage()));
            return;
        }
        kick(msg);
    }
    @Override
    public void startAFKTimer() {
        if(task != null) {
            task.cancel();
        }
        i = Miniverse.getSettings().getInt("Afk.Timer");
        task = (new BukkitRunnable() {
            @Override
            public void run() {
                if(cancelAll) {
                    cancel();
                    return;
                }


                if(!base.isOnline()) {
                    return;
                }
                if(isAFK()&&i>0) {
                    i = 0;
                }
                if(i <= 0) {
                    if(!isAFK()) {
                        if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.AnnounceAFK")) {
                            if(Miniverse.getSettings().getBoolean("Afk.AfkMessage.Permission")) {
                                Miniverse.broadCastPerm(getUser(), "afkAnnounce", Miniverse.format(Miniverse.getSettings(), ("Afk.AfkMessage.All"), getName()));
                                sendMessage(Miniverse.getSettings(), "Afk.AfkMessage.Self");
                            } else {
                                Miniverse.broadCastPerm(getUser(), "", Miniverse.format(Miniverse.getSettings(), ("Afk.AfkMessage.All"), getName()));
                                sendMessage(Miniverse.getSettings(), "Afk.AfkMessage.Self");
                            }
                        } else {
                            sendMessage(Miniverse.getSettings(), "Afk.AfkMessage.Self");
                        }
                        toggleAFK();
                    }
                    i--;
                    return;
                }
                i--;
            }
        }).runTaskTimer(MiniverseCore.getAPI(), 0, 20);

    }

    @Override
    public boolean isAFK() {
        return isAFK;
    }

    boolean w = false;

    @Override
    public void toggleAFK() {
        if(w) return;
        w = true;
        (new BukkitRunnable(){
            @Override
            public void run() {
                isAFK = !isAFK;
                if(isAFK) setName("&4&lAFK&e - &c", "");
                else setName("&e", "");
                w = false;
            }
        }).runTaskLater(MiniverseCore.getAPI(), 0);
    }


    void setName(String prefix, String suffix) {
        setSuffix(suffix);
        setPrefixTab(prefix);
        getBase().setPlayerListName(StringUtils.translate(prefix+getName()+suffix));
    }

    void setSuffix(String suffix) {
        Scoreboard sb = Bukkit.getScoreboardManager().getMainScoreboard();
        Team t = null;
        if (sb.getEntryTeam(getName()) != null)
        {
            t =sb.getEntryTeam(getName());
        }else {
            if(sb.getTeam(getName()) != null) {
                t = sb.getTeam(getName());
            } else {
                t = sb.registerNewTeam(getName());
            }
        }
        t.addEntry(getName());
        t.setSuffix(StringUtils.translate(suffix));
        t.setColor(StringUtils.getLastColorForChat(suffix, ChatColor.WHITE));
        t.setDisplayName(StringUtils.getLastColors(suffix));
    }
    void setPrefixTab(String suffix) {
        Scoreboard sb = Bukkit.getScoreboardManager().getMainScoreboard();
        Team t = null;
        if (sb.getEntryTeam(getName()) != null)
        {
            t =sb.getEntryTeam(getName());
        }else {
            if(sb.getTeam(getName()) != null) {
                t = sb.getTeam(getName());
            } else {
                t = sb.registerNewTeam(getName());
            }
        }
        t.addEntry(getName());
        t.setPrefix(StringUtils.translate(suffix));
        t.setColor(StringUtils.getLastColorForChat(suffix, ChatColor.WHITE));
        t.setDisplayName(StringUtils.getLastColors(suffix));
    }

    @Override
    public void sendMessage(BaseComponent... components) {
        base.spigot().sendMessage(components);
    }

    @Override
    public CommandSource getReplyTo() {
        return replyTo;
    }

    @Override
    public void setReplyTo(CommandSource s) {
        replyTo = s;
    }

    @Override
    public void createAccount() {
        cfg.set("Account.balance", 0);
        if(EconomyManager.getBaltop().containsKey(getName())) {
            EconomyManager.getBaltop().remove(getName());
        }
        EconomyManager.getBaltop().put(getName(), getBalanceRaw());
    }

    @Override
    public void deleteAccount() {
        cfg.set("Account.balance", null);
        cfg.set("Account", null);
        if(EconomyManager.getBaltop().containsKey(getName())) {
            EconomyManager.getBaltop().remove(getName());
        }
    }

    @Override
    public boolean hasEnough(double amount) {
        if(Miniverse.getSettings().getBoolean("Commands.Economy.minBalance")) {
            return (getBalanceRaw() +- amount) >= Miniverse.getSettings().getDouble("Commands.Economy.minBal");
        }
        return getBalanceRaw() >= amount;
    }

    @Override
    public boolean hasAccount() {
        return cfg.getSection("Account") != null;
    }

    @Override
    public Double getBalanceRaw() {
        return cfg.get("Account.balance") == null ? 0 : cfg.getDouble("Account.balance");
    }

    @Override
    public String getBalance() {
        return NumberUtil.format(getBalanceRaw()) == null ? "Error" : NumberUtil.format(getBalanceRaw());
    }

    @Override
    public boolean balanceTooBig(double amount) {
        double money = getBalanceRaw() + amount;
        if(!Miniverse.getSettings().getBoolean("Commands.Economy.maxBalance")) return false;
        if(Miniverse.getSettings().getInt("Commands.Economy.maxBal") <= -1) return false;
        if(money >= Miniverse.getSettings().getInt("Commands.Economy.maxBal")) {
            return true;
        }
        return false;
    }

    @Override
    public void withDrawMoney(double money) {
        double d = money;
        setBal(getBalanceRaw()+-d);
    }

    @Override
    public void depositMoney(double money) {
        double d = money;
        setBal(getBalanceRaw()+d);
    }

    @Override
    public void setBal(double money) {
        Double d = NumberUtil.getMoney(money+"");

        cfg.set("Account.balance", d);

        if(EconomyManager.getBaltop().containsKey(getName())) {
            EconomyManager.getBaltop().remove(getName());
        }
        EconomyManager.getBaltop().put(getName(), getBalanceRaw());
        boolean b = isAFK;
        if(b) setName("&4&lAFK&e - &c","");
        else setName("&e", "");
    }

    @Override
    public void setGamemode(GameMode gm) {
        base.setGameMode(gm);
    }

    @Override
    public Player getBase() {
        return base;
    }

    @Override
    public Set<PermissionAttachmentInfo> getEffectivePermissions() {
        return base.getEffectivePermissions();
    }

    public void teleport(Location loc) {
        base.teleport(loc);
    }

    @Override
    public void tp(Location loc) {
        teleport(loc);
    }

    public Location getLocation() {
        return base.getLocation();
    }

    @Override
    public void setHealth(double health) {
        base.setHealth(health);
    }

    @Override
    public double getHealth() {
        return base.getHealth();
    }

    @Override
    public void setMaxHealth(double maxHealth) {
        base.setMaxHealth(maxHealth);
    }

    @Override
    public boolean commandSpy() {
        return cfg.getBoolean("Settings.CommandSpy");
    }

    @Override
    public void toggleCommandSpy() {
        cfg.set("Settings.CommandSpy", !commandSpy());
    }

    @Override
    public void toggleCommandSpy(boolean commandSpy) {
        cfg.set("Settings.CommandSpy", commandSpy);
    }

    @Override
    public GameMode getGameMode() {
        return base.getGameMode();
    }

    @Override
    public double getMaxHealth() {
        return base.getMaxHealth();
    }

    @Override
    public UUID getUUID() {
        return base.getUniqueId();
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    @Override
    public CommandSender getSender() {
        return getBase();
    }

    @Override
    public ConsoleCommandSender getConsole() {
        return Bukkit.getConsoleSender();
    }

    @Override
    public boolean hasPermission(String... permissions) {
        for(String perm : permissions) {
            if(		base.hasPermission("*")
                       || base.hasPermission(perm + ".*")
                       || base.hasPermission("miniverse.command." + perm + ".*")
                       || base.hasPermission("miniverse." + perm + ".*")
                       || base.hasPermission("miniverse." + "*")
                       || base.hasPermission(perm)
                       || base.hasPermission("miniverse." + perm)
                       || base.hasPermission("miniverse.command." + perm)
                       || base.isOp()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void sendMessage(String... messages) {
        for(int i = 0; i < messages.length; i++) {
            String msg = StringUtils.translate(messages[i]);
            new Message(msg).send(this);
            //getBase().sendMessage(ChatColor.translateAlternateColorCodes('&', msg + "\n").trim());
        }
    }


    @Override
    public void sendMessage(Config cfg, String location, Object... objects) {
        getBase().sendMessage(MiniverseCore.getAPI().formatted(cfg, location, objects));
    }

    @Override
    public void sendMessage(String location, Object... objects) {
        getBase().sendMessage(MiniverseCore.getAPI().formatted(Miniverse.getSettings(), location, objects));
    }


    @Override
    public void sendMessage(Invalidations... messages) {
        for(Invalidations inv : messages) {
            sendMessage(Miniverse.getSettings(), inv.getPath());
        }
    }

    @Override
    public String getName() {
        return getBase().getName();
    }

    @Override
    public User getUser() {
        return this;
    }

    @Override
    public void sendFancyMSG(FancyMessage fm) {
        Validate.isTrue(fm != null, "FancyMessage cannot be null");
        fm.send(base);
    }

    @Override
    public void startCooldown(String key, double time) {
        cooldown.put(key, time);
        BukkitTask t = (new BukkitRunnable() {

            @Override
            public void run() {
                if(cancelAll) {
                    cancel();
                    return;
                }
                if(cooldown.get(key) > 0.1) {
                    cooldown.put(key, cooldown.get(key) +- 0.1);
                }else {
                    cooldown.remove(key);
                    cancel();
                }
            }
        }).runTaskTimer(MiniverseCore.getAPI(), 0, 2);
    }

    @Override
    public boolean isCooldown(String key) {
        return cooldown.containsKey(key);
    }

    @Override
    public double getCooldownRemains(String key) {
        return isCooldown(key) ? cooldown.get(key) : 0;
    }


    @Override
    public Location deSerializeLocation(String loc) {
        if(loc != null && loc != "" && loc.contains(split)) {
            String[] l = loc.split(split);
            Location location =
                    new Location(
                            Bukkit.getWorld(l[0]),
                            Double.parseDouble(l[1]),
                            Double.parseDouble(l[2]),
                            Double.parseDouble(l[3]),
                            Float.parseFloat(l[4]),
                            Float.parseFloat(l[5])
                    );
            return location;
        }
        return null;
    }

    @Override
    public String serializeLocation(Location loc) {
        DecimalFormat df = new DecimalFormat(".000");
        String serialized = loc.getWorld().getName()
                            + split + df.format(loc.getX())
                            + split + df.format(loc.getY())
                            + split + df.format(loc.getZ())
                            + split + loc.getYaw()
                            + split + loc.getPitch();
        return serialized;
    }

    boolean check(Location l1, Location l2) {
        if(l1 == null || l2 == null){
            return false;
        }
        return l1.getBlockX() == l2.getBlockX() && l1.getBlockY() == l2.getBlockY() && l1.getBlockZ() == l2.getBlockZ();
    }

    @Override
    public List<String> getFriends() {
        return cfg.getStringList("Friends");
    }

    @Override
    public boolean hasFriend(User u) {
        return getFriends().contains(u.getName());
    }


    @Override
    public void addFriend(User u) {
        getFriends().add(u.getName());
    }


    @Override
    public void addFriend(String u) {
        getFriends().add(u);
    }


    @Override
    public User getFriend(String u) {
        for(String friend : getFriends()) {
            if(friend.equalsIgnoreCase(u)) {
                return Miniverse.getUser(friend);
            }
        }
        return null;
    }


    @Override
    public User getFriend(User u) {
        for(String friend : getFriends()) {
            if(friend.equalsIgnoreCase(u.getName())) {
                return u;
            }
        }
        return null;
    }


    @Override
    public void messageFriend(String user, String... message) {
        setFriendReply(Miniverse.getUser(user));
        for(String msg : message) {
            getFriendReply().sendMessage(StringUtils.translate(msg));
        }
    }


    @Override
    public void messageFriend(User user, String... message) {
        messageFriend(user.getName(), message);
    }


    @Override
    public User getFriendReply() {
        return friend;
    }


    @Override
    public void setFriendReply(User u) {
        friend = u;
    }
    @Override
    public void setVanish(boolean vanish) {
        if(isPlayer()) {
            cfg.set("Settings.vanish", vanish);
            if(vanish) {
                for(Player p : Bukkit.getOnlinePlayers()) {
                    p.hidePlayer(MiniverseCore.getAPI(), getBase());
                }
                base.setPlayerListName("VANISHED -" + getName());
            }else if(!vanish) {
                for(Player p : Bukkit.getOnlinePlayers()) {
                    p.showPlayer(MiniverseCore.getAPI(), getBase());
                }
                base.setPlayerListName(getName());
            }
        }else {
            cfg.set("Settings.vanish", false);
        }
    }

    @Override
    public boolean isVanished() {
        return cfg.getBoolean("Settings.vanish");
    }


    @Override
    public String toString() {
        return "UserBase=(name="+getName()+",bal="+getBalance()+",isVanish="+isVanished()+",isAFK="+isAFK+",isVeinMine"+isVeinMine()+",uuid="+getUUID()+")";
    }
}
