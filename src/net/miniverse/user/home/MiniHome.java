package net.miniverse.user.home;

import net.miniverse.user.OfflineUser;
import net.miniverse.user.OfflineUserBase;
import net.miniverse.user.UserBase;
import org.bukkit.Location;

public class MiniHome implements Home
{
  private Location loc;
  private String name;
  private OfflineUser u;
  private OfflineUserBase mu;
  
  public MiniHome(OfflineUser u, String name, Location loc)
  {
    this.u = u;
    this.name = name;
    this.loc = loc;
    u.getConfig().set("Homes." + name + ".Location", u.serializeLocation(loc));
    if (!u.getBase().isOnline()) {
      mu = ((OfflineUserBase)u);
      mu.getHomes().add(this);
    } else {
      UserBase userBase = (UserBase)net.miniverse.core.Miniverse.getUser(u.getUUID());
      userBase.getHomes().add(this);
    }
  }
  
  public MiniHome(OfflineUser u, String name, Location loc, String e) {
    this.u = u;
    this.name = name;
    this.loc = loc;
  }
  
  public void setHome(Location loc, String name)
  {
    this.name = name;
    this.loc = loc;
    u.getConfig().set("Homes." + name + ".Location", u.serializeLocation(loc));
  }
  
  public Location getLocation() { return loc; }
  

  public String getName()
  {
    return name;
  }
  
  public Home setName(String name)
  {
    u.delHome(name);
    return u.setHome(name, getLocation());
  }
  
  public Home setLocation(Location loc)
  {
    u.delHome(name);
    return u.setHome(name, loc);
  }
}
