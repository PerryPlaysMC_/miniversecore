package net.miniverse.user.home;

import org.bukkit.Location;

public abstract interface Home
{
  public abstract void setHome(Location paramLocation, String paramString);
  
  public abstract Location getLocation();
  
  public abstract String getName();
  
  public abstract Home setName(String paramString);
  
  public abstract Home setLocation(Location paramLocation);
}
