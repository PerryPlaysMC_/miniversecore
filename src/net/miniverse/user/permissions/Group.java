package net.miniverse.user.permissions;

import net.miniverse.util.config.Config;
import net.miniverse.core.Miniverse;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class Group {

	private String name, prefix;
	private List<String> permissions, players;
	private Config cfg = Miniverse.getPermissions();
	private int rank;

	public Group(String name, List<String> permissions) {
		this.name = name;
		this.permissions = permissions;
		players = new ArrayList<>();
		prefix = cfg.getString("Permissions.groups." + getName() + ".prefix");
		GroupManager.addGroup(this);
		for(OfflineUser u : Miniverse.getAllUsers()) {
			if(u != null && u.getGroups() != null && u.getGroups().size() > 0 && u.getGroups().contains(this)) {
				if(!players.contains(u.getName())) {
					players.add(u.getName());
				}
			}
		}
	}

	public Group(String name, List<String> permissions, int rank) {
		this.name = name;
		this.permissions = permissions;
		this.rank = rank;
		players = new ArrayList<>();
		cfg.set("Permissions.Groups." + name + ".rank", rank);
		prefix = cfg.getString("Permissions.Groups." + getName() + ".prefix");
		GroupManager.addGroup(this);
		for(OfflineUser u : Miniverse.getAllUsers()) {
			if(u != null && u.getGroups() != null && u.getGroups().size() > 0 && u.getGroups().contains(this)) {
				if(!players.contains(u.getName())) {
					players.add(u.getName());
				}
			}
		}
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		cfg.set("Permissions.Groups." + name + ".rank", rank);
		this.rank = rank;
	}

	public List<String> getUsers(){
		return players;
	}

	public String getPrefix() {
		return cfg.getString("Permissions.Groups." + getName() + ".prefix");
	}
	public void setPrefix(String prefix) {
		cfg.set("Permissions.Groups." + getName() + ".prefix", prefix);
	}

	public boolean addUser(OfflineUser u) {
		if(players.contains(u.getName()) && u.getGroups().contains(this)) {
			return false;
		}
		players.add(u.getName());
		List<String> groups = cfg.getStringList("Permissions.Users." + u.getUUID() + ".groups");
		if(!groups.contains(getName())) {
			groups.add(getName());
		}else return false;
		cfg.set("Permissions.Users." + u.getUUID().toString() + ".groups", groups);
		u.getGroups();
		if(u.getBase().isOnline()) {
			User u1 = Miniverse.getUser(u.getName());
			u1.getGroups();
		}
		return true;
	}

	public boolean removeUser(OfflineUser u) {
		if(!players.contains(u.getName()) && !u.getGroups().contains(this)) {
			return false;
		}
		players.remove(u.getName());
		List<String> groups = cfg.getStringList("Permissions.Users." + u.getUUID() + ".groups");
		if(groups.contains(getName())) {
			groups.remove(getName());
		}else return false;
		cfg.set("Permissions.Users." + u.getUUID() + ".groups", groups);
		u.getGroups();
		if(u.getBase().isOnline()) {
			User u1 = Miniverse.getUser(u.getName());
			u1.getGroups();
		}
		return true;
	}

	public void updateInheritance() {
		if(cfg.getStringList("Permissions.Groups." + name + ".inherits") != null){
			for(String groupName : cfg.getStringList("Permissions.Groups." + name + ".inherits")) {
				if(GroupManager.getGroup(groupName) != null) {
					Group g = GroupManager.getGroup(groupName);
					for(String perm : g.getPermissions()) {
						for(User u : Miniverse.getOnlineUsers()) {
							if(u != null) {
								if(u.getGroups().contains(this)) {
									u.addPermissions(perm);
								}
							}
						}
					}
				}
			}
		}
	}

	public List<Group> getInherits() {
		List<Group> groups = new ArrayList<>();
		if(cfg.getStringList("Permissions.Groups." + name + ".inherits") != null){
			for(String groupName : cfg.getStringList("Permissions.Groups." + name + ".inherits")) {
				if(GroupManager.getGroup(groupName) != null) {
					Group g = GroupManager.getGroup(groupName);
					if(g != null && !groups.contains(g)) {
						groups.add(g);
					}
				}
			}
		}
		return groups;
	}

	public void addInheritance(Group group) {
		List<String> a = cfg.getStringList("Permissions.Groups." + name + ".inherits");
		if(!a.contains(group.getName())){
			a.add(group.getName());
		}
		cfg.set("Permissions.Groups." + name + ".inherits", a);
		updateInheritance();
	}

	public void addPerm(String perm) {
		List<String> perms = cfg.getStringList("Permissions.Groups." + name + ".permissions");
		if(!perms.contains(perm)) {
			perms.add(perm);
		}
		cfg.set("Permissions.Groups." + name + ".permissions", perms);
		this.permissions = perms;
		for(User u : Miniverse.getOnlineUsers()) {
			if(u != null) {
				if(u.getGroups().contains(this)) {
					for(String p : getPermissions()) {
						u.addPermissions(p);
					}
				}
			}
		}
	}

	public void removePerm(String perm) {
		List<String> perms = cfg.getStringList("Permissions.Groups." + name + ".permissions");
		if(perms.contains(perm)) {
			perms.remove(perm);
		}
		cfg.set("Permissions.Groups." + name + ".permissions", perms);
		for(User u : Miniverse.getOnlineUsers()) {
			if(u != null) {
				if(u.getGroups().contains(this)) {
					for(String p : getPermissions()) {
						u.removePermissions(p);
					}
				}
			}
		}
	}

	public String toString() {
		return "Group=[{Name=" + getName() + "}, {Prefix=" + getPrefix() + "}]";
	}

	public String getName() {
		return name;
	}

	public List<String> getPermissions() {
		return cfg.getStringList("Permissions.Groups." + name + ".permissions");
	}

	public boolean isDefault() {
		if(!cfg.getConfig().isSet("Permissions.Groups."+name+".default")) return false;
		else return cfg.getBoolean("Permissions.Groups."+name+".default");
	}

    public void setDefault(boolean b) {
		cfg.set("Permissions.Groups."+name+".default", b);
    }
}
