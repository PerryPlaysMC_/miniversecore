package net.miniverse.user.permissions;

import net.miniverse.util.config.Config;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.User;
import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GroupManager {

	private static List<Group> groups;

	public static List<Group> getGroups(){
		if(groups == null) {
			groups = new ArrayList<>();
		}
		return groups;
	}

	public static void addGroup(Group... groups) {
		for(Group group : groups) {
			if(!getGroups().contains(group)) {
				getGroups().add(group);
			}
		}
	}

	public static List<Group> getSorted() {
		int highest = 0;
		List<Group> groups = new ArrayList<>();
		List<Group> groups2 = new ArrayList<>();
		groups.addAll(getGroups());
		Iterator<Group> g = groups.iterator();
		while(g.hasNext()) {
			for (int i = 0; i < groups.size(); i++) {
				if(groups.get(i).getRank() < groups.get(highest).getRank()) highest = i;
			}
			groups2.add(g.next());
			g.remove();
		}
		return groups2;
	}

	@SuppressWarnings("all")
	public static void reload() {
		Permission pe = Bukkit.getPluginManager().getPermission("*");
		if(pe == null) {
			Bukkit.getPluginManager().addPermission(new Permission("*"));
			pe = Bukkit.getPluginManager().getPermission("*");
		}
		for(Permission p : Bukkit.getPluginManager().getPermissions()) {
			if(p.getName() != "*") {
				p.addParent(pe, true);
				pe.getChildren().put(p.getName().toLowerCase(), true);
			}
		}
		pe.setDefault(PermissionDefault.FALSE);
		pe.recalculatePermissibles();
		Config perm = MiniverseCore.getAPI().getPermissions() == null ? new Config("permissions.yml") : MiniverseCore.getAPI().getPermissions();
		if(getGroups().size() > 0) {
			getGroups().clear();
		}
		perm.reload();
		if(perm.getSection("Permissions.Groups") != null) {
			for(String group : perm.getSection("Permissions.Groups").getKeys(false)) {
				Group g = new Group(group, perm.getStringList("Permissions.Groups." + group + ".permissions"),
						perm.getInt("Permissions.Groups." + group + ".rank"));
				g.updateInheritance();
				for(User u : Miniverse.getOnlineUsers()) {
					if(u.getGroups().contains(g)) {
						for(String per : g.getPermissions()) {
							u.addPermissions(per);
						}
					}
				}
			}
		}
	}

	public static Group getGroup(String name) {
		for(Group g : getGroups()) {
			if(g.getName().equalsIgnoreCase(name)) {
				return g;
			}
		}
		return null;
	}

	public static Group getGroup(int rank) {
		for(Group g : getGroups()) {
			if(g.getRank() == rank) {
				return g;
			}
		}
		return null;
	}

	public static Group getGroup(User u, String a, int index) {
		for(Group g : getGroups()) {
			if(g.getUsers().contains(u.getUUID().toString())) {
				return u.getGroups().get(index);
			}
		}
		return null;
	}

	public static Group getGroup(User u, int rank) {
		for(Group g : getGroups()) {
			if(g.getRank() == rank && g.getUsers().contains(u.getName())) {
				return g;
			}
		}
		return null;
	}


}
