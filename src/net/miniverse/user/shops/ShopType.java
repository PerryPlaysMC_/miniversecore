package net.miniverse.user.shops;

public enum ShopType
{
  BUY,  SELL;
  
  private ShopType() {}
}
