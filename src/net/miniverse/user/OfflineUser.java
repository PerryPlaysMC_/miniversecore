package net.miniverse.user;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import net.miniverse.util.config.Config;
import net.miniverse.user.permissions.Group;
import net.miniverse.user.home.Home;
import net.miniverse.user.shops.Shop;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.permissions.PermissionAttachmentInfo;

public abstract interface OfflineUser
{
  public abstract Config getConfig();
  
  public abstract OfflinePlayer getBase();
  
  public abstract Set<PermissionAttachmentInfo> getEffectivePermissions();
  
  public abstract Shop getShop(Location paramLocation);
  
  public abstract List<Shop> sellShops();
  
  public abstract void addSell(Location paramLocation);
  
  public abstract void removeSell(Location paramLocation);
  
  public abstract List<Shop> buyShops();
  
  public abstract void addBuy(Location paramLocation);
  
  public abstract void removeBuy(Location paramLocation);
  
  public abstract Home getHome(String paramString);
  
  public abstract void delHome(String paramString);
  
  public abstract List<Home> getHomes();
  
  public abstract Home setHome(String paramString, Location paramLocation);
  
  public abstract UUID getUUID();
  
  public abstract void addPermissions(String... paramVarArgs);
  
  public abstract void removePermissions(String... paramVarArgs);
  
  public abstract void clearPermissions();
  
  public abstract String getPrefix();
  
  public abstract void setPrefix(String paramString);
  
  public abstract String getName();
  
  public abstract void createAccount();
  
  public abstract void deleteAccount();
  
  public abstract Double getBalanceRaw();
  
  public abstract void sendMessage(String... paramVarArgs);
  
  public abstract void sendMessage(Config paramConfig, String paramString, Object... paramVarArgs);
  
  public abstract void sendMessage(String paramString, Object... paramVarArgs);
  
  public abstract boolean hasEnough(double paramDouble);
  
  public abstract boolean hasAccount();
  
  public abstract boolean balanceTooBig(double paramDouble);
  
  public abstract String getBalance();
  
  public abstract void depositMoney(double paramDouble);
  
  public abstract void withDrawMoney(double paramDouble);
  
  public abstract void setBal(double paramDouble);
  
  public abstract Location deSerializeLocation(String paramString);
  
  public abstract String serializeLocation(Location paramLocation);
  
  public abstract List<Group> getGroups();
  
  public abstract Group getPrimaryGroup();
}
