package net.miniverse.user;

import net.md_5.bungee.api.chat.BaseComponent;
import net.miniverse.chat.Invalidations;
import net.miniverse.util.config.Config;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public interface CommandSource {

    Player getBase();

    boolean isPlayer();

    CommandSender getSender();

    ConsoleCommandSender getConsole();

    void sendMessage(String... message);

    void sendMessage(Invalidations... invalidations);

    void sendMessage(Config cfg, String path, Object... objects);

    void sendMessage(String path, Object... objects);

    void setVanish(boolean vanish);

    boolean isVanished();

    String getName();

    User getUser();

    void sendMessage(BaseComponent... components);

    boolean hasPermission(String... permissions);

    CommandSource getReplyTo();

    void setReplyTo(CommandSource source);
}
