package net.miniverse.gui.shops;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import net.miniverse.util.config.Config;
import net.miniverse.core.IMiniverse.LogType;
import net.miniverse.core.MiniverseCore;
import net.miniverse.util.itemutils.MiniItem;
import net.miniverse.util.StringUtils;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MiniShopHandler
{
    private static List<MiniShop> shops;

    public MiniShopHandler() {}

    public static List<MiniShop> getShops() {
        if (shops == null) shops = new ArrayList<>();
        return shops;
    }

    public static void addShop(MiniShop shop)
    {
        if (!getShops().contains(shop)) {
            getShops().add(shop);
        }
    }

    public static MiniShop getShop(String name) {
        for (MiniShop shop : getShops()) {
            if ((shop.getDisplayName().equalsIgnoreCase(name)) || (shop.getName().equalsIgnoreCase(StringUtils.stripColor(name))) ||
                    (StringUtils.stripColor(shop.getDisplayName()).equalsIgnoreCase(StringUtils.stripColor(name)))) return shop;
        }
        return null;
    }

    public static ItemValues getValue(ItemStack i) {
        for (MiniShop shop : getShops()) {
            if (shop.getValue(i) != null) {
                return shop.getValue(i);
            }
        }
        return null;
    }

    public static ItemValues getValue(Material i) {
        for (MiniShop shop : getShops() ) {
            if (shop.getValue(i) != null) {
                return shop.getValue(i);
            }
        }
        return null;
    }

    public static void reload(MiniShop gui) {
        Config cfg = gui.getConfig();
        if (cfg.isSet("Items")) {
            List<Value> itemsToAdd = getList(gui, new ArrayList<>());
            add(gui, itemsToAdd);
        }
    }

    public static void reload() {
        if (shops == null) {
            shops = new ArrayList<>();
        }
        if (shops.size() > 0) {
            shops.clear();
        }
        File[] files = new File("plugins/Miniverse/Inventories").listFiles();

        if (files != null)
            for (File f : files)
                if ((f != null) && (f.getName().endsWith(".yml"))) {
                    MiniShop gui = new MiniShop(f.getName().replace(".yml", ""));
                    Config cfg = gui.getConfig();
                    if (cfg.isSet("Items")) {
                        List<Value> itemsToAdd = getList(gui, new ArrayList<>());
                        add(gui, itemsToAdd);
                    }
                }
        net.miniverse.core.events.ShopInteract.load();
    }

    private static void add(MiniShop gui, List<Value> itemsToAdd) {
        Config cfg = gui.getConfig();
        for (Value e : itemsToAdd) {
            String path = e.getPath();
            Inventory i = gui.getInventory();
            if (gui.getInventory().firstEmpty() == -1) {
                if (!gui.hasNext()) {
                    gui.createNext();
                }
                i = gui.getNext().getInventory();
                gui.getNext().getConfig().set(path + "isSellable", e.isSell())
                        .set(path + "itemColor", e.getColor())
                        .set(path + "isBuyable", e.isBuy())
                        .set(path + "lore", cfg.getStringList(path + "lore"))
                        .set(path + "buyCost", cfg.getDouble(path + "buyCost"))
                        .set(path + "sellCost", cfg.getDouble(path + "sellCost"));

                if(cfg.isSet(path+"slot")) {
                    gui.getNext().getConfig().set(path + "slot", cfg.getInt(path+"slot"));
                }
                gui.getConfig().set("Items." + e.getName(), null);
            }
            if(gui.getConfig().isSet(path + "slot")) {
                i.setItem(cfg.getInt(path + "slot"), e.getStack());
            }else if (!gui.getInventory().contains(e.getStack())) {
                i.addItem(e.getStack());
            }
            for (int a = 0; a < i.getContents().length; a++) {
                ItemStack f = i.getItem(a);
                if ((f != null) && (f.getType() != Material.AIR)) {
                    f.setAmount(1);
                    i.setItem(a, f);
                }
            }
        }
        if ((gui.hasNext()) &&
                (gui.getNext().getConfig().isSet("Items"))) {
            add(gui.getNext(), getList(gui.getNext(), new ArrayList<>()));
        }
    }

    private static List<Value> getList(MiniShop gui, List<Value> itemsToAdd) {
        Config cfg = gui.getConfig();
        for (String i : cfg.getSection("Items").getKeys(false)) {
            ItemStack stack = MiniItem.getItem(i);
            if (stack == null) {
                MiniverseCore.getAPI().log(LogType.WARNING, "MiniShopHandler- Invalid item type, " + i + " exists in bukkit? " + (Material.getMaterial(i) != null ? 1 : false) + " exists in minecraft? " + (
                        Material.matchMaterial(i) != null ? 1 : false));
            }
            else {
                String path = "Items." + i + ".";
                String color = !cfg.isSet(path + "nameColor") ? cfg.getString(path + "itemColor") : cfg.getString(path + "nameColor");
                String format = StringUtils.getNameFromEnum(stack.getType());
                ItemStack item = net.miniverse.util.GuiUtil.createItem(stack.getType(), color + format, 1, cfg.getStringList(path + "lore"));
                ItemMeta im = item.getItemMeta();
                List<String> lore = im.hasLore() ? im.getLore() : new ArrayList<>();
                List<String> l = new ArrayList<>();
                boolean isBuy = true;boolean isSell = true;
                if ((cfg.isSet(path + "isSellable")) &&
                        (!cfg.getBoolean(path + "isSellable"))) {
                    lore.add("§cNot Sellable");
                    for (String a : lore)
                        if ((a.toLowerCase().contains("sell")) && (!a.contains("Sellable")))
                            l.add(a.replace("Sell", "Buy")); else
                            l.add(a);
                    isSell = false;
                }

                if ((cfg.isSet(path + "isBuyable")) &&
                        (!cfg.getBoolean(path + "isBuyable"))) {
                    lore.add("§cNot Buyable");
                    for (String a : lore)
                        if ((a.toLowerCase().contains("buy")) && (!a.contains("Buyable")))
                            l.add(a.replace("Buy", "Sell")); else
                            l.add(a);
                    isBuy = false;
                }

                if (isSell)
                    l.add(StringUtils.translate("[c]Sell price $[pc]" + cfg.getDouble("Items." + i + ".sellCost")));
                if (isBuy)
                    l.add(StringUtils.translate("[c]Buy price $[pc]" + cfg.getDouble("Items." + i + ".buyCost")));
                im.setLore(l);
                stack.setItemMeta(im);

                if ((!isSell) && (!isBuy)) {
                    MiniverseCore.getAPI().log(LogType.WARNING, "Invalid item, " + i + " Is not buy or sellable");
                }
                else
                    itemsToAdd.add(new Value(stack, i, path, color, isBuy, isSell));
            } }
        return itemsToAdd;
    }
}
