package net.miniverse.gui.shops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.miniverse.util.config.Config;
import net.miniverse.core.IMiniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.util.GuiUtil;
import net.miniverse.util.itemutils.MiniItem;
import net.miniverse.util.StringUtils;
import net.miniverse.util.itemutils.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class MiniShop
{
    private Inventory inv;
    private Config cfg;
    private ItemStack display;
    private String displayName;
    private String name;
    private MiniShop next;

    public MiniShop(String name)
    {
        cfg = new Config("plugins/Miniverse/Inventories", name + ".yml");
        if (!cfg.isSet("Settings.Size")) {
            MiniverseCore.getAPI().log(IMiniverse.LogType.ERROR, "Settings.Size must be set in config");
            return;
        }
        if (!cfg.isSet("Settings.Display")) {
            MiniverseCore.getAPI().log(IMiniverse.LogType.ERROR, "Settings.Display must be set in config");
            return;
        }
        if (((!cfg.isSet("Settings.isNext")) || (!cfg.getBoolean("Settings.isNext"))) &&
                (!cfg.isSet("Settings.Item"))) {
            MiniverseCore.getAPI().log(IMiniverse.LogType.ERROR, "Settings.Size must be set in config");
            return;
        }

        if ((cfg.isSet("NextPage.Display")) && (cfg.isSet("NextPage.Name"))) {
            Config cfg2 = new Config("plugins/Miniverse/Inventories", cfg.getString("NextPage.Name") + ".yml");
            if (!cfg2.isSet("Settings.Size")) cfg2.set("Settings.Size", cfg.getInt("Settings.Size"));
            if (!cfg2.isSet("Settings.Display")) cfg2.set("Settings.Display", cfg.getString("NextPage.Display"));
            cfg2.set("Settings.isNext", true);
            cfg2.set("Settings.Previous", getName());
            next = new MiniShop(cfg.getString("NextPage.Name"));
        }
        this.name = name;
        inv = Bukkit.createInventory(null, cfg.getInt("Settings.Size"), StringUtils.translate(cfg.getString("Settings.Display")));
        displayName = StringUtils.translate(cfg.getString("Settings.Display"));
        if (getInventory().getSize() >= 9) {
            Material[] glass = { Material.PURPLE_STAINED_GLASS_PANE, Material.LIME_STAINED_GLASS_PANE };
            for (int i = 1; i < 9; i++) {
                setItem(getInventory().getSize() + -i, ItemUtil.setItem(new ItemStack(glass[new java.util.Random().nextInt(glass.length)])).setDisplayName(" ").finish());
            }
            String end = "previous page";
            setItem(getInventory().getSize() + -9, GuiUtil.createItem(Material.LIME_WOOL, "[c]&lBack", 1, "[c]Return to " + end));
            if (!isNext()) {
                setItem(getInventory().getSize() + -9, GuiUtil.createItem(Material.LIME_WOOL, "[c]&lBack", 1, "[c]Return to Main page"));
            }
        }
        if (hasNext())
            setItem(getInventory().getSize() + -1, GuiUtil.createItem(Material.LIME_WOOL, "[c]&lNext", 1, "[c]Go to the next page"));
        try {
            if (cfg.isSet("Settings.Item")) {
                display = MiniItem.getItem(cfg.getString("Settings.Item"));
                display = ItemUtil.setItem(display).setDisplayName(getDisplayName()).finish();
                if (cfg.isSet("Settings.ItemLore")) {
                    display = ItemUtil.setItem(display).setDisplayName(getDisplayName()).setLore(true,
                            cfg.getStringList("Settings.ItemLore").toArray(new String[0])).finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        MiniShopHandler.addShop(this);
    }

    public boolean hasNext() {
        return next != null;
    }

    void createNext() {
        cfg.set("NextPage.Name", name.replace(".yml", "") + "_2");
        cfg.set("NextPage.Display", getDisplayName() + "-2");
        Config cfg2 = new Config("plugins/Miniverse/Inventories", cfg.getString("NextPage.Name") + ".yml");
        if (!cfg2.isSet("Settings.Size")) cfg2.set("Settings.Size", cfg.getInt("Settings.Size"));
        if (!cfg2.isSet("Settings.Display")) cfg2.set("Settings.Display", cfg.getString("NextPage.Display"));
        cfg2.set("Settings.Previous", getName());
        cfg2.set("Settings.isNext", true);
        next = new MiniShop(cfg.getString("NextPage.Name"));
    }

    public boolean isNext() {
        return cfg.isSet("Settings.isNext") && cfg.getBoolean("Settings.isNext");
    }

    public ConfigurationSection getItemSection() {
        return cfg.getSection("Items");
    }

    public void remove(ItemStack item) {
        for (String a : getItemSection().getKeys(false)) {
            ItemStack stack = MiniItem.getItem(a);
            if (stack == null) {
                MiniverseCore.getAPI().log(IMiniverse.LogType.WARNING, "MiniShop- Invalid item type, " + a + " exists in bukkit? " + (Material.getMaterial(a) != null ? 1 : false) + " exists in minecraft? " + (
                        Material.matchMaterial(a) != null ? 1 : false));

            }
            else if (stack.getType() == item.getType()) {
                cfg.set("Items." + a, null);
                break;
            }
        }
    }

    public String getPath(ItemStack i) {
        String r = "";
        for (String a : getItemSection().getKeys(false)) {
            ItemStack stack = MiniItem.getItem(a);
            if (stack == null) {
                MiniverseCore.getAPI().log(IMiniverse.LogType.WARNING, "MiniShop- Invalid item type, " + a + " exists in bukkit? " + (Material.getMaterial(a) != null ? 1 : false) + " exists in minecraft? " + (
                        Material.matchMaterial(a) != null ? 1 : false));

            }
            else if (stack.getType() == i.getType()) {
                r = "Items." + a;
                break;
            }
        }
        return r;
    }

    public List<ItemValues> getPrices() {
        List<ItemValues> f = new ArrayList<>();
        if (getItemSection() == null) return null;
        for (String a : getItemSection().getKeys(false)) {
            try {
                ItemStack stack = MiniItem.getItem(a);
                if (stack == null) {
                    MiniverseCore.getAPI().log(IMiniverse.LogType.WARNING, "MiniShop- Invalid item type, " + a + " exists in bukkit? " + (Material.getMaterial(a) != null ? 1 : false) + " exists in minecraft? " + (
                            Material.matchMaterial(a) != null ? 1 : false));
                }
                else {
                    double buy = getItemSection().getDouble(a + ".buyCost");
                    double sell = getItemSection().getDouble(a + ".sellCost");
                    f.add(new ItemValues(stack, buy, sell));
                }
            } catch (Exception e) { MiniverseCore.getAPI().log(IMiniverse.LogType.WARNING, "MiniShop- Invalid item type, " + a + " exists in bukkit? " + (Material.getMaterial(a) != null ? 1 : false) + " exists in minecraft? " + (
                    Material.matchMaterial(a) != null ? 1 : false));
            }
        }
        return f;
    }

    public ItemValues getValue(ItemStack i) { if ((getPrices() == null) || (getPrices().size() == 0)) return null;
        for (ItemValues val : getPrices()) {
            if ((val != null) && (val.getStack() != null) && (val.getStack().getType() == i.getType())) {
                return val;
            }
        }
        return null;
    }

    public ItemValues getValue(Material i) { return getValue(new ItemStack(i)); }

    public double getSellCost(ItemStack i)
    {
        for (ItemValues val : getPrices()) {
            if (val.getStack().isSimilar(i)) {
                return val.getSell();
            }
        }
        return -1.0D;
    }

    public double getSellCost(Material i) { return getSellCost(new ItemStack(i)); }

    public double getBuyCost(ItemStack i)
    {
        for (ItemValues val : getPrices()) {
            if (val.getStack().isSimilar(i)) {
                return val.getBuy();
            }
        }
        return -1.0D;
    }

    public double getBuyCost(Material i) { return getBuyCost(new ItemStack(i)); }

    public Inventory getInventory()
    {
        return inv;
    }

    public Config getConfig() {
        return cfg;
    }

    public ItemStack getDisplay() {
        return display;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getName() {
        return name;
    }

    public MiniShop getNext() {
        return next;
    }

    public HashMap<Integer, ItemStack> addItems(ItemStack... items) {
        return inv.addItem(items);
    }

    public ItemStack getItem(int slot)
    {
        return inv.getItem(slot);
    }


    public ItemStack[] getItems()
    {
        return inv.getContents();
    }

    public void setItem(int slot, ItemStack im)
    {
        inv.setItem(slot, im);
    }

    public void setItems(ItemStack... items)
    {
        inv.setContents(items);
    }
}
