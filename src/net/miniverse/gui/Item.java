package net.miniverse.gui;

import java.util.List;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Item
{
  private ItemStack item;
  private ItemMeta im;
  
  public Item() {}
  
  public ItemStack create(String name, Material mat, int amount, String... lore)
  {
    item = new ItemStack(mat, amount);
    im = item.getItemMeta();
    List<String> lores = new java.util.ArrayList();
    for (String l : lore) {
      lores.add(net.miniverse.util.StringUtils.translate(l));
    }
    im.setLore(lores);
    im.setDisplayName(net.miniverse.util.StringUtils.translate(name));
    item.setItemMeta(im);
    return item;
  }
  
  public ItemStack create(String name, Material mat, int amount, List<String> lore) {
    item = new ItemStack(mat, amount);
    im = item.getItemMeta();
    List<String> lores = new java.util.ArrayList();
    for (String l : lore) {
      lores.add(net.miniverse.util.StringUtils.translate(l));
    }
    im.setLore(lores);
    im.setDisplayName(net.miniverse.util.StringUtils.translate(name));
    item.setItemMeta(im);
    return item;
  }
  
  public void create(Gui gui, int slot, String name, Material mat, int amount, String... lore) {
    item = new ItemStack(mat, amount);
    im = item.getItemMeta();
    List<String> lores = new java.util.ArrayList();
    for (String l : lore) {
      lores.add(net.miniverse.util.StringUtils.translate(l));
    }
    im.setLore(lores);
    im.setDisplayName(net.miniverse.util.StringUtils.translate(name));
    item.setItemMeta(im);
    gui.setItem(slot, item);
  }
}
