package net.miniverse.gui;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.miniverse.util.config.Config;
import net.miniverse.core.IMiniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.util.StringUtils;
import net.miniverse.util.itemutils.MiniItem;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MiniGuiManager
{
  private static List<MiniGui> guis;
  
  public MiniGuiManager() {}
  
  public static List<MiniGui> getGuis()
  {
    if (guis == null) {
      guis = new ArrayList();
    }
    return guis;
  }
  
  public static void addGui(MiniGui gui)
  {
    if (!getGuis().contains(gui)) {
      getGuis().add(gui);
    }
  }
  
  public static MiniGui getGui(String name)
  {
    for (MiniGui gui : getGuis()) {
      if ((gui.getName().equalsIgnoreCase(name)) || (gui.getDisplayName().equalsIgnoreCase(name)) || 
        (StringUtils.stripColor(gui.getName()).equalsIgnoreCase(StringUtils.stripColor(name))) || 
        (StringUtils.stripColor(gui.getDisplayName()).equalsIgnoreCase(StringUtils.stripColor(name)))) return gui;
    }
    return null;
  }
  
  public static void reload() {
    if (guis == null) {
      guis = new ArrayList();
    }
    if (guis.size() > 0) {
      guis.clear();
    }
    File dir = new File(MiniverseCore.getAPI().getFolder(), "Inventories");
    if (dir.listFiles() != null) {
      for (File f : dir.listFiles())
        if ((f != null) && (f.getName().endsWith(".yml"))) {
          Config cfg = new Config(dir, f.getName());
          MiniGui g = new MiniGui(cfg.getLocalName(), cfg.getInt("Rows"));
          
          if (cfg.getSection("Items") != null) {
            for (String item : cfg.getSection("Items").getKeys(false)) {
              try
              {
                String color = cfg.getString("Items." + item + ".nameColor") == null ? cfg.getString("Items." + item + ".itemColor") : cfg.getString("Items." + item + ".nameColor");
                org.bukkit.Material mat = MiniItem.getItem(item).getType();
                String format = StringUtils.getNameFromEnum(mat);
                ItemStack stack = net.miniverse.util.GuiUtil.createItem(mat, color + format, 1, cfg
                  .getStringList("Items." + item + ".lore"));
                boolean isBuy = true;boolean isSell = true;
                if ((cfg.isSet("Items." + item + ".isBuyable")) && 
                  (!cfg.getBoolean("Items." + item + ".isBuyable"))) {
                  ItemMeta im = stack.getItemMeta();
                  List<String> lore = im.getLore();
                  lore.add("§cNot Buyable");
                  List<String> l = new ArrayList();
                  for (String a : lore) {
                    if ((a.toLowerCase().contains("buy")) && (!a.contains("Buyable"))) {
                      l.add(a.replace("Buy", "Sell"));
                    } else {
                      l.add(a);
                    }
                  }
                  isBuy = false;
                  im.setLore(l);
                  stack.setItemMeta(im);
                }
                List<String> l;
                if ((cfg.isSet("Items." + item + ".isSellable")) && 
                  (!cfg.getBoolean("Items." + item + ".isSellable"))) {
                  ItemMeta im = stack.getItemMeta();
                  List<String> lore = im.getLore();
                  lore.add("§cNot Sellable");
                  l = new ArrayList();
                  for (String a : lore) {
                    if ((a.toLowerCase().contains("sell")) && (!a.contains("Sellable"))) {
                      l.add(a.replace("Sell", "Buy"));
                    } else {
                      l.add(a);
                    }
                  }
                  isSell = false;
                  im.setLore(l);
                  stack.setItemMeta(im);
                }
                
                if (cfg.get("Items." + item + ".slot") != null) {
                  if (cfg.getInt("Items." + item + ".slot") >= g.getInventory().getSize() + -9) {
                    g.nextPage().getInventory().addItem(new ItemStack[] { stack });
                    continue;
                  }
                  g.getInventory().setItem(cfg.getInt("Items." + item + ".slot"), stack);
                } else {
                  HashMap<Integer, ItemStack> excess = g.getInventory().addItem(new ItemStack[] { stack });
                  boolean con = false;
                  if (!excess.isEmpty())
                    for (Object me : excess.entrySet()) {
                      g.nextPage().getInventory().addItem(new ItemStack[] { (ItemStack)((java.util.Map.Entry)me).getValue() });
                      con = true;
                    }
                  if (con) {
                    continue;
                  }
                  if (g.getInventory().firstEmpty() >= g.getInventory().getSize() + -9) {
                    g.nextPage().getInventory().addItem(new ItemStack[] { stack });
                  }
                }
              } catch (Exception e) {
                MiniverseCore.getAPI().log(IMiniverse.LogType.WARNING, "Invalid item type, " + item);
              }
            }
          }
          MiniverseCore.getAPI().debug("Adding GUI: " + g.getDisplayName() + "/" + g.getName());
        }
    }
  }
}
