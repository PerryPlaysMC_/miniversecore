package net.miniverse.gui;

import java.io.File;
import java.util.HashMap;
import net.miniverse.util.config.Config;
import net.miniverse.core.MiniverseCore;
import net.miniverse.util.GuiUtil;
import net.miniverse.util.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class MiniGui
  implements Gui
{
  private Inventory inv;
  private Config cfg;
  private String name;
  private MiniGui next;
  
  public MiniGui(String name, int size)
  {
    this.name = name;
    cfg = new Config(new File(MiniverseCore.getAPI().getFolder(), "Inventories"), name + ".yml");
    if ((cfg.get("Next.name") != null) && (cfg.get("Next.rows") != null) && (cfg.get("Next.rawName") != null)) {
      Config a = new Config(new File(MiniverseCore.getAPI().getFolder(), "Inventories"), cfg.getString("Next.rawName") + ".yml");
      a.set("DisplayName", cfg.getString("Next.name"));
      a.set("isNext", Boolean.valueOf(true));
      a.set("Rows", Integer.valueOf(cfg.getInt("Next.rows")));
      

      next = new MiniGui(cfg.getString("Next.rawName"), cfg.getInt("Next.rows"));
    }
    inv = Bukkit.createInventory(null, size * 9, StringUtils.translate(getDisplayName())); if (getInventory().getSize() >= 9) {
      Material[] glass = { Material.PURPLE_STAINED_GLASS_PANE, Material.LIME_STAINED_GLASS_PANE };
      for (int i = 1; i < 9; i++) {
        setItem(getInventory().getSize() + -i, GuiUtil.createItem(glass[new java.util.Random().nextInt(glass.length)], "", 1));
      }
      setItem(getInventory().getSize() + -9, GuiUtil.createItem(Material.LIME_WOOL, "[c]&lBack", 1, 5, new String[] { "[c]Return to previous page" }));
    }
    if (nextPage() != null)
      setItem(getInventory().getSize() + -1, GuiUtil.createItem(Material.LIME_WOOL, "[c]&lNext", 1, 5, new String[] { "[c]Go to the next page" }));
    MiniGuiManager.addGui(this);
  }
  
  public boolean isNext() {
    return cfg.getBoolean("isNext");
  }
  
  public String getName() {
    return StringUtils.translate(name);
  }
  
  public String getDisplayName() {
    return StringUtils.translate(cfg.getString("DisplayName"));
  }
  
  public MiniGui nextPage() {
    return next;
  }
  
  public Config getConfig() {
    return cfg;
  }
  
  public Inventory getInventory()
  {
    return inv;
  }
  
  public HashMap<Integer, ItemStack> addItems(ItemStack... items)
  {
    return inv.addItem(items);
  }
  
  public ItemStack getItem(int slot)
  {
    return inv.getItem(slot);
  }
  

  public ItemStack[] getItems()
  {
    return inv.getContents();
  }
  
  public void setItem(int slot, ItemStack im)
  {
    inv.setItem(slot, im);
  }
  
  public void setItems(ItemStack... items)
  {
    inv.setContents(items);
  }
}
