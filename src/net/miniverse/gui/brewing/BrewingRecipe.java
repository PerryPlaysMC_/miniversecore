package net.miniverse.gui.brewing;

import net.miniverse.core.MiniverseCore;
import org.bukkit.Material;
import org.bukkit.block.BrewingStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitRunnable;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class BrewingRecipe {
    private static List<BrewingRecipe> recipes = new ArrayList<BrewingRecipe>();
    private ItemStack ingredient;
    private BrewAction action;
    private boolean perfect, awkwardTransform;
    private BrewClock clock = null;

    public BrewingRecipe(ItemStack ingredient, BrewAction action, boolean perfect) {
        this.ingredient = ingredient;
        this.action = action;
        this.perfect = perfect;
        recipes.add(this);
    }

    public BrewingRecipe(Material ingredient, BrewAction action) {
        this(new ItemStack(ingredient), action, false);
    }
    public BrewingRecipe(boolean awkwardTransform, Material ingredient, BrewAction action) {
        this(new ItemStack(ingredient), action, false);
        this.awkwardTransform = awkwardTransform;
    }
    public ItemStack getIngredient() {
        return ingredient;
    }

    public BrewAction getAction() {
        return action;
    }

    public boolean isPerfect() {
        return perfect;
    }

    private static boolean hasAwkward(BrewerInventory inventory) {
        boolean hasAwkward = false;
        for(int i = 0; i < 3 ; i ++) {
            if(inventory.getItem(i) == null || inventory.getItem(i).getType() == Material.AIR)
                continue;
            ItemStack item = inventory.getItem(i);
            ItemStack pot = new Potion(PotionType.AWKWARD).toItemStack(1);
            if(pot == null || !item.isSimilar(pot)) continue;
            hasAwkward = true;
        }
        return hasAwkward;
    }
    public boolean isAwkwardTransform() {return awkwardTransform;}
    /**
     * Get the BrewRecipe of the given recipe, will return null if no recipe is found
     * @param inventory The inventory
     * @return The recipe
     */
    @Nullable
    public static BrewingRecipe getRecipe(BrewerInventory inventory) {
        for(BrewingRecipe recipe : recipes) {
            if(inventory.getIngredient() == null)break;
            if(!recipe.isPerfect() && inventory.getIngredient().getType() == recipe.getIngredient().getType()) {
                return recipe;
            }
            if(recipe.isPerfect() && inventory.getIngredient().isSimilar(recipe.getIngredient())) {
                return recipe;
            }
        }
        return null;
    }

    public void startBrewing(BrewerInventory inventory) {
        clock = new BrewClock(this, inventory);
        clock.runTaskTimer(MiniverseCore.getAPI(), 0L, 1L);
    }

    public boolean isRunning() {
        return clock != null;
    }
    public void stopBrew() {
        clock.cancel();
        clock.stand.setBrewingTime(400);
        clock.stand.update(true);
        clock.stand.update();
        clock = null;
    }

    public BrewClock getClock() {
        return clock;
    }

    public class BrewClock extends BukkitRunnable {
        private BrewerInventory inventory;
        private BrewingRecipe recipe;
        private ItemStack ingredient;
        private BrewingStand stand;
        private int time = 400; //Like I said the starting time is 400
        private boolean paused = false;

        public BrewClock(BrewingRecipe recipe, BrewerInventory inventory) {
            this.recipe = recipe;
            this.inventory = inventory;
            this.ingredient = inventory.getIngredient();
            this.stand = inventory.getHolder();
        }

        public void setPaused(boolean x) {
            this.paused = x;
        }

        public BrewingStand getStand() {return stand;}

        @Override
        public void run() {
            boolean hasAwkward = BrewingRecipe.hasAwkward(inventory);
            if(!hasAwkward && recipe.isAwkwardTransform()) {
                time = 400;
                stand.setBrewingTime(410); //Reseting everything
                if(!paused)
                    stand.update();
                return;
            }
            if(time <= 0) {
                inventory.setIngredient(new ItemStack(Material.AIR));
                inventory.setIngredient(null);
                inventory.setItem(3, null);
                stand.getInventory().setIngredient(null);
                stand.getSnapshotInventory().setIngredient(null);
                inventory.getViewers().forEach(x -> ((Player)x).updateInventory());
                if(!paused)
                    stand.update();
                for(int i = 0; i < 3 ; i ++) {
                    if(inventory.getItem(i) == null || inventory.getItem(i).getType() == Material.AIR)
                        continue;
                    recipe.getAction().brew(inventory, inventory.getItem(i), ingredient);
                }
                cancel();
                return;
            }
            if(inventory.getIngredient()!=null&&!inventory.getIngredient().isSimilar(ingredient)) {
                stand.setBrewingTime(400); //Reseting everything
                if(!paused)
                    stand.update();
                cancel();
                return;
            }
            //You should also add here a check to make sure that there are still items to brew
            if(ingredient == null) {
                stand.setBrewingTime(400);
                (new BukkitRunnable(){
                    @Override
                    public void run() {
                        inventory.setIngredient(new ItemStack(Material.AIR));
                        inventory.setIngredient(null);
                        inventory.setItem(3, null);
                        stand.getInventory().setIngredient(null);
                        stand.getSnapshotInventory().setIngredient(null);
                        if(!paused)
                            stand.update();
                    }
                }).runTaskLater(MiniverseCore.getAPI(), 5);
                cancel();
                return;
            }
            time--;
            stand.setBrewingTime(time);
            if(!paused)
                stand.update();
        }
    }
}