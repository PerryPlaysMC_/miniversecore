package net.miniverse.gui.brewing;

import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.ItemStack;

public abstract interface BrewAction
{
  public abstract void brew(BrewerInventory paramBrewerInventory, ItemStack paramItemStack1, ItemStack paramItemStack2);
}
