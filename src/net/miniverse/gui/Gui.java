package net.miniverse.gui;

import java.util.HashMap;
import net.miniverse.util.config.Config;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract interface Gui
{
  public abstract Gui nextPage();
  
  public abstract boolean isNext();
  
  public abstract String getName();
  
  public abstract String getDisplayName();
  
  public abstract Config getConfig();
  
  public abstract Inventory getInventory();
  
  public abstract HashMap<Integer, ItemStack> addItems(ItemStack... paramVarArgs);
  
  public abstract ItemStack getItem(int paramInt);
  
  public abstract ItemStack[] getItems();
  
  public abstract void setItems(ItemStack... paramVarArgs);
  
  public abstract void setItem(int paramInt, ItemStack paramItemStack);
}
