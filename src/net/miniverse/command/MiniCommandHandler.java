package net.miniverse.command;

import com.google.common.collect.ImmutableList;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.TopicHandler;
import net.miniverse.user.CommandSource;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import java.util.ArrayList;
import java.util.List;


public class  MiniCommandHandler extends Command {

	private static  List<MiniCommand> commands;
	private static MiniCommandHandler inst;
	private String name;
	private static SimpleCommandMap cmdMap = MiniverseCore.getAPI().getCommandMap();
    private static boolean isDoubleSlash = Miniverse.getSettings().getBoolean("settings.isDoubleSlash");
    private static String dub = isDoubleSlash ? "/" : "";

	public MiniCommandHandler(String name) {
		super(name);
		this.name = dub + name;
		this.setName(dub+name);
		inst = this;
		if(commands == null) {
			commands = new ArrayList<>();
		}
	}

	@Override
	public boolean setName(String name) {
		this.name = name;
		return true;
	}

	@Override
	public String getName() {
		return name;
	}


	public static List<MiniCommand> getCommands(){
		if(commands == null) {
			commands = new ArrayList<>();
		}
		return commands;
	}

	public static void addCommand(MiniCommand cmd) {
		if(commands == null) {
			commands = new ArrayList<>();
		}
		if(!commands.contains(cmd)) {
			cmd.setHandle(inst);
			boolean isDoubleSlash = Miniverse.getSettings().getBoolean("settings.isDoubleSlash");
			String dub = isDoubleSlash ? "/" : "";
			cmd.name = dub + cmd.name;
			cmd.usage = cmd.usage.replace("<command>", dub + cmd.name);
			if(cmd.permission!=""){
				inst.setPermission(cmd.getPermission());
				cmd.getHandle().setPermission(cmd.getPermission());
			}
			commands.add(cmd);
		}
	}

	public static MiniCommand getCommand(String command) {
		for(MiniCommand cmd : getCommands()) {
			if(command.equalsIgnoreCase(cmd.getName()) || command.equalsIgnoreCase("miniverse:"+cmd.getName())) {
				return cmd;
			}
		}
		return null;
	}

	public static MiniCommand getCMD(String command) {
		MiniCommand cmd = getCommand(command);
		if(cmd == null) {
			cmd = getCommandFromAlias(command);
		}
		if(cmd == null&&isDoubleSlash) {
		    cmd = getCommand("/" + command);
            if(cmd == null) {
                cmd = getCommandFromAlias("/" + command);
            }
        }
		return cmd;
	}

	public static MiniCommand getCommandFromAlias(String alias) {
		for(MiniCommand cmd : getCommands()) {
			for(String a : cmd.getAliases()) {
				if(alias.equalsIgnoreCase(a) || alias.equalsIgnoreCase("miniverse:"+a)) {
					return cmd;
				}
			}
		}
		return null;
	}

	@SuppressWarnings("all")
	public static void reloadCommands() {
		List<MiniCommand> cmdsz = getCommands();
		getCommands().clear();
		SimpleCommandMap cMap = MiniverseCore.getAPI().getCommandMap();

		for(MiniCommand c : cmdsz) {
			if(getCommandFromAlias(c.getName()) == null) {
                c.handle.unregister(cmdMap);
				if(isDoubleSlash && !c.getName().startsWith("/")) {
					c.handle.setName(dub + c.getName());
				}else if(c.getName().startsWith("/") && dub.isEmpty()){
					c.handle.setName(c.getName().replaceFirst("/", ""));
				}
				List<String> aliases = new ArrayList<>();
				for(String a : c.getAliases()) {
					if(isDoubleSlash && !a.startsWith("/")) {
						String b = dub + a;
						aliases.add(b);
					}else if(a.startsWith("/") && dub.isEmpty()){
						String b = a.replace("/", "");
						aliases.add(b);
					}
				}
				c.handle.setAliases(aliases);
				MiniCommandHandler cm = new MiniCommandHandler(c.name);
				cm.setAliases(c.getAliases());
                cmdMap.register(cm.getName(), cm);
				continue;
			}
		}
		for(Player p : Bukkit.getOnlinePlayers()) p.updateCommands();
		TopicHandler.reloadTopics();
	}

	@Override
	public List<String> tabComplete(CommandSender sender, String cl, String[] args){
		MiniCommand cmd = getCMD(cl);
		if(cmd != null) {
			CommandSource s = Miniverse.getSource(sender.getName());
			Label c = new Label(cl);
			cmd.s = s;
			cmd.args = args;
			cmd.cl = c;
			return cmd.onTab(s, c, args);
		}
		return ImmutableList.of();
	}

	@Override
	public boolean execute(CommandSender se, String cl, String[] args) {
		MiniCommand cmd = getCMD(cl);
		if(cmd != null) {
			this.description = cmd.getDescription();
			this.usageMessage = cmd.getUsage();
			this.setDescription(cmd.getDescription());
			this.setUsage(cmd.getUsage());
			CommandSource s = Miniverse.getSource(se.getName());
			Label c = new Label(cl);
			cmd.s = s;
			cmd.args = args;
			cmd.cl = c;
			cmd.run(s, c, args);
		}
		return true;
	}

}
