package net.miniverse.command;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.user.CommandSource;
import net.miniverse.user.OfflineUser;
import net.miniverse.user.User;
import net.miniverse.util.StringUtils;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public abstract class MiniCommand {

    protected boolean isOnlineTab, isTab;
    private List<String> aliases;
    protected CommandSource s;
    protected Label cl;
    protected String[] args;
    protected MiniCommandHandler handle;
    protected String name, desc = "NO DESCRIPTION SET", usage = "NO USAGE SET", permission = "";
    protected MiniverseCore core;

    public MiniCommand(String name, String... aliases) {
        this(name, null, aliases);
    }

    public MiniCommand(boolean isOnlineTab, String name, String... aliases) {
        this(isOnlineTab, name, null, aliases);
    }
    public MiniCommand(String name, boolean isTab, String... aliases) {
        this(name, null, aliases);
        this.isTab = isTab;
    }

    public MiniCommand(String name, Perm permission, String... aliases) {
        this(true, name, permission, aliases);
    }
    public MiniCommand(boolean isOnlineTab, String name, Perm permission, String... aliases) {
        this.isOnlineTab = isOnlineTab;
        this.core = MiniverseCore.getAPI();
        this.name = name;
        this.aliases = new ArrayList<>();
        if(aliases != null && aliases.length > 0) {
            for (String alias : aliases) {
                this.aliases.add(alias);
            }
        }
        usage = "/<command>";
        if(permission != null)
            this.permission = permission.getPermission();
        this.isTab = true;
    }

    public MiniCommand(String name) {
        this(name, null);
    }

    public MiniCommandHandler getHandle(){
        return handle;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }

    public String getDescription() {
        return StringUtils.translate(desc);
    }

    public void setDescription(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public List<String> getAliases(){
        return aliases;
    }

    protected boolean sendInvalidPermission(String... permissions) {
        String perm = "No permission set";
        if(permissions.length > 0) {
            perm = permissions[0];
            if(!perm.startsWith("miniverse.command.")) {
                perm = "miniverse.command." + perm;
            }
            List<String> permss = Miniverse.getPermLoader().isSet("perms") ? Miniverse.getPermLoader().getStringList("perms") : new ArrayList<>();
            if(!permss.contains(perm))
                permss.add(perm);
            Miniverse.getPermLoader().set("perms", permss);
        }
        for(String ss : permissions) {
            if(!s.hasPermission(ss)) {
                perm = ss;
                if(!perm.startsWith("miniverse.command.") && !perm.startsWith("miniverse.")) {
                    perm = "miniverse.command." + perm;
                }
                break;
            }
        }
        if(!s.hasPermission(permissions)) {
            if (args.length >= 1) {
                s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.invalid-permission", cl.getName() + " " + formMessage(0, args.length), perm);
            } else {
                s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.invalid-permission", cl.getName(), perm);
            }
            return true;
        }
        return false;
    }
    protected String formMessage(int start, int stop) {
        StringBuilder sb = new StringBuilder();
        for(int i = start; i < stop; i++) {
            sb.append(args[i] + " ");
        }
        return sb.toString().trim();
    }

    protected String formFullMessage(String[] args, int start, int stop) {
        StringBuilder sb = new StringBuilder();
        for(int i = start; i < stop; i++) {
            sb.append(args[i] + " ");
        }
        return sb.toString();
    }

    protected void sendMessage(String location, Object... objects) {
        s.sendMessage(Miniverse.getSettings(), location, objects);
    }

    protected void sendCMessage(String location, Object... objects) {
        s.sendMessage(Miniverse.getSettings(), "Commands."+location, objects);
    }

    protected void sendCMessage(User t, String location, Object... objects) {
        t.sendMessage(Miniverse.getSettings(), "Commands."+location, objects);
    }

    protected boolean validTarget(String name) {
        OfflineUser t = Miniverse.getOfflineUser(name);
        if(t == null) {
            sendCMessage("Invalidations.invalid-player", name);
            return true;
        }
        return false;
    }

    protected boolean validTarget(String name, boolean online) {
        if(!online) {
            OfflineUser t = Miniverse.getOfflineUser(name);
            if(t == null) {
                sendCMessage("Invalidations.invalid-player", name);
                return true;
            }
            return false;
        }else {
            User t = Miniverse.getUser(name);
            if(t == null) {
                sendCMessage("Invalidations.invalid-player", name);
                return true;
            }
            return false;
        }
    }

    protected boolean validate(String name) {
        OfflineUser t = Miniverse.getOfflineUser(name);
        if(t == null) {
            return true;
        }
        return false;
    }

    protected boolean mustBePlayer() {
        if(!s.isPlayer()) {
            sendMustBePlayer();
            return true;
        }
        return false;
    }

    protected boolean check(String path) {
        return Miniverse.getSettings().getBoolean("Commands." + path);
    }


    protected void sendSpecifyTarget() {
        s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.specify-player");
    }

    protected void sendMustBePlayer() {
        s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.must-be-Player");
    }

    protected void sendInvalidTarget(String args) {
        s.sendMessage(MiniverseCore.getAPI().getSettings(), "Commands.Invalidations.invalid-player", args);
    }

    protected void sendMoreArguments() {
        s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.more-arguments");
    }

    protected void sendTooManyArguments() {
        s.sendMessage(Miniverse.getSettings(), "Commands.Invalidations.too-many-args");
    }

    public abstract void run(CommandSource s, Label cl, String[] args);

    public String getUsage() {
        return StringUtils.translate(usage.replace("<command>", getName()));
    }

    protected BukkitTask runLater(Runnable run, int i) {
        return (new BukkitRunnable() {
            @Override
            public void run() {
                run.run();
            }}).runTaskLater(MiniverseCore.getAPI(),i);
    }

    protected BukkitTask runRepeat(Runnable run, int i, int j) {
        return (new BukkitRunnable() {
            @Override
            public void run() {
                run.run();
            }}).runTaskTimer(MiniverseCore.getAPI(), i, j);
    }

    public List<String> onTab(CommandSource s, Label cl, String[] args) {
        if (args.length == 0) {
            return ImmutableList.of();
        }

        List<String> users = new ArrayList<>();
        List<String> f = Lists.newArrayList();
        if(isTab) {
            for (OfflineUser u : core.getAllUsers()) {
                if(!u.getBase().isOnline() && isOnlineTab) continue;
                if(u != null && !users.contains(u.getName())) {
                    users.add(u.getName());
                }
            }

            String lastWord = args[(args.length - 1)];
            for (String a : users) {
                if(a.toLowerCase().startsWith(lastWord.toLowerCase())) f.add(a);
            }
            return f;
        }
        return ImmutableList.of();
    }

    public void setHandle(MiniCommandHandler inst) {
        this.handle = inst;
    }
}
