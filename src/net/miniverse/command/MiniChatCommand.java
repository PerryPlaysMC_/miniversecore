package net.miniverse.command;

import net.miniverse.user.User;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.ArrayList;
import java.util.List;

public abstract class MiniChatCommand {

	private String name;
	private List<String> aliases;

	public MiniChatCommand(String name, String... aliases) {
		this.name = name.toLowerCase();
		if(aliases.length > 0) {
			if(this.aliases == null) {
				this.aliases = new ArrayList<>();
			}
			for(String a : aliases) {
				this.aliases.add(a.toLowerCase());
			}
		}
	}

	public List<String> getAliases(){
		return aliases;
	}

	public String getName() {
		return name;
	}

	public abstract void execute(User u, AsyncPlayerChatEvent e, String cl, String[] args);
	
	public List<String> onTab(User u, String cl, String[] args) {
		return null;
	}


}
