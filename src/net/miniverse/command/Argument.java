package net.miniverse.command;

import net.miniverse.user.CommandSource;

public class Argument {

    CommandSource s;
    MiniCommand cmd;
    String arg;
    Label cl;
    String[] args;

    public Argument(MiniCommand cmd, Label cl, String[] args, String arg, CommandSource s) {
        this.s = s;
        this.cmd = cmd;
        this.arg = arg;
        this.cl = cl;
        this.args = args;
    }

    public String getArg() {
        return arg;
    }

    public boolean equalsAndPerm(String equals, String... perm) {
        if(s.hasPermission(perm) && arg.equalsIgnoreCase(equals)) {
            return true;
        }else{
            cmd.sendInvalidPermission(perm[0]);
            return false;
        }
    }

    public boolean equals(String... args) {
        for(String a : args) {
            if(arg.equalsIgnoreCase(a)) return true;
        }
        return false;
    }
    public boolean equals(String arg) {
        return getArg().equalsIgnoreCase(arg);
    }


    public CommandSource getSender() {
        return s;
    }

    public MiniCommand getCommand() {
        return cmd;
    }
}
