package net.miniverse.command;

public class Label {


    protected String name;

    public Label(String name) {
        this.name = name;
    }

    public boolean isIgnoreCase(String... name) {
        for(String x : name)
            if(getName().equalsIgnoreCase(x)) return true;
        return false;
    }

    boolean startsWith(String name) {
        return getName().toLowerCase().startsWith(name.toLowerCase());
    }

    public String getName() {
        return name;
    }

}
