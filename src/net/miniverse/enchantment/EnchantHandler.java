package net.miniverse.enchantment;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.miniverse.util.StringUtils;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class EnchantHandler
{
    private static List<CustomEnchant> enchants;


    public static List<CustomEnchant> getEnchantments()
    {
        if (enchants == null) {
            enchants = new ArrayList<>();
        }
        return enchants;
    }

    public static CustomEnchant getEnchantment(String name) {
        for (CustomEnchant ench : getEnchantments()) {
            if (ench.getName().equalsIgnoreCase(name) || StringUtils.stripColor(ench.getName()).equalsIgnoreCase(StringUtils.stripColor(name))) {
                return ench;
            }
        }
        return null;
    }

    public static boolean hasCustomEnchant(ItemStack item) {
        boolean ret = false;
        for (CustomEnchant ench : getEnchantments()) {
            if (item.containsEnchantment(ench)) {
                ret = true;
            }
        }
        return ret;
    }

    private static void loadEnchant(CustomEnchant ench) {
        try {
            try {
                Field f = Enchantment.class.getDeclaredField("acceptingNew");
                f.setAccessible(true);
                f.set(null, true);
                f.setAccessible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if(Enchantment.getByName(ench.getName()) == null || Enchantment.getByKey(ench.getKey()) == null)
                    Enchantment.registerEnchantment(ench);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }

            try {
                Field f = Enchantment.class.getDeclaredField("acceptingNew");
                f.setAccessible(true);
                f.set(null, false);
                f.setAccessible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addEnchantments(CustomEnchant... enchantments) {
        for (CustomEnchant e : enchantments) {
            if (!getEnchantments().contains(e)) {
                getEnchantments().add(e);
                loadEnchant(e);
            }
        }
    }

    public static void resetEnchantments() {
        for (CustomEnchant ench : getEnchantments()) {
            try {
                Field byKeyField = Enchantment.class.getDeclaredField("byKey");
                Field byNameField = Enchantment.class.getDeclaredField("byName");

                byKeyField.setAccessible(true);
                byNameField.setAccessible(true);

                HashMap<Integer, Enchantment> byKey = (HashMap)byKeyField.get(null);
                HashMap<Integer, Enchantment> byName = (HashMap)byNameField.get(null);

                if (byKey.containsKey(ench.getKey())) {
                    byKey.remove(ench.getKey());
                }

                if (byName.containsKey(ench.getName())) {
                    byName.remove(ench.getName());
                }
            }
            catch (Exception localException) {}
        }
    }
}
