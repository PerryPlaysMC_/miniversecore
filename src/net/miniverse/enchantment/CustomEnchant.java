package net.miniverse.enchantment;

import net.minecraft.server.v1_14_R1.MinecraftKey;
import net.miniverse.core.MiniverseCore;
import net.miniverse.util.StringUtils;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomEnchant extends Enchantment {

    private String name;
    private int max, min;
    private Levels levels;
    private EnchantTarget target;
    private boolean isTreasure;
    private boolean isCursed;
    private double chance;
    private List<Enchantment> conflicts;

    public CustomEnchant(String name, Levels levels, int minLevel, int maxLevel, double chance) {
        this(name, levels, minLevel, maxLevel, chance, EnchantTarget.ALL);
    }

    public CustomEnchant(String name, Levels levels, int minLevel, int maxLevel, double chance, Enchantment... conflicts) {
        this(name, levels, minLevel, maxLevel, chance, EnchantTarget.ALL, conflicts);
    }


    public CustomEnchant(String name, Levels levels, int minLevel, int maxLevel, double chance, EnchantTarget target) { this(name, levels, minLevel, maxLevel, chance, target, false); }

    public CustomEnchant(String name, Levels levels, int minLevel, int maxLevel, double chance, EnchantTarget target, Enchantment... conflicts)
    { this(name, levels, minLevel, maxLevel, chance, target, false, conflicts); }


    public CustomEnchant(String name, Levels levels, int minLevel, int maxLevel, double chance, EnchantTarget target, boolean isTreasure) { this(name, levels, minLevel, maxLevel, chance, target, isTreasure, false); }

    public CustomEnchant(String name, Levels levels, int minLevel, int maxLevel, double chance, EnchantTarget target, boolean isTreasure, Enchantment... conflicts)
    { this(name, levels, minLevel, maxLevel, chance, target, isTreasure, false, conflicts); }

    public CustomEnchant(String name, Levels levels, int minLevel, int maxLevel, double chance, EnchantTarget target, boolean isTreasure, boolean isCursed) {
        this(name, levels, minLevel, maxLevel, chance, target, isTreasure, isCursed, null);
    }
    public CustomEnchant(String name, Levels levels, int minLevel, int maxLevel, double chance, EnchantTarget target, boolean isTreasure, boolean isCursed, Enchantment... conflicts) {
        super(new NamespacedKey(MiniverseCore.getAPI(), StringUtils.stripColor(name.toLowerCase())));
        this.name = StringUtils.translate(name);
        min = minLevel;
        max = maxLevel;
        this.target = target;
        this.isTreasure = isTreasure;
        this.isCursed = isCursed;
        this.chance = chance;
        this.levels = levels;
        if(conflicts != null){
            this.conflicts = Arrays.asList(conflicts);
        }
        EnchantHandler.addEnchantments(this);
    }

    public CustomEnchant addConflicts(Enchantment... enchants) {
        if(conflicts == null) conflicts = new ArrayList<>();
        for(Enchantment ench : enchants)
            if(!conflicts.contains(ench))
                conflicts.add(ench);
        return this;
    }

    public double getChance() {
        return chance;
    }

    public String getName()
    {
        return name;
    }

    public int getMaxLevel()
    {
        return max;
    }

    public int getStartLevel()
    {
        return min;
    }

    public EnchantmentTarget getItemTarget()
    {
        return EnchantmentTarget.ALL;
    }

    public EnchantTarget getTarget()
    {
        return target;
    }

    public boolean isTreasure()
    {
        return isTreasure;
    }

    public boolean isCursed()
    {
        return isCursed;
    }

    public boolean conflictsWith(Enchantment enchantment) {
        if(conflicts == null) return false;
        return conflicts.contains(enchantment);
    }

    public boolean canEnchantItem(ItemStack itemStack) {
        return target.includes(itemStack);
    }

    public Levels getLevels() {
        return levels;
    }
}
