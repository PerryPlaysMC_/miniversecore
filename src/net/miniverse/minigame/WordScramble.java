package net.miniverse.minigame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WordScramble
{
  String newWord = "";
  String word;
  
  public WordScramble(String word) {
    this.word = word;
    scrambleWord();
    WordHandler.addWord(this);
  }
  
  public void scrambleWord() {
    newWord = "";
    HashMap<String, List<Character>> words = new HashMap<>();
    for (String x : word.split(" ")) {
      words.put(x, new ArrayList<>());
    }
    for(String x : word.split(" ")) {
      for (char c : x.toCharArray()) {
       words.get(x).add(c);
      }
    }
    java.util.Random r = new java.util.Random();
    for(String x : word.split(" ")) {
      List<Character> chars = words.get(x);
      int character = r.nextInt(((List)chars).size());
      while (chars.size() > 0) {
        newWord += chars.get(character);
        chars.remove(character);
        if (chars.size() > 0)
          character = r.nextInt(chars.size());
      }
      newWord += " ";
    }
    newWord = newWord.substring(0, newWord.length() + -1);
    
    if (((word.equalsIgnoreCase(newWord)) || (newWord.equalsIgnoreCase(word))) && (word.length() > 1)) {
      scrambleWord();
    }
  }
  
  public String getUnScrambledWord() {
    return word;
  }
  
  public String getScrambledWord() {
    return newWord;
  }
}
