package net.miniverse.minigame;

import java.util.ArrayList;
import java.util.List;
import net.miniverse.core.Miniverse;
import net.miniverse.core.MiniverseCore;
import net.miniverse.core.events.user.UserChatEvent;
import net.miniverse.user.User;
import net.miniverse.util.TimeUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

public class GameHandler extends BukkitRunnable implements Listener {
  WordScramble current;
  int currentNumber = -1;
  int i = -1;
  private List<User> won = new ArrayList<>();
  private long start = System.currentTimeMillis();

  public GameHandler() {
    runTaskTimer(MiniverseCore.getAPI(), 0L, 20L);
  }

  @EventHandler
  void onChat(UserChatEvent e) {
    if (current != null) {
      if ((!e.getMessage().equalsIgnoreCase(current.getUnScrambledWord())) && (!e.getMessage().startsWith("!!")) && (!e.getMessage().startsWith("!"))) {
        return;
      }
      if (e.getMessage().equalsIgnoreCase(current.getUnScrambledWord())) {
        if (!won.contains(e.getUser())) { won.add(e.getUser());
        } else {
          e.setCancelled(true);
          e.getUser().sendMessage("[c]You already got it!");
          e.setCancelled(true);
          return;
        }
        sendAll(e);
        e.setCancelled(true);
        return;
      }
      if ((e.getMessage().startsWith("!")) && (e.getMessage().split("!").length > 0) && (e.getMessage().split("!")[1].equalsIgnoreCase(current.getUnScrambledWord()))) {
        if (!won.contains(e.getUser())) { won.add(e.getUser());
        } else {
          e.getUser().sendMessage("[c]You already got it!");
          e.setCancelled(true);
          e.setCancelled(true);
          return;
        }
        sendAll(e);
        e.setCancelled(true);
      } else {
        if ((e.getMessage().startsWith("!!")) && (e.getMessage().split("!!").length > 0) && (e.getMessage().split("!!")[1].equalsIgnoreCase(current.getUnScrambledWord()))) {
          if (!won.contains(e.getUser())) { won.add(e.getUser());
          } else {
            e.getUser().sendMessage("[c]You already got it!");
            e.setCancelled(true);
            return;
          }
          sendAll(e);
          e.setCancelled(true);
          return;
        }
        e.getUser().sendMessage("[c]Oops. Try again!");
        e.setCancelled(true);
        return;
      }
    }
  }


  void sendAll(UserChatEvent e)
  {
    String time = TimeUtil.getFormatedTimeMillis(System.currentTimeMillis() - start, false);
    if (won.size() == 1) {
      Miniverse.broadCast("[pc]" + e.getUser().getName() + "[c]: Got it #[pc]" + won.size() + "[c] in [pc]" + time + "[c]s!", "[c]And won $1,000!");

      e.getUser().depositMoney(1000.0D);
      return;
    }
    if (won.size() == 2) {
      Miniverse.broadCast("[pc]" + e.getUser().getName() + "[c]: Got it #[pc]" + won.size() + "[c] in [pc]" + time + "[c]s!", "[c]And won $500!");

      e.getUser().depositMoney(500.0D);
      return;
    }
    if (won.size() == 3) {
      Miniverse.broadCast("[pc]" + e.getUser().getName() + "[c]: Got it #[pc]" + won.size() + "[c] in [pc]" + time + "[c]s!", "[c]And won $250!");

      e.getUser().depositMoney(250.0D);
      return;
    }
    Miniverse.broadCast("[pc]" + e.getUser().getName() + "[c]: Got it #[pc]" + won.size() + "[c] in [pc]" + time + "[c]s!");
  }

  public void run()
  {
    i += 1;
    if (i > 30) {
      if (current != null) {
        Miniverse.broadCast("[c][[pc]WordScramble[c]] [pc]" + current.getScrambledWord() + " [c]=[pc] " + current.getUnScrambledWord());
        current.scrambleWord();
        current = null;
      }
      if (i == 60) i = -1;
      return;
    }
    if ((i == 0) &&
            (WordHandler.getWords().size() > 0)) {
      currentNumber += 1;
      if (current != null) {
        Miniverse.broadCast("[c][[pc]WordScramble[c]] [pc]" + current.getScrambledWord() + " [c]=[pc] " + current.getUnScrambledWord());
        current.scrambleWord();
      }
      won.clear();
      current = WordHandler.getWord(currentNumber);
      start = System.currentTimeMillis();
      Miniverse.broadCast("[c][[pc]WordScramble[c]] Word: [pc]" + current.getScrambledWord());
      if (currentNumber == WordHandler.getWords().size() + -1) currentNumber = -1;
    }
  }
}
